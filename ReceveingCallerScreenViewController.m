//
//  ReceveingCallerScreenViewController.m
//  SiempreWifi
//
//  Created by Jayesh on 12/13/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "ReceveingCallerScreenViewController.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import "HomeViewController.h"
#import "AFNetworking.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"

@interface ReceveingCallerScreenViewController ()<TCDeviceDelegate,TCConnectionDelegate>
@property (weak, nonatomic) IBOutlet UILabel *displayTimer;

@end

@implementation ReceveingCallerScreenViewController
{
    bool start;
    NSTimeInterval time;
    NSString *startTime;
    NSString *userName;
    NSString *incomingNumber;
    NSString *callStatus;
}
@synthesize incomingPhNumber,endBtn,answerBtn;

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = NO;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = YES;
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted)
        {
            // Microphone enabled code
        }
        else
        {
            UIAlertView *alert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Microphone access required",nil) message:NSLocalizedString(@"In order to use this app to make phone calls you will need to grant access to the phone's microphone.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [alert show];
        }
    }];
    
    // Do any additional setup after loading the view.
    [self.endBtn setTitle:NSLocalizedString(@"END", nil) forState:UIControlStateNormal];
    [self.answerBtn setTitle:NSLocalizedString(@"ANSWER", nil) forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismisView) name:@"receiveDismissCallScreen" object:nil];

    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/ringring.mp3", [[NSBundle mainBundle] resourcePath]]];
    NSArray *inputs = [[AVAudioSession sharedInstance] availableInputs];
    for (id temp in inputs)
    {
        NSLog(@"%@",temp);
        
    }
    
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.numberOfLoops = -1;
   
    if (audioPlayer == nil)
    {
        NSLog(@"Error");
    }else
    {
       [audioPlayer play];
    }
    self.displayTimer.text = @"00.00";
    start = false;
   self.displayTimer.hidden =YES;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    userName = [defaults objectForKey:@"userName"];
    incomingNumber = [defaults objectForKey:@"incomingNumber"];
    NSLog(@"Incoming Number On The Screen ------->%@",incomingNumber);
   incomingPhNumber.text = incomingNumber;
 
    NSUserDefaults *defaultsStr = [NSUserDefaults standardUserDefaults];
    [defaultsStr setObject:@"true" forKey:@"missedCalledStatus"];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}
-(void) dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)connection:(TCConnection*)connection didFailWithError:(NSError*)error
{
    NSLog(@"Failed Due to Error : %@",[error localizedDescription]);
    
}

-(void)device:(TCDevice*)device didStopListeningForIncomingConnections:(NSError*)error
{
    NSLog(@"Stopped Listening");
    NSLog(@"Twilio Service Error : Twillio Server Disconnected");
}

-(void)update{
    if (start == false) {
        return;
    }
    NSTimeInterval currebtTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elappedTime = currebtTime -time;
    
    int minutes = (int)(elappedTime / 60.0);
    int seconds = (int)(elappedTime - (minutes*60));
    
    self.displayTimer.text = [NSString stringWithFormat:@"%02u:%02u",minutes,seconds];
    
    [self performSelector:@selector(update) withObject:self afterDelay:0.1];
    
}



-(IBAction)callEndBtn:(id)sender
{
    [self disconnectCall];

}

-(void)callStatusType{
    
    if([callStatus isEqualToString:@"false"]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        userName = [defaults objectForKey:@"userName"];
        
        NSLog(@"Call End Button --->%@",startTime);
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        startTime = [DateFormatter stringFromDate:[NSDate date]];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"update_call_log?email_ID=%@&time=%@&duration=%@&caller_ID=%@&type=missed",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.displayTimer.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[incomingNumber stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        
        NSLog(@"startTime------>%@",serverAddress);
        [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"Saved");
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Not Saved.....: %@", error);
             
         }];

    }
}

-(void)removeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)callPickUp:(id)sender
{
    
    answerBtn.hidden  =YES;
    endBtn.frame = CGRectMake(10, endBtn.frame.origin.y, ((self.view.frame.size.width/30)*95),54);
    endBtn.hidden = YES;
    UIButton *EndButton =[[UIButton alloc]initWithFrame:CGRectMake(40, endBtn.frame.origin.y, self.view.frame.size.width-80, self.endBtn.frame.size.height)];
    [EndButton setTitle:NSLocalizedString(@"END", nil)  forState:UIControlStateNormal];
    [EndButton setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:EndButton];
    [EndButton addTarget:self action:@selector(disconnectCall) forControlEvents:UIControlEventTouchUpInside];
    
    [audioPlayer stop];

    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    NSLog(@"%@",delegate.phConnection);
    
    [delegate.phConnection accept];
    
    NSLog(@"Inside callPickUp %@",delegate.phConnection.description);
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    startTime = [DateFormatter stringFromDate:[NSDate date]];
   
    callStatus =@"true";
    NSUserDefaults *defaultsStr = [NSUserDefaults standardUserDefaults];
    [defaultsStr setObject:@"true" forKey:@"missedCalledStatus"];

    
    self.displayTimer.hidden = NO;
    if (start == false)
    {
        start = true;
        
        time = [NSDate timeIntervalSinceReferenceDate];
        [self update];
        
    }else{
        
        start = false;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"update_call_log?email_ID=%@&time=%@&duration=%@&caller_ID=%@&type=received",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.displayTimer.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.incomingPhNumber.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                  [NSString stringWithFormat:@"Received"],@"CALL TYPE",
                                 [NSString stringWithFormat:@"%@",self.incomingPhNumber.text], @"CALLER",
                                  nil];
    
    
    [Flurry logEvent:@"Call" withParameters:flurryParams];

    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithFormat:@"CALL RECEIVED FORM : %@",self.incomingPhNumber.text]     // Event category (required)
                                                          action:@"CALL TAKEN"  // Event action (required)
                                                           label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                                                           value:nil] build]];
    
    
    NSLog(@"startTime------>%@",serverAddress);
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Saved");
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Not Saved.....: %@", error);
         
     }];
    

}

-(void)disconnectCall
{
        [audioPlayer stop];
        NSLog(@"Disconnect......");
        [self dismisView];
    
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        NSLog(@"Inside disconnectCall :  phConnection : %@",delegate.phConnection);

        [delegate.phConnection reject];
        [delegate.phConnection disconnect];
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
        userName = [defaults objectForKey:@"userName"];
    
        NSLog(@"Call End Button --->%@",startTime);
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        startTime = [DateFormatter stringFromDate:[NSDate date]];
    
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"update_call_log?email_ID=%@&time=%@&duration=%@&caller_ID=%@&type=missed",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.displayTimer.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[incomingNumber stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
        NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]], @"USER NAME",
                                  [NSString stringWithFormat:@"Missed"],@"CALL TYPE",
                                  [NSString stringWithFormat:@"%@",self.incomingPhNumber.text],@"CALLER",
                                  nil];
    
        [Flurry logEvent:@"Call" withParameters:flurryParams];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithFormat:@"CALL MISSED FORM : %@",self.incomingPhNumber.text]     // Event category (required)
                                                          action:@"CALL MISSED"  // Event action (required)
                                                           label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                                                           value:nil] build]];
    
        NSLog(@"startTime------>%@",serverAddress);
         [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"Saved");
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Not Saved.....: %@", error);
             
         }];
}

-(void)dismisView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
