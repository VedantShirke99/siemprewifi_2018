//
//  IAPViewController.m
//  SiempreWifi
//
//  Created by Jayesh on 03/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//


#define CURRENCY_SYMBOL [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol]
#import "IAPViewController.h"
#import "SWIAPHelper.h"
#import "InAppPurchaseCellTableViewCell.h"
@interface IAPViewController ()
{
    NSArray *_products;
    UITableView *productTable;
    
    UILabel *packTitle;
    UILabel *packAmountTitle;
    UILabel *creditsTitle;
    
    UILabel *deductionrate;
    
    MBProgressHUD *responseHud;
}
@end

@implementation IAPViewController

-(instancetype) init
{
    self = [super init];
    if (self)
    {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        packTitle = [[UILabel alloc]initWithFrame:CGRectMake(LeftSpacing, 66, 120, 50)];
        [self.view addSubview:packTitle];
        [packTitle setText:@"Pack Name"];
        
        packAmountTitle= [[UILabel alloc]initWithFrame:CGRectMake(132, 66, 130, 50)];
        [self.view addSubview:packAmountTitle];
        [packAmountTitle setText:@"Amount"];
        
        creditsTitle= [[UILabel alloc]initWithFrame:CGRectMake(230,66,self.view.frame.size.width/3, 50)];
        [self.view addSubview:creditsTitle];
        [creditsTitle setText:@"Credits"];
        
        
        productTable = [[UITableView alloc]initWithFrame:CGRectMake(-1, self.navigationController.navigationBar.frame.size.height+105,self.view.frame.size.width+2,self.view.frame.size.height  -  self.navigationController.navigationBar.frame.size.height-150)];
        [productTable.layer setBorderWidth:0.3f];
        [productTable.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.view addSubview:productTable];
        
        self.title = @"In App Purchase";
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        self.navigationController.navigationBar.translucent = NO;
        
        productTable.delegate = self;
        productTable.dataSource = self;
        productTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [productTable registerClass:[InAppPurchaseCellTableViewCell class] forCellReuseIdentifier:@"ProductCell"];
        
        
        deductionrate = [[UILabel alloc]initWithFrame:CGRectMake(0, productTable.frame.origin.y+productTable.frame.size.height, self.view.frame.size.width, 50)];
        [deductionrate setFont:[UIFont systemFontOfSize:13]];
        [deductionrate setTextAlignment:NSTextAlignmentCenter];
        [deductionrate setText:@"Phone Calls = 2 Credits. Text Message = 2 Credits."];
        [self.view addSubview:deductionrate];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, 200, 50)];
        [titleLabel setText:@"In-App Purchase"];
        [titleLabel setFont:[UIFont systemFontOfSize:15]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [leftButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(backBarButtonItem_clicked) forControlEvents:UIControlEventTouchUpInside];
        [leftButton setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
        [self.navigationItem setLeftBarButtonItem:leftBtn];
        [self.navigationItem setTitleView:titleLabel];
        
        
        
    }
    return self;
}
-(void)backBarButtonItem_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadData];
}
-(void) responseReceived
{
    NSLog(@"Response Received ");
    [responseHud hide:YES];
}
-(void) loadData
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.delegate = self;
    hud.labelText = NSLocalizedString(@"Loading",nil);
    [SWIAPHelper sharedInstance].controllerDelegate = self;
    [[SWIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success)
        {
            [hud hide:YES];
            _products = [self sort:products];
            [productTable reloadData];
        }
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    responseHud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    responseHud.delegate = self;
    responseHud.labelText = NSLocalizedString(@"Loading",nil);
    SKProduct *product = _products[indexPath.row];
    NSLog(@"Buying %@...", product.productIdentifier);
    [[SWIAPHelper sharedInstance] buyProduct:product];
}

-(NSArray *) sort:(NSArray *) array
{
    NSMutableArray *newArray = [NSMutableArray array];
    [newArray addObject:[array objectAtIndex:1]];
    [newArray addObject:[array objectAtIndex:0]];
    [newArray addObject:[array objectAtIndex:2]];
    
    
    return (NSArray *) newArray;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_products count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InAppPurchaseCellTableViewCell *cell =(InAppPurchaseCellTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    SKProduct * product = (SKProduct *) _products[indexPath.row];
    cell.packName.text = product.localizedTitle;
    cell.packAmount.text = [NSString stringWithFormat:@"%@",product.price];
    
    if ([product.localizedTitle isEqualToString:PLATINUM_PACK])
    {
        cell.credits.text = @"125 Credits";
    }
    else
        if ([product.localizedTitle isEqualToString:GOLD_PACK])
        {
            cell.credits.text = @"45 Credits";
        }
        else
            if ([product.localizedTitle isEqualToString:SILVER_PACK])
            {
                cell.credits.text = @"20 Credits";
            }
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
