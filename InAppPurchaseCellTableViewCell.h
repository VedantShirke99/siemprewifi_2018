//
//  InAppPurchaseCellTableViewCell.h
//  SiempreWifi
//
//  Created by Jayesh on 14/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//
#define LeftSpacing 15
#import <UIKit/UIKit.h>

@interface InAppPurchaseCellTableViewCell : UITableViewCell
@property (nonatomic,strong)  UILabel *packName;
@property (nonatomic,strong)  UILabel *packAmount;
@property (nonatomic,strong)  UILabel *credits;
@end
