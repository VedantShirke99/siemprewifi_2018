//
//  MessageBoxTableViewController.m
//  SampleSlider
//
//  Created by Jayesh on 12/5/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "MessageBoxTableViewController.h"
#import "AFNetworking.h"
#import "DetailViewController.h"
#import "MyCell.h"
#import "MBProgressHUD.h"
#import "HomeViewController.h"


@interface MessageBoxTableViewController ()<MBProgressHUDDelegate>{
    
    NSMutableArray *phoneArray;
    NSMutableArray *bodyArray;
    NSArray *messages;
    NSMutableArray *dateArray;
    NSMutableArray *timeArray;
    MBProgressHUD *HUD;
}

@property (strong,nonatomic)NSArray * messsages;

@end

@implementation MessageBoxTableViewController

@synthesize myTableView;


- (id)initWithStyle:(UITableViewStyle)style
{
    
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-siempre-transparent.png"]];
    titleImageView.frame = CGRectMake(0, 0, 0, self.navigationController.navigationBar.frame.size.height); // Here I am passing
    titleImageView.contentMode=UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = titleImageView;
    
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    
    phoneArray =[NSMutableArray new];
    bodyArray = [NSMutableArray new];
    dateArray = [[NSMutableArray alloc] init];
    timeArray = [[NSMutableArray alloc] init];
    UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.layer.frame.size.width, 80)];
    headerview.backgroundColor =  [UIColor colorWithRed:(39/255.0) green:(48/255.0) blue:(56/255.0) alpha:alphaStage];
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc]initWithItems:@[NSLocalizedString(@"INBOX",nil),NSLocalizedString(@"SENT",nil)]];
    
    [segmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    NSLog(@"Width ------%f",self.view.layer.frame.size.width);
     segmentControl.frame = CGRectMake((((headerview.layer.frame.size.width)/2)-125),(((headerview.layer.frame.size.height)/100)*30), 250, 30);
    [headerview addSubview:segmentControl];
    NSLog(@"Header View Size----%f",headerview.layer.frame.size.width);
    NSLog(@"Self View Size----%f",self.view.layer.frame.size.width);

    self.tableView.tableHeaderView = headerview;
    segmentControl.tintColor = [UIColor orangeColor];
    segmentControl.layer.cornerRadius = 4;
    segmentControl.backgroundColor = [UIColor whiteColor];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"inbox" forKey:@"msgType"];

    [self inboxMessages];
    //[self hudAssignment];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hudAssignment
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.delegate = self;
}

-(void)updateInbox
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"inboxMessageLogs?email_ID=%@",userName]];
    
    NSURL *url = [NSURL URLWithString:serverAddress];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"Response : %@",responseObject);
        self.messsages = [[[responseObject objectForKey:@"messageLogArray"]reverseObjectEnumerator]allObjects];
        
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [DateFormatter stringFromDate:[NSDate date]];
        
        NSDateComponents *components = [[NSDateComponents alloc] init] ;
        [components setDay:-1];
        
        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        NSString *yesterdayDateStr = [DateFormatter stringFromDate:yesterday];
        
        
        
        phoneArray = [[NSMutableArray alloc] init];
        bodyArray = [[NSMutableArray alloc] init];
        dateArray = [[NSMutableArray alloc]init];
        timeArray = [[NSMutableArray alloc]init];
        
        for (NSDictionary *inbox in self.messsages) {
            
            if([[[inbox objectForKey:@"fields"]valueForKey:@"type"] isEqualToString:@"sentbox"]){
                NSDictionary *dict = [inbox valueForKey:@"fields"];
                
                
                NSLog(@"Inside The LOOP---->%@",dict[@"callerid"]);
                
                NSString *callerid = dict[@"callerid"];
                NSString* myNewString = [NSString stringWithFormat:@"%@", callerid];
                [phoneArray addObject:myNewString];
                [bodyArray addObject:dict[@"body"]];
                NSString *dictVal = [[inbox objectForKey:@"fields"]valueForKey:@"time"];
                NSArray* timestampStr = [dictVal componentsSeparatedByString: @" "];
                NSString* day = [timestampStr objectAtIndex: 0];
                NSString* time = [timestampStr objectAtIndex: 1];
                
                if([dateString isEqualToString:day]){
                    [dateArray addObject:NSLocalizedString(@"Today",nil)];
                }else if([day isEqualToString:yesterdayDateStr]){
                    [dateArray addObject:NSLocalizedString(@"Yesterday",nil)];
                }else{
                    [dateArray addObject:day];
                }
                [timeArray addObject:time];
            }
            
        }
        [self.tableView reloadData];
    }
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Request Failed: %@, %@", error, error.userInfo);
     }];

     
}
-(void)inboxMessages
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"inboxMessageLogs?email_ID=%@",userName]];
    
    NSURL *url = [NSURL URLWithString:serverAddress];
   
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        self.messsages = [[[responseObject objectForKey:@"messageLogArray"]reverseObjectEnumerator]allObjects];
    
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [DateFormatter stringFromDate:[NSDate date]];
        
        NSDateComponents *components = [[NSDateComponents alloc] init] ;
        [components setDay:-1];
        
        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        NSString *yesterdayDateStr = [DateFormatter stringFromDate:yesterday];

        
        phoneArray = [[NSMutableArray alloc] init];
        bodyArray = [[NSMutableArray alloc] init];
        dateArray = [[NSMutableArray alloc]init];
        timeArray = [[NSMutableArray alloc]init];
        
        for (NSDictionary *inbox in self.messsages)
        {
            
            NSDictionary *dict = [inbox valueForKey:@"fields"];
            if([dict[@"type"]isEqualToString:@"inbox"]){
                NSLog(@"Inside The LOOP---->%@",dict[@"callerid"]);
                
                NSString *callerid = [[inbox objectForKey:@"fields"]valueForKey:@"callerid"];
                NSString* myNewString = [NSString stringWithFormat:@"%@", callerid];
                [phoneArray addObject:myNewString];
                [bodyArray addObject:dict[@"body"]];
                NSString *dictVal = [[inbox objectForKey:@"fields"]valueForKey:@"time"];
                NSArray* timestampStr = [dictVal componentsSeparatedByString: @" "];
                NSString* day = [timestampStr objectAtIndex: 0];
                NSString* time = [timestampStr objectAtIndex: 1];
                
                if([dateString isEqualToString:day]){
                    [dateArray addObject:NSLocalizedString(@"Today",nil)];
                }else if([day isEqualToString:yesterdayDateStr]){
                    [dateArray addObject:NSLocalizedString(@"Yesterday",nil)];
                }else{
                    [dateArray addObject:day];
                }
                [timeArray addObject:time];
            }
        }
                NSLog(@"Phone Array---->%@",dateArray);
                NSLog(@"Body Array---->%@",timeArray);
                NSLog(@"The Array: %@",timeArray);
                HUD.hidden =YES;
                [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSLog(@"Request Failed: %@, %@", error, error.userInfo);
    }];
    
    [operation start];
    [self hudAssignment];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateInbox];
}

-(void)sendMessages
{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    NSLog(@"username---->%@",userName);
    
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"sendMessageLogs?email_ID=%@",userName]];
    
    NSURL *url = [NSURL URLWithString:serverAddress];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //AFNetworking asynchronous url request
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    NSLog(@"Response ------>%@",operation.responseSerializer);
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        HUD.hidden =YES;
        self.messsages = [[[responseObject objectForKey:@"messageLogArray"]reverseObjectEnumerator]allObjects];
        
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [DateFormatter stringFromDate:[NSDate date]];
        
        NSDateComponents *components = [[NSDateComponents alloc] init] ;
        [components setDay:-1];
        
        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        NSString *yesterdayDateStr = [DateFormatter stringFromDate:yesterday];
        
        
        
        phoneArray = [[NSMutableArray alloc] init];
        bodyArray = [[NSMutableArray alloc] init];
        dateArray = [[NSMutableArray alloc]init];
        timeArray = [[NSMutableArray alloc]init];
        
        for (NSDictionary *inbox in self.messsages) {
            
            if([[[inbox objectForKey:@"fields"]valueForKey:@"type"] isEqualToString:@"sentbox"]){
                NSDictionary *dict = [inbox valueForKey:@"fields"];
                
                
                NSLog(@"Inside The LOOP---->%@",dict[@"callerid"]);
                
                NSString *callerid = dict[@"callerid"];
                NSString* myNewString = [NSString stringWithFormat:@"%@", callerid];
                [phoneArray addObject:myNewString];
                [bodyArray addObject:dict[@"body"]];
                NSString *dictVal = [[inbox objectForKey:@"fields"]valueForKey:@"time"];
                NSArray* timestampStr = [dictVal componentsSeparatedByString: @" "];
                NSString* day = [timestampStr objectAtIndex: 0];
                NSString* time = [timestampStr objectAtIndex: 1];
                
                if([dateString isEqualToString:day]){
                    [dateArray addObject:NSLocalizedString(@"Today",nil)];
                }else if([day isEqualToString:yesterdayDateStr]){
                    [dateArray addObject:NSLocalizedString(@"Yesterday",nil)];
                }else{
                    [dateArray addObject:day];
                }
                [timeArray addObject:time];
            }
            
        }

       [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Request Failed: %@, %@", error, error.userInfo);
    }];
    
    [operation start];
    [self hudAssignment];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.messsages count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";

    NSLog(@"tempDictionary------>%@",self.messsages);
    MyCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!mycell){
        
        mycell = [[MyCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSLog(@"Phone Array->-->-->-->%@",phoneArray);
    mycell.phoneLabel.text = phoneArray[indexPath.row];
    mycell.msgBody.text = bodyArray[indexPath.row];
    mycell.date.text = dateArray[indexPath.row];
    mycell.time.text = timeArray[indexPath.row];
    
    return mycell;
}


#pragma mark - Prepare For Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    DetailViewController *detailViewController = (DetailViewController *)segue.destinationViewController;
    detailViewController.detailsOfMSg = [self.messsages objectAtIndex:indexPath.row];
}

-(void) segmentedControlValueDidChange:(UISegmentedControl *)segment{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (segment.selectedSegmentIndex == 0) {
        NSLog(@"Inbox Clicked");
        [defaults setObject:@"inbox" forKey:@"msgType"];
        [self inboxMessages];
        
    } else {
        NSLog(@"Send Clicked");
        
        [defaults setObject:@"sent" forKey:@"msgType"];
        [self sendMessages];
        
    }
    
}



@end
