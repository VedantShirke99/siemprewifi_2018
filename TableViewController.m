//
//  TableViewController.m
//  SampleSlider
//
//  Created by Jayesh on 12/2/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "TableViewController.h"
#import "ViewController.h"
#import "AFNetworking.h"
#import "DateAndTimeTableViewCell.h"
#import "MBProgressHUD.h"
#import "CallViewController.h"
#import "HomeViewController.h"

@interface TableViewController ()<MBProgressHUDDelegate>{
    
    MBProgressHUD *HUD;
}

@property (strong, nonatomic) NSArray *googlePlacesArrayFromAFNetworking;


@property (strong, nonatomic) NSArray *missedCallArray;



@end

@implementation TableViewController

@synthesize InboxAndSendSegmentedControll,filteredTableData,dateArray,timeArray,callerIDArray;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-siempre-transparent.png"]];
    titleImageView.frame = CGRectMake(0, 0, 0, self.navigationController.navigationBar.frame.size.height); // Here I am passing
    titleImageView.contentMode=UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = titleImageView;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    NSString *password = [defaults objectForKey:@"password"];
    int flag = [defaults integerForKey:@"flag"];
    
    NSLog(@"Inherited Values userName ----->%@",userName);
    NSLog(@"Inherited Values Password ----->%@",password);
    NSLog(@"Inherited Values flag----->%d",flag);

    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    dateArray = [[NSMutableArray alloc] init];
    timeArray = [[NSMutableArray alloc] init];
    callerIDArray = [[NSMutableArray alloc]init];
    
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.delegate = self;
    [self callLogs];
    
    UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.layer.frame.size.width, 80)];
    headerview.backgroundColor = [UIColor colorWithRed:(39/255.0) green:(48/255.0) blue:(56/255.0) alpha:alphaStage];
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc]initWithItems:@[NSLocalizedString(@"ALL",nil),NSLocalizedString(@"MISSED",nil)]];
    
    [segmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    NSLog(@"self.view.layer.frame.size.width-->%f",self.view.layer.frame.size.width);
    NSLog(@"self.view.layer.frame.size.width-->%f",self.view.layer.frame.size.height);
    
    segmentControl.frame = CGRectMake((((headerview.layer.frame.size.width)/2)-125),(((headerview.layer.frame.size.height)/100)*30), 250, 30);
    [headerview addSubview:segmentControl];
    segmentControl.tintColor = [UIColor orangeColor];
    segmentControl.layer.cornerRadius = 4;
    segmentControl.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headerview;
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self callLogs];
    [self.tableView reloadData];
}

-(void)spinner
{
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.dimBackground = YES;
    HUD.delegate = self;
}

-(void)myTask
{
    sleep(2);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callLogs
{
  
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    NSLog(@"username---->%@",userName);
    
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"callLogs?email_ID=%@",userName]];
    
    NSURL *url = [NSURL URLWithString:serverAddress];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        HUD.hidden =YES;
        
        self.googlePlacesArrayFromAFNetworking = [responseObject objectForKey:@"callLogArray"];
        
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [DateFormatter stringFromDate:[NSDate date]];
        NSLog(@"Date--->%@",dateString);
        
        NSDateComponents *components = [[NSDateComponents alloc] init] ;
        [components setDay:-1];
        
        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        NSString *yesterdayDateStr = [DateFormatter stringFromDate:yesterday];
        
        NSLog(@"Yesterday--->%@",[DateFormatter stringFromDate:yesterday]);
        
        for (NSDictionary *datAndTime in self.googlePlacesArrayFromAFNetworking) {
            
            NSString *dict = [[datAndTime valueForKey:@"fields"]valueForKey:@"time"];
            NSString *callerID = [[datAndTime valueForKey:@"fields"]valueForKey:@"callerid"];
            NSArray* timestampStr = [dict componentsSeparatedByString: @" "];
            NSString* day = [timestampStr objectAtIndex: 0];
            NSString* time = [timestampStr objectAtIndex: 1];
            
            if([dateString isEqualToString:day]){
                [dateArray addObject:NSLocalizedString(@"Today",nil)];
            }else if([day isEqualToString:yesterdayDateStr]){
                [dateArray addObject:NSLocalizedString(@"Yesterday",nil)];
            }else{
                [dateArray addObject:day];
            }
            [timeArray addObject:time];
            [callerIDArray addObject:callerID];
            
        }
        
        
        [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Request Failed: %@, %@", error, error.userInfo);
        
    }];
    
    [operation start];
    
}

-(void)missedCalls
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"missedCallLogs?email_ID=%@",userName]];
    
    NSURL *url = [NSURL URLWithString:serverAddress];

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //AFNetworking asynchronous url request
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [HUD hide:YES];
        
         self.googlePlacesArrayFromAFNetworking =[[self.googlePlacesArrayFromAFNetworking reverseObjectEnumerator]allObjects];
        
        self.googlePlacesArrayFromAFNetworking = [responseObject objectForKey:@"callLogArray"];
        
       
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [DateFormatter stringFromDate:[NSDate date]];
        
        NSDateComponents *components = [[NSDateComponents alloc] init] ;
        [components setDay:-1];
        
        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        NSString *yesterdayDateStr = [DateFormatter stringFromDate:yesterday];
        
        for (NSDictionary *datAndTime in self.googlePlacesArrayFromAFNetworking) {
            
            NSString *dict = [[datAndTime valueForKey:@"fields"]valueForKey:@"time"];
            NSString *callerID = [[datAndTime valueForKey:@"fields"]valueForKey:@"callerid"];
            NSArray* timestampStr = [dict componentsSeparatedByString: @" "];
            NSString* day = [timestampStr objectAtIndex: 0];
            NSString* time = [timestampStr objectAtIndex: 1];
        
            
           
            if([dateString isEqualToString:day]){
                [dateArray addObject:NSLocalizedString(@"Today",nil)];
            }else if([day isEqualToString:yesterdayDateStr]){
                [dateArray addObject:NSLocalizedString(@"Yesterday",nil)];
            }else{
                [dateArray addObject:day];
            }
            [timeArray addObject:time];

            [callerIDArray addObject:callerID];
            
        }
        
        NSLog(@"CallerId----->%@",callerIDArray);	
        
        NSLog(@"The Array: %@",self.googlePlacesArrayFromAFNetworking);
        
        for (NSDictionary *call in self.googlePlacesArrayFromAFNetworking) {
            
            NSDictionary *dict = [call valueForKey:@"fields"];
            NSLog(@"Dictionary---->%@",dict);
            
        }
        
        [self.tableView reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Request Failed: %@, %@", error, error.userInfo);
        
    }];
    
    [operation start];

    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.delegate = self;
    //HUD.mode = MBProgressHUDModeDeterminate;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.googlePlacesArrayFromAFNetworking count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    DateAndTimeTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSDictionary *tempDictionary= [self.googlePlacesArrayFromAFNetworking objectAtIndex:indexPath.row];
    
    UIImage * outgoing = [UIImage imageNamed:@"outgoingCall.png"];
    UIImage * incoming = [UIImage imageNamed:@"Incoming_call.jpg"];
    UIImage * missed = [UIImage imageNamed:@"incomingCall.png"];
    
    NSString *str = [[tempDictionary objectForKey:@"fields"]valueForKey:@"type"];
    
    if([str isEqualToString:@"missed"])
    {
        mycell.callerImage.image = missed;
    }
    if( [str isEqualToString:@"received"])
    {
        mycell.callerImage.image = incoming;
    }
    if([str isEqualToString:@"dialed"])
    {
        mycell.callerImage.image = outgoing;
    }
    
       mycell.date.text = dateArray[indexPath.row];
    mycell.time.text = timeArray[indexPath.row];
    mycell.callerID.text = callerIDArray[indexPath.row];
    
    return mycell;
}

#pragma mark - Prepare For Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    CallViewController *callViewController = (CallViewController*)segue.destinationViewController;
    callViewController.detailsOfCallLogs = [self.googlePlacesArrayFromAFNetworking objectAtIndex:indexPath.row];
}

-(void) segmentedControlValueDidChange:(UISegmentedControl *)segment{
    if (segment.selectedSegmentIndex == 0) {
        NSLog(@"All Clicked");
       [self callLogs];
        
    } else {
        NSLog(@"Missed Clicked");
        [self missedCalls];
        
    }

}




- (IBAction)segmetedControllBtn:(id)sender {
    if (InboxAndSendSegmentedControll.selectedSegmentIndex == 0) {
        NSLog(@"All Clicked");
        [self callLogs];
        
    } else {
        NSLog(@"Missed Clicked");
        [self missedCalls];
        
    }
    
}
@end
