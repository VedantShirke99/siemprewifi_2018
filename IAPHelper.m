//
//  IAPHelper.m
//  SiempreWifi
//
//  Created by Jayesh on 03/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "IAPHelper.h"
#include "AFNetworking.h"
#import "HomeViewController.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
@implementation IAPHelper
{
SKProductsRequest * _productsRequest;
// 4
RequestProductsCompletionHandler _completionHandler;
NSSet * _productIdentifiers;
NSMutableSet * _purchasedProductIdentifiers;
}

@synthesize controllerDelegate;

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    
    if ((self = [super init])) {
        
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        
        // Check for previously purchased products
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString * productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
                NSLog(@"Previously purchased: %@", productIdentifier);
            } else {
                NSLog(@"Not purchased: %@", productIdentifier);
            }
        }
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    
    // 1
    _completionHandler = [completionHandler copy];
    
    // 2
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}
#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    
    NSLog(@"Loaded list of products...");
    //_productsRequest = nil;
    
    NSArray * skProducts = response.products;

    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}

- (BOOL)productPurchased:(NSString *)productIdentifier
{
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product {
    
    NSLog(@"Buying %@...", product.productIdentifier);
    
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
    
    
}
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    
    NSNumber *rechargeAmount;
    long credits;
    NSString *transactionIdentifier =transaction.transactionIdentifier;
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
     [dateformatter setDateFormat:@"dd-MMMM-YYYY"];
    
    NSDate *transactionDate = transaction.transactionDate;
    
    NSString *transactionDateString =[dateformatter stringFromDate:transaction.transactionDate];
    [dateformatter setDateFormat:@"HH:mm:ss.SSS aa"];
    transactionDateString = [NSString stringWithFormat:@"%@ %@",transactionDateString,[dateformatter stringFromDate:transactionDate]];
    transactionDateString = [[transactionDateString stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    if ([transaction.payment.productIdentifier isEqualToString:PLATINUM_PACK_ID])
    {
        rechargeAmount = [NSNumber numberWithDouble:25.00];
        credits  = 125;
    }
    else
        if ([transaction.payment.productIdentifier  isEqualToString:GOLD_PACK_ID])
        {
            rechargeAmount = [NSNumber numberWithDouble:10.00];
              credits  = 45;
        }
        else
            if ([transaction.payment.productIdentifier  isEqualToString:SILVER_PACK_ID])
            {
                rechargeAmount = [NSNumber numberWithDouble:5.00];
                credits  = 20;
            }
    
    if (!rechargeAmount)
    rechargeAmount = [NSNumber numberWithLong:0];
    
    
    NSLog(@"completeTransaction...");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"userName"];
    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString *url = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"setCreditTransaction?email=%@&amount=%@&paymentTime=%@&paymentId=%@",username,rechargeAmount,transactionDateString,transactionIdentifier]];
    NSLog(@"url----%@",url);
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         UIAlertView *paymentAlert = [[UIAlertView alloc]initWithTitle:@"In-App Purcahse Alert" message:[NSString stringWithFormat:@"%ld Credits has been credited to your account.",credits] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [paymentAlert show];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
     }];
    [controllerDelegate responseReceived];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"restoreTransaction...");
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        [controllerDelegate responseReceived];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"In-App Purcahse Alert" message: transaction.error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
        [controllerDelegate responseReceived];
}
- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];
    
}
@end
