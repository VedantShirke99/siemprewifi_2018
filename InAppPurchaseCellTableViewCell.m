//
//  InAppPurchaseCellTableViewCell.m
//  SiempreWifi
//
//  Created by Jayesh on 14/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "InAppPurchaseCellTableViewCell.h"

@implementation InAppPurchaseCellTableViewCell
{
   
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.packName = [[UILabel alloc]initWithFrame:CGRectMake(LeftSpacing, 10, 120, 50)];
        [self.contentView addSubview:self.packName];
        
        self.packAmount= [[UILabel alloc]initWithFrame:CGRectMake(132, 10, 130, 50)];
        [self.contentView addSubview:self.packAmount];
        
        self.credits= [[UILabel alloc]initWithFrame:CGRectMake(230,10, 100, 50)];
        [self.contentView addSubview:self.credits];
        
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
