//
//  AppDelegate.m
//  SampleSlider
//
//  Created by Jayesh on 11/18/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "AppDelegate.h"
#import "CallViewController.h"
#import "PayPalMobile.h"
#import "TwilioClient.h"
#import "AFNetworking.h"
#import "MainVCViewController.h"
#import "UIViewController+AMSlideMenu.h"
#import "ReceveingCallerScreenViewController.h"
#import "Reachability.h"
#import "HomeViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"
#import "GAIDictionaryBuilder.h"

@interface AppDelegate ()<TCDeviceDelegate,TCConnectionDelegate,UIApplicationDelegate,AVAudioPlayerDelegate>
{
    TCDevice* _phone;
    TCConnection* _connection;
    id storedViewController;
    UIAlertView *alertView;
}
@end



@implementation AppDelegate
@synthesize window = _window;

@synthesize phone = _phone;
@synthesize phConnection = _connection;
@synthesize tracker;
@synthesize okToWait;



- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [GAI sharedInstance].optOut =  (BOOL)![[NSUserDefaults standardUserDefaults] boolForKey:GATRACKINGID];
    
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted)
        {
            // Microphone enabled code
        }
        else
        {
            UIAlertView *alert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Microphone access required",nil) message:NSLocalizedString(@"In order to use this app to make phone calls you will need to grant access to the phone's microphone.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [alert show];
        }
    }];
    
}
-(void) applicationDidFinishLaunching:(UIApplication *)application
{
    [Flurry startSession:FLURRYTOKEN];
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setDebugLogEnabled:YES];
    [self warnUserifOffline];
    
     [self handleTokenExpiry];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
    }
    
    if (![application isRegisteredForRemoteNotifications])
    {
        UIAlertView *alert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Allow Notification",nil) message:NSLocalizedString(@"To be able to send and receive text messages you will need to enable Notifications.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
        [alert show];
        
    }
    
}
-(void) warnUserifOffline
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There is no internet connection");

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please check your internet connection and try again later",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       
    }
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Flurry startSession:FLURRYTOKEN];
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setDebugLogEnabled:YES];
    NSLog(@" FLURRYTOKEN : %@",FLURRYTOKEN);
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"App_Status",@"Started",
                                   nil];
    [Flurry logEvent:@"App Status Event : Started" withParameters:articleParams];
    
    [self warnUserifOffline];
    
    [GAI sharedInstance].optOut = YES;
    [GAI sharedInstance].dispatchInterval = -1;
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    self.tracker = [[GAI sharedInstance] trackerWithName:@"SimpreWifi" trackingId:GATRACKINGID];

    self.tracker = [[GAI sharedInstance] defaultTracker];
    [self.tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"App Launched"     // Event category (required)
                                                          action:@"None"  // Event action (required)
                                                           label:@"NOne"          // Event label
                                                           value:nil] build]];
    
    NSLog(@"Twilion Client Version : %ld",(long)TwilioClient.version);
   [[TwilioClient sharedInstance] setLogLevel:TC_LOG_DEBUG];
    
    // Override point for customization after applicati [self connectionDidStartConnecting:_
      if (launchOptions!=nil) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"handleNotification" object:nil];
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
    }
    

    if (![application isRegisteredForRemoteNotifications])
    {
        UIAlertView *alert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Allow Notification",nil) message:NSLocalizedString(@"To be able to send and receive text messages you will need to enable Notifications.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
        [alert show];
        
    }
    
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AeS1HGyFuDkzbDKcczcJxoQj-acyG6rgANSxg3OHOY_jZmb7gOMNzNDCcqh6KY6jJIoB2s8p4_7UAhPr",PayPalEnvironmentSandbox : @""}];

    [self handleTokenExpiry];
    

    
    return YES;
}
-(void) handleTokenExpiry
{
    
     NSLog(@"Inside handleTokenExpiry");
    
    NSLog(@"%@",_phone.capabilities);
    [[UIApplication sharedApplication] setKeepAliveTimeout:TOKENINTERVAL handler:^
     {
         
         NSError *error;
         NSString *token;

         
         [self retrieveToken:&error token_p:&token];
         
         
         if (token == nil)
         {
             NSLog(@"Error retrieving token: %@", [error localizedDescription]);
         } else
         {
             NSLog(@"Got token: %@", token);
             [_phone updateCapabilityToken:token];
             
         }

         NSLog(@"Handler Called");
         
     }];
}
-(void)connection:(TCConnection*)connection didFailWithError:(NSError*)error
{
    NSLog(@"Failed Due to Error : %@",[error localizedDescription]);
    
  
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
  
    NSString* deviceTokenStr = [[[[deviceToken description]
                                stringByReplacingOccurrencesOfString: @"<" withString: @""]
                               stringByReplacingOccurrencesOfString: @">" withString: @""]
                              stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceTokenStr forKey:@"deviceTokenStr"];  
    [defaults synchronize];
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
 
   if ( application.applicationState == UIApplicationStateInactive)
    {

        
        NSDictionary *notificationMessage = [userInfo objectForKey:@"aps"];
        NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                      [NSString stringWithFormat:@"%@",notificationMessage[@"Sender"]],@"FROM",
                                      nil];
        
        [Flurry logEvent:@"Message Received" withParameters:flurryParams];
        
        
        [self.tracker send:[[GAIDictionaryBuilder createAppView] build]];
        [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithFormat:@"Message Received From :%@",notificationMessage[@"Sender"]]     // Event category (required)
                                                                   action:@"None"  // Event action (required)
                                                                    label:[NSString stringWithFormat:@"FROM : %@",notificationMessage[@"Sender"]]         // Event label
                                                                    value:nil] build]];
    }
    else if(application.applicationState == UIApplicationStateActive)
    {
        alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message Received",nil) message:NSLocalizedString(@"You have received new text in your inbox.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
        [alertView show];
        
        NSDictionary *notificationMessage = [userInfo objectForKey:@"aps"];
        NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                      [NSString stringWithFormat:@"%@",notificationMessage[@"Sender"]],@"FROM",
                                      nil];
        [Flurry logEvent:@"Message Received" withParameters:flurryParams];
        
        [self.tracker send:[[GAIDictionaryBuilder createAppView] build]];
        [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithFormat:@"Message Received From :%@",notificationMessage[@"Sender"]]     // Event category (required)
                                                                   action:@"None"  // Event action (required)
                                                                    label:[NSString stringWithFormat:@"FROM : %@",notificationMessage[@"Sender"]]         // Event label
                                                                    value:nil] build]];
    }
    
}

-(void)postNotificationToPresentPushMessagesVC
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"handleNotification" object:nil];
}


- (void)retrieveToken:(NSError **)error_p token_p:(NSString **)token_p
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    NSString *urlString = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"generate_token/?email_ID=%@",userName]];
    NSURL *url1 = [NSURL URLWithString:urlString];
    *error_p = nil;
    *token_p = [NSString stringWithContentsOfURL:url1 encoding:NSUTF8StringEncoding error:&(*error_p)];
    NSLog(@"Token Received: %@",*token_p);
    NSLog(@"\n");
   
}

-(void)registrationForTwilio
{
    NSError *error;
    NSString *token;
    [self retrieveToken:&error token_p:&token];
    
    
    if (token == nil)
    {
        NSLog(@"Error retrieving token: %@", [error localizedDescription]);
    } else
    {
        _phone = [[TCDevice alloc] initWithCapabilityToken:token delegate:self];
        NSLog(@"Device Capabilities: %@",_phone.capabilities);
        
    }
    
}



- (NSIndexPath *)initialIndexPathForLeftMenu
{
    return [NSIndexPath indexPathForRow:2 inSection:0];
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Invalid Token" forKey:@"deviceTokenStr"];
    NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                  nil];
    [Flurry logEvent:@"Failed to register for  Push Notification" withParameters:flurryParams];
    
    [self.tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Failed to register for  Push Notification"     // Event category (required)
                                                               action:@"None"  // Event action (required)
                                                                label:@"Failed to register for  Push Notification"         // Event label
                                                                value:nil] build]];
    
    
}
-(void)device:(TCDevice *)device didStopListeningForIncomingConnections:(NSError *)error
{
    NSLog(@"Stopped Listening due to Error : %@ ",[error localizedDescription]);
}
- (void)device:(TCDevice *)device didReceiveIncomingConnection:(TCConnection *)connection
{
    NSUserDefaults *incomingPhNumber = [NSUserDefaults standardUserDefaults];
    [incomingPhNumber setObject:[connection parameters][@"From"] forKey:@"incomingNumber"];
    NSLog(@"Incoming Number------>%@",[connection parameters][@"From"]);
    [incomingPhNumber setObject:[connection parameters][@"From"] forKey:@"incomingNo"];
    [incomingPhNumber synchronize];
    
    NSLog(@"Inside didReceiveIncomingConnection %@",connection.description);
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"receivePresentCallScreen"
     object:self];

    NSLog(@"Incoming connection from: %@", [connection parameters][@"From"]);
   
    if (device.state == TCDeviceStateBusy)
    {
        [connection reject];
        NSLog(@"Call Rejected");
    } else
    {
        //[connection accept];
        _connection = connection;
        _connection.delegate = self;
    }
    
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    [notification setAlertBody:@"Incoming Call on Siempre"];
    notification.soundName = UILocalNotificationDefaultSoundName;
    UIApplication *application = [UIApplication sharedApplication] ;
    [application setScheduledLocalNotifications:[NSArray arrayWithObject:notification]];
}

-(void)connectionDidConnect:(TCConnection*)connection
{
    NSLog(@"Connection Established");
}
-(void)connectionDidDisconnect:(TCConnection *)connection
{
    NSLog(@"Disconnected...appdelgate");
    
    //self.window.rootViewController = storedViewController;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"receiveDismissCallScreen"
     object:self];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * userName = [defaults objectForKey:@"userName"];
    NSString *missedCallStatus = [defaults objectForKey:@"missedCalledStatus"];
   
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *startTime = [DateFormatter stringFromDate:[NSDate date]];
    NSString *displayTimer = [defaults objectForKey:@"displayTimer"];
    NSString *calledNumber = [defaults objectForKey:@"incomingNo"];
    [defaults removeObjectForKey:@"missedCalledStatus"];
    
    
    if([missedCallStatus isEqualToString:@"true"]){
        NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"update_call_log?email_ID=%@&time=%@&duration=%@&caller_ID=%@&type=missed",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[displayTimer stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[calledNumber stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];

        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:serverAddress]];
        NSLog(@"%@",request);
        
    }
    
}

- (void)deviceDidStartListeningForIncomingConnections:(TCDevice*)device
{
    NSLog(@"Stopped Listening");
    NSLog(@"Twilio Service Error : Twillio Server Disconnected");
   
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self handleTokenExpiry];
    NSLog(@"Application-----%@",application);

    
}
- (BOOL)shouldAutorotate
{
       return NO;
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    //	(iOS 5)
    //	Only allow rotation to portrait
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)doSomething
{
    NSLog(@"background");
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
   
    
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                  nil];
    [Flurry logEvent:@"App Terminated" withParameters:flurryParams];
    
    [self.tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"App Termination "     // Event category (required)
                                                               action:@"None"  // Event action (required)
                                                                label:@"App Terminated"         // Event label
                                                                value:nil] build]];
}

@end
