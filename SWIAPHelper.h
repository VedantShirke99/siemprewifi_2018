//
//  SWIAPHelper.h
//  SiempreWifi
//
//  Created by Jayesh on 03/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "IAPHelper.h"

@interface SWIAPHelper : IAPHelper
+(instancetype) sharedInstance;
@end
