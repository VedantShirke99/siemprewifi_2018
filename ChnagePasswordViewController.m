//
//  ChnagePasswordViewController.m
//  SampleSlider
//
//  Created by Jayesh on 12/3/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "ChnagePasswordViewController.h"
#import "AFNetworking.h"
#import "HomeViewController.h"
#import "MBProgressHUD.h"
@interface ChnagePasswordViewController ()

@end

@implementation ChnagePasswordViewController
@synthesize currentpassword,newpassword,confirmPassword;
- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-siempre-transparent.png"]];
    titleImageView.frame = CGRectMake(0, 0, 0, self.navigationController.navigationBar.frame.size.height); // Here I am passing
    titleImageView.contentMode=UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = titleImageView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   }

- (IBAction)changePasswordBtn:(id)sender
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.delegate = self;
       
    if ([currentpassword.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please Enter Password",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        alert.tag =1;
        [alert show];
    } else if ([newpassword.text isEqualToString:@"" ]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter the New Password",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        alert.tag =2;
        [alert show];
    }else if ([confirmPassword.text isEqualToString:@""]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter the Confirm Password",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        alert.tag = 3;
        [alert show];
    }else if (![newpassword.text isEqualToString:confirmPassword.text])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Passwords mismatch.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        alert.tag = 4;
        [alert show];
    }
    else
    {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [defaults objectForKey:@"userName"];
    NSLog(@"username---->%@",userName);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"changePassword?email_ID=%@&currentPassword=%@&newPassword=%@",userName,currentpassword.text,newpassword.text]];
    
    
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         self.posts = (NSDictionary *) responseObject;
         
         self.post = self.posts[@"PasswordCheck"][@"success"];
         NSString *value = self.posts[@"PasswordCheck"][@"success"];
         

         
         if ([value isEqualToString:@"true"])
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Password Changed Successfully",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
             alert.tag = 5;
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [alert show];
             
         }else{
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Password Mismatch",nil) message:NSLocalizedString(@"The current password you entered is wrong.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
             alert.tag = 6;
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [alert show];
         }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
        {
           NSLog(@"Error: %@", error);
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Error Connecting to Server...!!!",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
         [alert setTag:7];
          [alert show];
     }];
    
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 5)
    {
        NSLog(@"Alert Clicked");
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SignInMainStoryBoard"];
        [self presentViewController:vc animated:YES completion:nil ];

    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}



- (IBAction)tapGesture:(id)sender {
      [self.view endEditing:YES];
}
@end
