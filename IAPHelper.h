//
//  IAPHelper.h
//  SiempreWifi
//
//  Created by Jayesh on 03/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//
#import <StoreKit/StoreKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "IAPViewController.h"

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAPHelper : NSObject <SKProductsRequestDelegate,SKPaymentTransactionObserver>
UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;

@property (nonatomic,strong) IAPViewController *controllerDelegate;

// Add two new method declarations
- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;


- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;

@end
