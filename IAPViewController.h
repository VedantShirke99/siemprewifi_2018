//
//  IAPViewController.h
//  SiempreWifi
//
//  Created by Jayesh on 03/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "ViewController.h"
#import "MBProgressHUD.h"
@interface IAPViewController : ViewController <UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>
-(void) responseReceived;
@end
