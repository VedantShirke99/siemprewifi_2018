//
//  ChangePasswordAndLogOutViewController.m
//  SampleSlider
//
//  Created by Jayesh on 12/12/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "ChangePasswordAndLogOutViewController.h"
#import "AFNetworking.h"
#import "HomeViewController.h"
#import "TwilioClient.h"
#import "MBProgressHUD.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "Flurry.h"
#import "AppDelegate.h"
@interface ChangePasswordAndLogOutViewController ()

@end

@implementation ChangePasswordAndLogOutViewController
@synthesize twiliNumber,userNameLabel;
- (void)viewDidLoad
{

    [super viewDidLoad];
    self.userNameLabel.text =@"";
    self.twiliNumber.text =@"";
    
    
    [self getUserDetails];

    
    UIButton *logoutbtn = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 30)];
    [self.view addSubview:logoutbtn];
    [logoutbtn setTitle:@"LOGOUT" forState:UIControlStateNormal];
    [logoutbtn setBackgroundColor:[UIColor redColor]];

    
}
-(void) getUserDetails
{
    MBProgressHUD *hud1 = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud1.delegate = self;
    hud1.labelText = NSLocalizedString(@"Loading",nil);
    
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defs objectForKey:@"userName"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"getTwilioPhoneNumber?email_ID=%@",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
  

    
    UILabel *TwilioNumber = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height - 50, self.view.frame.size.width,30)];
    [TwilioNumber setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:TwilioNumber];
    
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    
    long long expiryTime =[[delegate.phone.capabilities valueForKey:@"expiration"] longLongValue];
    
    
    NSDate *now = [NSDate date];
    
    long long seconds = (long long)([now timeIntervalSince1970]);

    
    
    if (seconds >= expiryTime)
    {
        self.twiliNumber.textColor = [UIColor redColor];
        //expired
    }
    else
    {
       self.twiliNumber.textColor = [UIColor whiteColor];
        //not expired
    }
    
    
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.userNameLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Username", nil),userName]];
         [self.twiliNumber setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Phone Number", nil),[[responseObject objectForKey:@"twilioPhoneNumber"] valueForKey:@"number"]]];
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
         NSLog(@"LogOut Saved.....: %@", error);
         
     }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)logoutButton:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Are you sure you want to logout?",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO",nil) otherButtonTitles:NSLocalizedString(@"YES",nil), nil];
    [alert show];
    

}
-(void)logout
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.delegate = self;
    hud.labelText = NSLocalizedString(@"Logging out",nil);
    
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
     __block NSString *userName = [defs objectForKey:@"userName"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"appLogout?email_ID=%@",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Logout");
         
         UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SignInMainStoryBoard"];
         [self presentViewController:vc animated:YES completion:nil ];
         NSDictionary * dict = [defs dictionaryRepresentation];
         NSLog(@"Dictionary ----%@",dict);
         
         [defs removeObjectForKey:@"userName"];
         [defs removeObjectForKey:@"password"];
         [defs removeObjectForKey:@"flag"];
         [defs synchronize];
          [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         
         id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         [tracker send:[[GAIDictionaryBuilder createAppView] build]];
         [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Message Received "     // Event category (required)
                                                               action:@"None"  // Event action (required)
                                                                label:[NSString stringWithFormat:@"User Logged Out : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                                                                value:nil] build]];
         
        
         
         NSLog(@"USER : %@",userName);
         NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSString stringWithFormat:@"%@",userName],@"USER NAME",
                                       nil];
         [Flurry logEvent:@"User LoggedOut" withParameters:flurryParams];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
         NSLog(@"LogOut Saved.....: %@", error);
          [MBProgressHUD hideHUDForView:self.view animated:YES];
     }];

}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        [self logout];
    }
   }
@end
