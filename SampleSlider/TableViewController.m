//
//  TableViewController.m
//  SampleSlider
//
//  Created by Jayesh on 12/2/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "TableViewController.h"
#import "ViewController.h"
#import "AFNetworking.h"
#import "DateAndTimeTableViewCell.h"
#import "MBProgressHUD.h"
#import "CallViewController.h"
#import "HomeViewController.h"

@interface TableViewController ()<MBProgressHUDDelegate>{
    
    MBProgressHUD *HUD;
}

@property (strong, nonatomic) NSArray *googlePlacesArrayFromAFNetworking;


@property (strong, nonatomic) NSArray *missedCallArray;

@end

@implementation TableViewController
{
    NSArray *callLogs;
    NSMutableArray *missCallArray;
    NSMutableArray *allCallArray;
    UISegmentedControl *segmentControl;
}

@synthesize InboxAndSendSegmentedControll,filteredTableData,dateArray,timeArray,callerIDArray;


-(void) parseCallLogs:(NSArray *)CallLogs
{
    missCallArray =[[NSMutableArray alloc] init];
    allCallArray =[[NSMutableArray alloc] init];
    
    for (NSDictionary *calllog in CallLogs)
    {
        NSMutableDictionary *field = [NSMutableDictionary dictionary];
//        [calllog objectForKey:@"fields"];
//        field = calllog
        [field setValue:[calllog objectForKey:@"pk"] forKey:@"pk"];
        [field setValue:[[calllog objectForKey:@"fields"] objectForKey:@"updated"] forKey:@"updated"];
        [field setValue:[[calllog objectForKey:@"fields"] objectForKey:@"time"] forKey:@"time"];
        [field setValue:[[calllog objectForKey:@"fields"] objectForKey:@"created"] forKey:@"created"];
        [field setValue:[[calllog objectForKey:@"fields"] objectForKey:@"duration"] forKey:@"duration"];
        [field setValue:[[calllog objectForKey:@"fields"] objectForKey:@"type"] forKey:@"type"];
        [field setValue:[[calllog objectForKey:@"fields"] objectForKey:@"customer"] forKey:@"customer"];
        [field setValue:[[calllog objectForKey:@"fields"] objectForKey:@"callerid"] forKey:@"callerid"];
        if ([[field objectForKey:@"type"] isEqualToString:@"missed"])
        {
            int counter = 0;
            //            [missCallArray addObject:field];
            for (NSArray *missedCall in missCallArray) {
                if([missedCall valueForKey:@"pk"] == [field objectForKey:@"pk"]){
                    counter ++;
                }
            }
            if(counter == 0){
                [missCallArray addObject:field];
            }
        }
        //            [allCallArray addObject:field];
        int counter = 0;
        //            [missCallArray addObject:field];
        for (NSArray *allCall in allCallArray) {
            if([allCall valueForKey:@"pk"] == [field objectForKey:@"pk"]){
                counter ++;
            }
        }
        if(counter == 0){
            [allCallArray addObject:field];
        }
    }
}
-(void) loadCallLogs
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.delegate = self;
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"callLogs?email_ID=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]]];
    
    [[AFHTTPRequestOperationManager manager] GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self parseCallLogs:[responseObject objectForKey:@"callLogArray"]];
         [HUD hide:YES];
         [self.tableView reloadData];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         NSLog(@"Request Failed: %@, %@", error, error.userInfo);
         [HUD hide:YES];
         
     }];
    
}
-(void)updateTable
{
    [self.tableView reloadData];
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadCallLogs];
    [self updateTable];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    missCallArray = [NSMutableArray array];
    allCallArray  = [NSMutableArray array];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.layer.frame.size.width, 80)];
    headerview.backgroundColor = [UIColor colorWithRed:(39/255.0) green:(48/255.0) blue:(56/255.0) alpha:alphaStage];
    segmentControl = [[UISegmentedControl alloc]initWithItems:@[NSLocalizedString(@"ALL",nil),NSLocalizedString(@"MISSED",nil)]];
    
    [segmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    
    segmentControl.frame = CGRectMake((((headerview.layer.frame.size.width)/2)-125),(((headerview.layer.frame.size.height)/100)*30), 250, 30);
    [headerview addSubview:segmentControl];
    segmentControl.tintColor = [UIColor orangeColor];
    segmentControl.layer.cornerRadius = 4;
    segmentControl.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = headerview;
}

-(void)backBarButtonItem_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (segmentControl.selectedSegmentIndex == 0)
    {
        return allCallArray.count;
    }
    else
    {
        return missCallArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DateAndTimeTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSDictionary *callLog;
    
    if (segmentControl.selectedSegmentIndex == 0)
        callLog = [allCallArray objectAtIndex:indexPath.row];
    else
        callLog = [missCallArray objectAtIndex:indexPath.row];
    
    NSLog(@"CALL LOG : %@",callLog);
    
    NSString *callType = [callLog valueForKey:@"type"]; //[[callLog objectForKey:@"fields"]valueForKey:@"type"];
    
    if([callType isEqualToString:@"missed"])
        mycell.callerImage.image = [UIImage imageNamed:@"incomingCall.png"];
    else
        if( [callType isEqualToString:@"received"])
            mycell.callerImage.image = [UIImage imageNamed:@"Incoming_call.jpg"];
        else
            if([callType isEqualToString:@"dialed"])
                mycell.callerImage.image = [UIImage imageNamed:@"outgoingCall.png"];
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSString *dateTime = [[allCallArray objectAtIndex:indexPath.row]valueForKey:@"time"];
    
    NSArray* dateTimeArray = [dateTime componentsSeparatedByString: @" "];
    
    NSString *callLogDate = [dateTimeArray objectAtIndex: 0];
    NSString *CallLogTime = [dateTimeArray objectAtIndex: 1];
    
    
    NSDateComponents *components = [[NSDateComponents alloc] init] ;
    [components setDay:-1];
    [[NSCalendar currentCalendar]
     dateFromComponents:components];
    NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *yesterdayString = [dateFormatter stringFromDate:yesterday];
    
    [mycell.callerID setFrame:CGRectMake(mycell.callerImage.frame.origin.x + mycell.callerImage.frame.size.width + 10, mycell.callerImage.frame.origin.y, 150, 20)];
    mycell.callerID.text =[NSString stringWithFormat:@"%@",[[allCallArray objectAtIndex:indexPath.row]valueForKey:@"callerid"]];
    mycell.callerID.text = [mycell.callerID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [mycell.date setFrame:CGRectMake(mycell.callerID.frame.origin.x + mycell.callerID.frame.size.width + 20, mycell.callerID.frame.origin.y - 5, 100, 20)];
    if ([[dateFormatter stringFromDate:[NSDate date]] isEqualToString:callLogDate])
        mycell.date.text = NSLocalizedString(@"Today", nil) ;
    else
        if ([yesterdayString isEqualToString:callLogDate])
            mycell.date.text = NSLocalizedString(@"Yesterday", nil) ;
    
    mycell.time.text = CallLogTime;
    [mycell.time setFrame:CGRectMake(mycell.date.frame.origin.x, mycell.date.frame.origin.y + mycell.date.frame.size.height, 100, 20)];
    return mycell;
}

#pragma mark - Prepare For Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    CallViewController *callViewController = (CallViewController*)segue.destinationViewController;
    DateAndTimeTableViewCell *cell = (DateAndTimeTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    callViewController.selectedNumber = cell.callerID.text;
    callViewController.detailsOfCallLogs = [self.googlePlacesArrayFromAFNetworking objectAtIndex:indexPath.row];
}

-(void) segmentedControlValueDidChange:(UISegmentedControl *)segment
{
    [self loadCallLogs];
    [self updateTable];
}
@end

