//
//  SWActiveUser.h
//  SiempreWifi
//
//  Created by Jayesh on 26/03/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWActiveUser : NSObject
-(BOOL) checkActive;
@end
