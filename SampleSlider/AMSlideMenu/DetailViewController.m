//
//  DetailViewController.m
//  SampleSlider
//
//  Created by Jayesh on 12/5/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *msgBody;
@property (weak, nonatomic) IBOutlet UILabel *time;

@end

@implementation DetailViewController

@synthesize reply,msgView,msgType,textView,promptLabel;
- (void)viewDidLoad
{
    
    // Do any additional setup after loading the view.
    reply.hidden = NO;
    reply.layer.cornerRadius = 4;
    msgView.layer.cornerRadius = 4;
    [_msgBody sizeToFit];

    
    NSLog(@"Detaisls ----->%@",self.detailsOfMSg);
    NSString *callerId =[[self.detailsOfMSg objectForKey:@"fields"]valueForKey:@"callerid"];
    NSString* callerIdStr = [NSString stringWithFormat:@"%@", callerId];
    self.phoneNumber.text = callerIdStr;
    
    self.textView.text = [[self.detailsOfMSg objectForKey:@"fields"]valueForKey:@"body"];
    
    NSArray *time = [[[self.detailsOfMSg objectForKey:@"fields"]valueForKey:@"time"] componentsSeparatedByString:@" "];
    
    
    self.time.text = [time objectAtIndex:0];
    self.time.text = [[self.time.text stringByAppendingString: @" "] stringByAppendingString:[time objectAtIndex:1]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[[self.detailsOfMSg objectForKey:@"fields"]valueForKey:@"callerid"] forKey:@"phNumber"];
    [defaults setBool:YES forKey:@"REPLY"];
    
    if([[defaults objectForKey:@"msgType"]isEqualToString:@"sent"])
    {
        self.msgType.text = NSLocalizedString(@"Sent:",nil);
        reply.hidden = YES;
        self.promptLabel.text = @"To";
    }
    else
    {
        self.msgType.text = NSLocalizedString(@"Received:", nil);
        self.promptLabel.text = @"From";
    }
    [super viewDidLoad];
    
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [leftButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backBarButtonItem_clicked) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
}

-(void)backBarButtonItem_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
