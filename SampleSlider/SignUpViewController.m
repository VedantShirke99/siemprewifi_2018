//
//  SignUpViewController.m
//  SiempreWifi
//
//  Created by Priya on 22/08/18.
//  Copyright © 2018 WhiteSnow. All rights reserved.
//

#import "SignUpViewController.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "HomeViewController.h"
#import "SignInViewController.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "GAIDictionaryBuilder.h"

@interface SignUpViewController ()<MBProgressHUDDelegate>

@end

@implementation SignUpViewController
@synthesize usernameTextField, passwordTextField, reEnterPasswordTextField;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.reEnterPasswordTextField.delegate = self;
    
    self.usernameTextField.returnKeyType = UIReturnKeyNext;
    self.passwordTextField.returnKeyType = UIReturnKeyNext;
    self.reEnterPasswordTextField.returnKeyType = UIReturnKeyGo;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboardOnTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
}
- (IBAction)signUp:(id)sender {
    
    if([usernameTextField.text isEqualToString:@""] && [passwordTextField.text isEqualToString:@""] && [reEnterPasswordTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter all fields.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    } else if([usernameTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter username.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    } else if([passwordTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter password.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    } else if([reEnterPasswordTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please re-enter password.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    } else if (![self validateEmail:usernameTextField.text]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please Enter valid Email ID",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    } else if(![passwordTextField.text isEqualToString:reEnterPasswordTextField.text]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Passwords do not match. Please try again.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    } else if([passwordTextField.text length] < 8){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Password must contain at least 8 characters.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    } else {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        hud.delegate = self;
        hud.labelText = NSLocalizedString(@"Signing Up",nil);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *params = @{
                                 @"email": usernameTextField.text,
                                 @"password": passwordTextField.text
                                 };
        
        //        NSString  *serverAddress = [URL_LINk stringByAppendingString:@"signUp"];
    http://192.168.10.251:9000/
        [manager POST:@"https://www.siempre-wifi.com/signUp/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            [manager POST:@"http://192.168.10.251:9000/signUp/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            if([[responseObject valueForKey:@"status"] isEqualToString:@"failed"]){
                [hud hide:YES];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign Up Alert" message:[responseObject valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            } else {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *userName = usernameTextField.text;
                NSString *password = passwordTextField.text;
                NSInteger flag = true;
                userName = [userName lowercaseString];
                
                [defaults setObject:userName forKey:@"userName"];
                [defaults setObject:password forKey:@"password"];
                [defaults setInteger:flag forKey:@"flag"];
                [defaults synchronize];
                
                AppDelegate *delegate = [UIApplication sharedApplication].delegate;
                [delegate registrationForTwilio];
                [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
                [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
                
                [hud hide:YES];
                
                NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                              nil];
                [Flurry logEvent:@"User LoggedIn" withParameters:flurryParams];
                
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createAppView] build]];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Message Received "     // Event category (required)
                                                                      action:@"None"  // Event action (required)
                                                                       label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                                                                       value:nil] build]];
                
                NSString *deviceToken = [defaults objectForKey:@"deviceTokenStr"];
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"updateApnsRegId?email_ID=%@&regId=%@&device_type=iphone",[usernameTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[deviceToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     NSLog(@"Saved");
                     //                     [self dismissViewControllerAnimated:YES completion:nil];
                     //                     [self performSegueWithIdentifier:@"SignUpToMainVC" sender:self];
                     //                     UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                     //                     UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"MainStoryBoard"];
                     //                     [self presentViewController:vc animated:YES completion:nil ];
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Not Saved.....: %@", error);
                     
                 }];
                
                
                NSLog(@"Success...!!!");
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign Up Alert" message:@"Something went wrong. We are fixing it on a rocket speed." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [hud hide:YES];
        }];
        
    }
}

-(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SignInMainStoryBoard"];
    //    [self presentViewController:vc animated:YES completion:nil ];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.usernameTextField){
        [textField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    } else if(textField == self.passwordTextField){
        [textField resignFirstResponder];
        [self.reEnterPasswordTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}
- (IBAction)backgroundTap:(id)sender {
    [self.view endEditing:YES];
}

-(void)hideKeyboardOnTap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

