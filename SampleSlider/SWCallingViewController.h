//
//  SWCallingViewController.h
//  SiempreWifi
//
//  Created by Jayesh on 30/03/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwilioClient.h"
@interface SWCallingViewController : UIViewController<TCDeviceDelegate,TCConnectionDelegate>
@property (strong,nonatomic) TCConnection* _connection;
@property UIImageView *dialerImagerView;
@property UIImageView *handsfreeImagerView;
@property UILabel *callingTextLabel;
@property UILabel *calledNumberLabel;
@property UILabel *timerLabel;
@property UIButton *endButton;
@property UIButton *dialerButton;
@property UIButton *handsfreeButtonWhite;
@property UIButton *handsfreeButtonBlack;
@property UIButton *backButton;

-(void) callConnected;

@end
