//
//  SWCallingViewController.m
//  SiempreWifi
//
//  Created by Jayesh on 30/03/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "SWCallingViewController.h"
#import "SWCircleLineButton.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "Flurry.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "HomeViewController.h"
@interface SWCallingViewController ()

@end

@implementation SWCallingViewController
{
    __block UIView *dialerView;
    UILabel *digitsTypedTextFiled;
    BOOL iPhone4;
    BOOL isSpeakerOn;
    
    NSTimeInterval callStartTime;
    NSString *startTime;
    NSString *userName;
    
    CGRect dialerImageRect_org;
    CGRect handsfreeImageRect_org;
    CGRect callingLabelRect_org;
    CGRect displyTimerRect_org;
    CGRect calledNumberRect_org;
    CGRect dialerViewRect_org;
}
@synthesize calledNumberLabel;
@synthesize callingTextLabel;
@synthesize endButton;
@synthesize timerLabel;
@synthesize dialerImagerView;
@synthesize _connection;
@synthesize dialerButton;
@synthesize handsfreeButtonWhite;
@synthesize handsfreeButtonBlack;
@synthesize backButton;

-(void) loadView
{
    [super loadView];
    
    if (self.view.frame.size.width == 320 && self.view.frame.size.height  == 480) iPhone4 = YES; else iPhone4 = NO;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
    
    self.endButton = [[UIButton alloc]initWithFrame:CGRectMake(40, self.view.frame.size.height-90, self.view.frame.size.width-80, 60)];
    [self.endButton setTitle:NSLocalizedString(@"END",nil)  forState:UIControlStateNormal];
    [self.endButton setBackgroundColor:[UIColor colorWithRed:205.0f/256.0f green:54.0f/256.0f blue:63.0f/256.0f alpha:1.0f]];
    [self.endButton addTarget:self action:@selector(endButton_clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.endButton];
    isSpeakerOn = NO;
    
    //DIALER BUTTION
    
    //    self.dialerButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - 12.5,self.endButton.frame.origin.y - 50 , 25, 35)];
    
    self.handsfreeButtonWhite = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - 70,self.endButton.frame.origin.y - 50 , 25, 35)];
    [self.handsfreeButtonWhite setBackgroundImage:[UIImage imageNamed:@"handsfree-white.png"] forState:UIControlStateNormal];
    [self.handsfreeButtonWhite addTarget:self action:@selector(handsfreeButton_clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:handsfreeButtonWhite];
    
    self.handsfreeButtonBlack = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - 70,self.endButton.frame.origin.y - 50 , 25, 35)];
    [self.handsfreeButtonBlack setBackgroundImage:[UIImage imageNamed:@"handsfree.png"] forState:UIControlStateNormal];
    [self.handsfreeButtonBlack addTarget:self action:@selector(handsfreeButton_clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:handsfreeButtonBlack];
    [self.handsfreeButtonBlack setHidden:YES];
    
    self.dialerButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 + 35,self.endButton.frame.origin.y - 50 , 25, 35)];
    [self.dialerButton setBackgroundImage:[UIImage imageNamed:@"dilaer_icon.png"] forState:UIControlStateNormal];
    [self.dialerButton addTarget:self action:@selector(dialerButton_clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:dialerButton];
    
    
    //AREA FOR DIALLER
    
    
    //DIALER IMAGE VIEW
    dialerImageRect_org = CGRectMake(self.view.frame.size.width/2 -75, 75, 150, 150);
    self.dialerImagerView = [[UIImageView alloc]initWithFrame:dialerImageRect_org];
    [self.dialerImagerView setImage:[UIImage imageNamed:@"homeCall.png"]];
    [self.dialerImagerView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:self.dialerImagerView];
    
    
    //CALLED NUMBER LABEL
    calledNumberRect_org = CGRectMake(self.view.frame.size.width/2-100, self.dialerImagerView.frame.origin.y + self.dialerImagerView.frame.size.height +5, 200, 50);
    self.calledNumberLabel  = [[UILabel alloc]initWithFrame:calledNumberRect_org];
    [self.calledNumberLabel setTextAlignment:NSTextAlignmentCenter];
    [self.calledNumberLabel setTextColor:[UIColor whiteColor]];
    [self.calledNumberLabel setText:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"calledPhoneNumber" ]]];
    [self.calledNumberLabel setFont:[UIFont fontWithName:@"OpenSans" size:24]];
    [self.view addSubview:self.calledNumberLabel];
    
    //CALLING TEXT LABEL
    callingLabelRect_org = CGRectMake(self.view.frame.size.width/2-100, self.calledNumberLabel.frame.origin.y + self.calledNumberLabel.frame.size.height +5, 200, 50);
    self.callingTextLabel = [[UILabel alloc]initWithFrame:callingLabelRect_org];
    [self.callingTextLabel setTextAlignment:NSTextAlignmentCenter];
    [self.callingTextLabel setTextColor:[UIColor whiteColor]];
    [self.callingTextLabel setText:@"Calling..."];
    [self.callingTextLabel setFont:[UIFont fontWithName:@"OpenSans" size:20]];
    [self.view addSubview:self.callingTextLabel];
    
    //TIMER LABEL
    displyTimerRect_org = CGRectMake(self.view.frame.size.width/2-100, self.calledNumberLabel.frame.origin.y + self.calledNumberLabel.frame.size.height +5, 200, 50);
    self.timerLabel = [[UILabel alloc] initWithFrame:displyTimerRect_org];
    [self.timerLabel setTextAlignment:NSTextAlignmentCenter];
    [self.timerLabel setTextColor:[UIColor whiteColor]];
    [self.timerLabel setFont:[UIFont fontWithName:@"OpenSans" size:20]];
    [self.timerLabel setText:@""];
    [self.view addSubview:self.timerLabel];
    
    
    //BACK BUTTON
    self.backButton = [[UIButton alloc]init];
    [self.backButton setFrame:CGRectMake(self.endButton.frame.origin.x + self.endButton.frame.size.width - 50, self.endButton.frame.origin.y - 50, 50, 30)];
    [self.backButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.backButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButton_clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton setHidden:YES];
    [self.view addSubview:self.backButton];
    
    [self createDialer];
    
}

-(void)backButton_clicked
{
    NSLog(@"Back Button Pressed");
    [self.dialerButton setHidden:NO];
    
    [UIView animateWithDuration:0.30 animations:^
    {
        [dialerView setAlpha:0.0f];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.30 animations:^{
          [self dialerAppear];
        }];
    }];
    [dialerView setHidden:YES];
    [self.backButton setHidden:YES];
    
}
-(void)dialerButton_clicked
{
    [dialerView setHidden:NO];
    
    [UIView animateWithDuration:0.30 animations:^{
         [self dialerAppear];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.30 animations:^{
                [dialerView setAlpha:1.0f];
        }];

    }];
    
    [self.dialerButton setHidden:YES];
    [self.backButton setHidden:NO];
}
-(void)dialerAppear
{

    if (dialerView.hidden)
    {
        [self.dialerImagerView setFrame:dialerImageRect_org];
        [self.calledNumberLabel setFrame:calledNumberRect_org];
        [self.callingTextLabel setFrame:callingLabelRect_org];
        [self.timerLabel setFrame:displyTimerRect_org];
    }
    else
    {
        [self.dialerImagerView setFrame:CGRectMake(30, 60, 80, 80)];
        [self.calledNumberLabel setFrame:CGRectMake(120, 65, self.view.frame.size.width - 130, 50)];
        [self.callingTextLabel setFrame:CGRectMake(120, 100, 200, 40)];
        [self.timerLabel setFrame:CGRectMake(120, 100, 200, 40)];
    }

}

-(void)endButton_clicked
{
//    [self dismissViewControllerAnimated:true completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    AppDelegate *delegate =[UIApplication sharedApplication].delegate;
    [delegate.phone disconnectAll];
    [self creditCalculation];
}

-(void) creditCalculation
{
    NSLog(@"CREDITS CALCULATION");
    
    NSLog(@"Call End Button --->%@",startTime);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    userName = [defaults objectForKey:@"userName"];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    startTime = [DateFormatter stringFromDate:[NSDate date]];
    [defaults setObject:self.timerLabel.text forKey:@"displayTimer"];
    [defaults setObject:calledNumberLabel.text forKey:@"calledNumber"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString  *serverAddress =[URL_LINk stringByAppendingString:[NSString stringWithFormat:@"update_call_log?email_ID=%@&time=%@&duration=%@&caller_ID=%@&type=dialed",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.timerLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[calledNumberLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    serverAddress = [serverAddress stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                  [NSString stringWithFormat:@"Dialed"],@"CALL TYPE",
                                  [NSString stringWithFormat:@"%@",calledNumberLabel.text],@"CALL RECEIPEINT",
                                  nil];
    
    [Flurry logEvent:@"Call" withParameters:flurryParams];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:
                    [NSString stringWithFormat:@"CALL DAILED TO : %@",calledNumberLabel.text]     // Event category (required)
                                                          action:@"CALL TAKEN"  // Event action (required)
                                                           label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                                                           value:nil] build]];
    
    NSLog(@"startTime------>%@",serverAddress);
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Saved");
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Not Saved.....: %@", error);
         
     }];
    NSUserDefaults *durationTime = [NSUserDefaults standardUserDefaults];
    [durationTime setObject:self.timerLabel.text forKey:@"durationTime"];

    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)update_Timer
{
    [self.callingTextLabel setHidden:YES];
    int minutes;
   
    NSTimeInterval currentTimeInterval = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elapsedTime = currentTimeInterval - callStartTime;
    minutes= (int)(elapsedTime / 60.0);
    int seconds = (int)(elapsedTime - (minutes*60));
    self.timerLabel.text = [NSString stringWithFormat:@"%02u:%02u",minutes,seconds];
    [self performSelector:@selector(update_Timer) withObject:self afterDelay:0.1];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *credits = [defaults objectForKey:@"credits"];
        
        if([credits integerValue] == minutes)
        {
            AppDelegate *delegate =[UIApplication sharedApplication].delegate;
            [delegate.phone disconnectAll];
//            [self dismissViewControllerAnimated:true completion:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }

}
-(void) callConnected
{
    callStartTime  = [NSDate timeIntervalSinceReferenceDate];
    [callingTextLabel setHidden:YES];
    self.timerLabel.text =@"";
    [self performSelectorOnMainThread:@selector(update_Timer) withObject:self waitUntilDone:NO];
}
-(void)dailerView
{
    callStartTime  = [NSDate timeIntervalSinceReferenceDate];
    [callingTextLabel setHidden:YES];
    self.timerLabel.text =@"";
    [self performSelectorOnMainThread:@selector(update_Timer) withObject:self waitUntilDone:NO];    
}

-(void)createDialer
{
    dialerViewRect_org = CGRectMake(70,150,self.view.frame.size.width-120, self.view.frame.size.height - 290);
    dialerView = [[UIView alloc]initWithFrame:dialerViewRect_org];

    [self.view addSubview:dialerView];
    
    [dialerView setBackgroundColor:[UIColor clearColor]];
    
    
    digitsTypedTextFiled = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, dialerView.frame.size.width, 50)];
    [digitsTypedTextFiled setTextAlignment:NSTextAlignmentCenter];
    [digitsTypedTextFiled setBackgroundColor:[UIColor clearColor]];
    [digitsTypedTextFiled setTextColor:[UIColor whiteColor]];
    [digitsTypedTextFiled setFont:[UIFont fontWithName:@"OpenSans" size:20]];
    [dialerView addSubview:digitsTypedTextFiled];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, digitsTypedTextFiled.frame.origin.y-10 + digitsTypedTextFiled.frame.size.height, dialerView.frame.size.width,1)];
    [lineView setBackgroundColor:[UIColor lightGrayColor]];
    [dialerView addSubview:lineView];
    
    UIView *keypadView = [[UIView alloc]initWithFrame:CGRectMake(0, digitsTypedTextFiled.frame.size.height+2, dialerView.frame.size.width, dialerView.frame.size.height-digitsTypedTextFiled.frame.size.height-2)];
    [keypadView setBackgroundColor:[UIColor clearColor]];
    
    [dialerView addSubview:keypadView];
    
    
    int width;
    if (iPhone4)
    {
        [dialerView setFrame:CGRectMake(60,self.dialerImagerView.frame.origin.y+self.dialerImagerView.frame.size.height/2+20,self.view.frame.size.width-120,self.view.frame.size.height/2-10)];
        
        [digitsTypedTextFiled setFrame:CGRectMake(0, 0, dialerView.frame.size.width, 50)];
        CGFloat keypadHeight = dialerView.frame.size.height-digitsTypedTextFiled.frame.size.height-20;
        [keypadView setFrame:CGRectMake(dialerView.frame.size.width/2 - (3*keypadHeight/4)/2 ,digitsTypedTextFiled.frame.size.height+2, 3*keypadHeight/4, keypadHeight)];
        
        width= 30;
        
    }else
    {
        [dialerView setFrame:CGRectMake(60,self.dialerImagerView.frame.origin.y+self.dialerImagerView.frame.size.height/2-10,self.view.frame.size.width-120,self.view.frame.size.height/2)];
        
        [digitsTypedTextFiled setFrame:CGRectMake(0, 0, dialerView.frame.size.width, 50)];
        CGFloat keypadHeight = dialerView.frame.size.height-digitsTypedTextFiled.frame.size.height-2;
        [keypadView setFrame:CGRectMake(dialerView.frame.size.width/2 - (3*keypadHeight/4)/2 ,digitsTypedTextFiled.frame.size.height+2, 3*keypadHeight/4, keypadHeight)];
        width = 4*(keypadView.frame.size.width/3)/5;
    }
    CGFloat dialerWidth = keypadView.frame.size.width;
    CGFloat dialerHeight = keypadView.frame.size.height;
    CGFloat buttonWidth = dialerWidth/3;
    CGFloat buttonHeight = dialerHeight/4;
    buttonWidth = buttonHeight;
    int hpadding = 2;
    int vpadding = 2;
    for (int i=1; i<=12; i++)
    {
        SWCircleLineButton *button = [[SWCircleLineButton alloc]init];
        [button setTitle:[NSString stringWithFormat:@"%d",i] forState:UIControlStateNormal];
        switch (i)
        {
            case 1:
                [button setFrame:CGRectMake(0+2*hpadding, 0+vpadding, width, width)];
                break;
            case 2:
                [button setFrame:CGRectMake(buttonWidth+hpadding, 0+vpadding, width, width)];
                break;
            case 3:
                [button setFrame:CGRectMake(2*buttonWidth, 0+vpadding, width, width)];
                break;
            case 4:
                [button setFrame:CGRectMake(0+2*hpadding,buttonHeight+vpadding, width, width)];
                break;
            case 5:
                [button setFrame:CGRectMake(buttonWidth+hpadding, buttonHeight+vpadding, width, width)];
                break;
            case 6:
                [button setFrame:CGRectMake(2*buttonWidth, buttonHeight+vpadding, width, width)];
                break;
            case 7:
                [button setFrame:CGRectMake(0+2*hpadding, 2*buttonHeight+vpadding, width, width)];
                break;
            case 8:
                [button setFrame:CGRectMake(buttonWidth+hpadding, 2*buttonHeight+vpadding, width, width)];
                break;
            case 9:
                [button setFrame:CGRectMake(2*buttonWidth, 2*buttonHeight+vpadding, width, width)];
                break;
            case 10:
                [button setFrame:CGRectMake(0+2*hpadding,3*buttonHeight+vpadding, width, width)];
                [button setTitle:@"" forState:UIControlStateNormal];
                [button setImage:[UIImage imageNamed:@"STAR.png"] forState:UIControlStateNormal];
                break;
            case 11:
                [button setFrame:CGRectMake(buttonWidth+hpadding,3*buttonHeight+vpadding, width, width)];
                [button setTitle:@"0" forState:UIControlStateNormal];
                break;
            case 12:
                [button setFrame:CGRectMake(2*buttonWidth,3*buttonHeight+vpadding, width, width)];
                [button setTitle:@"#" forState:UIControlStateNormal];
                break;
            default:
                break;
        }
        
        [button addTarget:self action:@selector(dialerButtonClikedAtIndex:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundColor:[UIColor clearColor]];
        button.tag = i;
        [button.titleLabel setTextColor:[UIColor whiteColor]];
        [button drawCircleButton:[UIColor whiteColor]];
        [button.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:18]];
        [keypadView addSubview:button];
        
        dialerView.hidden = YES;
    }
    [self.view bringSubviewToFront:dialerView];
    [dialerView setAlpha:0.0f];
}

-(void) dialerButtonClikedAtIndex:(UIButton *)button
{
    if ( !digitsTypedTextFiled.text || [digitsTypedTextFiled.text isEqualToString:@"(null)"] )
    {
        digitsTypedTextFiled.text=@"";
    }
    
        if (button.tag ==11)
        {
            
            [self._connection sendDigits:@"0"];
            [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@0",digitsTypedTextFiled.text]];
        }else
            if (button.tag ==10)
            {
                
                [self._connection sendDigits:@"*"];
                [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@*",digitsTypedTextFiled.text]];
            }else
                if (button.tag ==12)
                {
                    
                    [self._connection sendDigits:@"#"];
                    [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@#",digitsTypedTextFiled.text]];
                }else
                {
                    [self._connection sendDigits:[NSString stringWithFormat:@"%ld",(long)button.tag]];
                    [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@%ld",digitsTypedTextFiled.text,(long)button.tag]];
                }
    
}
-(void)device:(TCDevice*)device didStopListeningForIncomingConnections:(NSError*)error
{
    NSLog(@"Stopped Listening");
    NSLog(@"Twilio Service Error : Twillio Server Disconnected");
}
-(void)connection:(TCConnection*)connection didFailWithError:(NSError*)error
{
    NSLog(@"Failed Due to Error: %@",error);
    
}

-(void)connectionDidDisconnect:(TCConnection *)connection
{
    NSLog(@"Disconnect....");
}

-(void)connectionDidStartConnecting:(TCConnection *)connection
{
    NSLog(@"Connecting........");
}

-(void)handsfreeButton_clicked{
    if(isSpeakerOn){
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord mode:AVAudioSessionModeVoiceChat options:AVAudioSessionCategoryOptionDuckOthers error:nil];
        isSpeakerOn = NO;
        [self.handsfreeButtonBlack setHidden:YES];
        [self.handsfreeButtonWhite setHidden:NO];
    } else {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord mode:AVAudioSessionModeVoiceChat options:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
        isSpeakerOn = YES;
        [self.handsfreeButtonBlack setHidden:NO];
        [self.handsfreeButtonWhite setHidden:YES];
    }
}
@end
