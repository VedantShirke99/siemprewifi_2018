//
//  CallViewController.h
//  SampleSlider
//
//  Created by Jayesh on 26/11/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCDevice.h"
#import "callingVieController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>

@interface CallViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,ABPeoplePickerNavigationControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,TCDeviceDelegate,TCConnectionDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate,CNContactPickerDelegate,CNContactViewControllerDelegate>
{
    UITextField* textFields;

    NSString* phoneNumber;
    callingVieController *callingView;
    
  
}
- (IBAction)phoneContact:(id)sender;
- (IBAction)additionSign:(id)sender;
@property (strong,nonatomic) NSString *selectedNumber;

@property (strong, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *creditSpinner;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *minuteUsedSpinner;
@property (weak, nonatomic) IBOutlet UIImageView *creditImg;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *creditUsedImg;
@property (nonatomic, retain) ABPeoplePickerNavigationController *contacts;
@property (nonatomic, retain) CNContactPickerViewController *contact;

@property (nonatomic) callingVieController *callingView;

@property (retain, nonatomic) IBOutlet UITextField *numberField;
@property (nonatomic,strong)NSArray *countryCodeData;

@property (strong, nonatomic) IBOutlet UILabel *countrySelected;

@property (strong, nonatomic) IBOutlet UIButton *downKeyButton;

@property (weak, nonatomic) IBOutlet UITextField *countryCodeTxtField;

@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@property (strong, nonatomic) IBOutlet UITextField *countryCodeTextField;

@property(strong, retain)IBOutlet TCDevice* phone;
@property(strong, retain)IBOutlet TCConnection* phConnection;

- (IBAction)tapGestureKeyboard:(id)sender;

- (IBAction)countryCodePicker:(id)sender;
- (IBAction)InAppClicked:(id)sender;

-(IBAction)dialButtonPressed:(id)sender;
-(IBAction)hangupButtonPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *creditsUsedImageView;
@property (strong, nonatomic) IBOutlet UIImageView *creditsRemainingImageView;



- (IBAction)hidePickerView:(id)sender;

@property (strong,nonatomic)NSDictionary *detailsOfCallLogs;

@property (weak, nonatomic) IBOutlet UIView *pickerViewContainer;

@property (weak, nonatomic) IBOutlet UILabel *creditsBalance;

@property (weak, nonatomic) IBOutlet UILabel *callCreditsUsed;

@property(nonatomic,assign) id<TCConnectionDelegate> delegate;

@end
