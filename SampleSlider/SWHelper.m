//
//  SWHelper.m
//  SiempreWifi
//
//  Created by Priya on 28/12/18.
//  Copyright © 2018 WhiteSnow. All rights reserved.
//

#import "SWHelper.h"
#import "UIKit/UIKit.h"

@implementation SWHelper


-(void)openAppSettingsForPermission{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
