
//
//  callingVieController.m
//  SampleSlider
//
//  Created by Jayesh on 12/1/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "callingVieController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "CallViewController.h"
#import "TableViewController.h"
#import "HomeViewController.h"
#import "Flurry.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "SWCircleLineButton.h"
@interface callingVieController ()<TCDeviceDelegate,TCConnectionDelegate>


@end

@implementation callingVieController
{
    bool start;
    NSTimeInterval time;
    NSString *startTime;
    NSString *userName;
    NSString *token;
    NSString *displayTimerStatus;
    UIButton *dialerButton;
    UIView *dialerView;
    UILabel *digitsTypedTextFiled;
    
    
    UIImageView *dialerImagerView;
    UILabel *timerLabel;
    UILabel *calledPhNumber;
    UILabel *callingTextLabel;
    
    BOOL iPhone4;
    CGRect dialerImageRect_org;
    CGRect callingLabelRect_org;
    CGRect displyTimerRect_org;
    CGRect calledNumberRect_org;
}
@synthesize dialerImage;
@synthesize calledPhoneNumber,calledNumber,displayTimer,endbtn;
@synthesize dialerButton;
@synthesize _connection;
-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = NO;
}
-(void) loadView
{
    [super loadView];
    dialerImagerView = [[UIImageView alloc]init];
    callingTextLabel  = [[UILabel alloc]init];
    calledPhNumber = [[UILabel alloc]init];
    timerLabel = [[UILabel alloc]init];
    //For iPhone4
    if (self.view.frame.size.width == 320 && self.view.frame.size.height  == 480)
    {
        iPhone4 =YES;
    }
    else
        iPhone4 = NO;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];

    self.displayTimer = [[UILabel alloc]initWithFrame:CGRectMake(60, 310, self.view.frame.size.width-120, 40)];
    [self.view addSubview:self.displayTimer];
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];

    
    [self.endbtn setTitle:NSLocalizedString(@"END",nil) forState:UIControlStateNormal];

    [self.endbtn.layer setCornerRadius:2];
    
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = YES;
   [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dailerView) name:@"dailerView" object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    calledNumber.text = [defaults objectForKey:@"calledPhoneNumber"];
    self.displayTimer.text = @"";
    timerLabel.text = self.displayTimer.text;
    start = false;
    [endbtn setTitle:NSLocalizedString(@"END", nil)  forState:UIControlStateNormal];
    
    if (start == false)
    {
        start = true;
    }
    else
    {
    start = false;
    }
     [self createDialer];
    
    
    if (iPhone4)
    {
        [dialerImagerView setFrame:CGRectMake(self.view.frame.size.width/2-60, 100, 120, 120)];
        [calledPhNumber setFrame:CGRectMake(60, 230, self.view.frame.size.width-120,50)];
        [callingTextLabel setFrame:CGRectMake(60,310,self.view.frame.size.width-120, 40)];
        [timerLabel setFrame:CGRectMake(60, 310, self.view.frame.size.width-120, 40)];
    }
    else
    {
        [dialerImagerView setFrame:CGRectMake(self.view.frame.size.width/2-75, 100, 150, 150)];
        [callingTextLabel setFrame:CGRectMake(60,230,self.view.frame.size.width-60, 50)];
    }
    
    [calledPhNumber setTextAlignment:NSTextAlignmentCenter];
    [callingTextLabel setTextAlignment:NSTextAlignmentCenter];
    [timerLabel setTextAlignment:NSTextAlignmentCenter];
    
    
   
    [dialerImagerView setImage:[UIImage imageNamed:@"homeCall.png"]];
    [self.view addSubview:dialerImagerView];
    
    [calledPhNumber setText:@"+00-000-000-0000"];
    [calledPhNumber setTextColor:[UIColor whiteColor]];
    [self.view addSubview:calledPhNumber];
    
    [callingTextLabel setText:NSLocalizedString(@"Calling...", nil)];
    [self.view addSubview:callingTextLabel];
    [callingTextLabel setTextColor:[UIColor whiteColor]];
    
    [timerLabel setText:@"00:00:00"];
    [self.view addSubview: timerLabel];
    [timerLabel setTextColor:[UIColor whiteColor]];
    
}
-(void) updateDisplyTimer
{
    
    for (int i = 0; i<5; i++)
    {
        sleep(1);
        displayTimer.text = [NSString stringWithFormat:@"%@.",displayTimer.text];
        timerLabel.text = displayTimer.text;
        
    }
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"APPEARED");
}
-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
-(void)device:(TCDevice*)device didStopListeningForIncomingConnections:(NSError*)error
{
    NSLog(@"Stopped Listening");
    NSLog(@"Twilio Service Error : Twillio Server Disconnected");
}
-(void)connection:(TCConnection*)connection didFailWithError:(NSError*)error
{
    NSLog(@"Failed Due to Error: %@",error);

}

-(void)update_Timer
{
    [self.callingLabel setHidden:YES];
    int minutes;
    if (start == false)
    {
        return;
    }
    else
    {
        displayTimer.hidden = YES;
        if([displayTimerStatus isEqualToString:@"YES"])
        {
            displayTimer.hidden = NO;
            NSTimeInterval currebtTime = [NSDate timeIntervalSinceReferenceDate];
            NSTimeInterval elappedTime = currebtTime -time;
            
             minutes= (int)(elappedTime / 60.0);
            int seconds = (int)(elappedTime - (minutes*60));
            self.displayTimer.text = [NSString stringWithFormat:@"%02u:%02u",minutes,seconds];
            displayTimer.text = self.displayTimer.text;
        }
        
        
        [self performSelector:@selector(update_Timer) withObject:self afterDelay:0.1];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *credits = [defaults objectForKey:@"credits"];
        
        if([credits integerValue] == minutes){
            AppDelegate *delegate =[UIApplication sharedApplication].delegate;
            [delegate.phone disconnectAll];
//            [self dismissViewControllerAnimated:true completion:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }

   
}
- (void)didReceiveMemoryWarning
{
    calledPhoneNumber.text = calledNumber.text;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)timer
{
       [self performSelectorOnMainThread:@selector(timerOnMainThread) withObject:nil waitUntilDone:NO];
}
-(void)timerOnMainThread
{
    NSLog(@"Timmer Clicked");
    self.displayTimer.hidden = YES;
    
}
- (IBAction)callEndBtn:(id)sender
{
    NSLog(@"End Button CLicked");
    start = false;
//    [self dismissViewControllerAnimated:true completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    AppDelegate *delegate =[UIApplication sharedApplication].delegate;
    [delegate.phone disconnectAll];
    [self creditCalculation];

}

-(void) creditCalculation
{
    NSLog(@"Call End Button --->%@",startTime);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    userName = [defaults objectForKey:@"userName"];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    startTime = [DateFormatter stringFromDate:[NSDate date]];
    [defaults setObject:self.displayTimer.text forKey:@"displayTimer"];
    [defaults setObject:calledNumber.text forKey:@"calledNumber"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString  *serverAddress =[URL_LINk stringByAppendingString:[NSString stringWithFormat:@"update_call_log?email_ID=%@&time=%@&duration=%@&caller_ID=%@&type=dialed",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.displayTimer.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[calledNumber.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                 [NSString stringWithFormat:@"Dialed"],@"CALL TYPE",
                                 [NSString stringWithFormat:@"%@",calledNumber.text],@"CALL RECEIPEINT",
                                 nil];
    
    [Flurry logEvent:@"Call" withParameters:flurryParams];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:
                    [NSString stringWithFormat:@"CALL DAILED TO : %@",calledNumber.text]     // Event category (required)
                    action:@"CALL TAKEN"  // Event action (required)
                    label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                    value:nil] build]];
    
    NSLog(@"startTime------>%@",serverAddress);
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Saved");
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Not Saved.....: %@", error);
         
     }];
    NSUserDefaults *durationTime = [NSUserDefaults standardUserDefaults];
    [durationTime setObject:self.displayTimer.text forKey:@"durationTime"];
}
-(void)connectionDidDisconnect:(TCConnection *)connection
{
    NSLog(@"Disconnect....");
}

-(void)connectionDidStartConnecting:(TCConnection *)connection
{
    NSLog(@"Connecting........");
}

-(void)dailerView
{   dialerButton.hidden = NO;
    NSLog(@"Call Connected....");
    [callingTextLabel setHidden:YES];
    displayTimer.text =@"";
    timerLabel.text= self.displayTimer.text;
    [self performSelectorOnMainThread:@selector(update_Timer) withObject:self waitUntilDone:NO];
    displayTimerStatus =@"YES";
    time = [NSDate timeIntervalSinceReferenceDate];
   

}

-(void)createDialer
{
    [displayTimer setFrame:CGRectMake(calledPhoneNumber.frame.origin.y, calledPhoneNumber.frame.origin.y, calledPhoneNumber.frame.size.width, calledPhoneNumber.frame.size.height)];
 
    dialerView = [[UIView alloc]initWithFrame:CGRectMake(70,150,self.view.frame.size.width-120, self.view.frame.size.height - 290)];
    
    [self.view addSubview:dialerView];
    
    [dialerView setBackgroundColor:[UIColor clearColor]];
    
    
    digitsTypedTextFiled = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, dialerView.frame.size.width, 50)];
    [digitsTypedTextFiled setTextAlignment:NSTextAlignmentCenter];
    [digitsTypedTextFiled setBackgroundColor:[UIColor clearColor]];
    [digitsTypedTextFiled setTextColor:[UIColor whiteColor]];
    [digitsTypedTextFiled setFont:[UIFont fontWithName:@"OpenSans" size:20]];
    [dialerView addSubview:digitsTypedTextFiled];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, digitsTypedTextFiled.frame.origin.y-10 + digitsTypedTextFiled.frame.size.height, dialerView.frame.size.width,2)];
    [lineView setBackgroundColor:[UIColor lightGrayColor]];
    [dialerView addSubview:lineView];
    
    UIView *keypadView = [[UIView alloc]initWithFrame:CGRectMake(0, digitsTypedTextFiled.frame.size.height+2, dialerView.frame.size.width, dialerView.frame.size.height-digitsTypedTextFiled.frame.size.height-2)];
    [keypadView setBackgroundColor:[UIColor clearColor]];
    
    [dialerView addSubview:keypadView];
    

    int width;
    if (iPhone4)
    {
        [dialerView setFrame:CGRectMake(60,self.dialerImage.frame.origin.y+self.dialerImage.frame.size.height/2+20,self.view.frame.size.width-120,self.view.frame.size.height/2-10)];
        
        [digitsTypedTextFiled setFrame:CGRectMake(0, 0, dialerView.frame.size.width, 50)];
        CGFloat keypadHeight = dialerView.frame.size.height-digitsTypedTextFiled.frame.size.height-20;
        [keypadView setFrame:CGRectMake(dialerView.frame.size.width/2 - (3*keypadHeight/4)/2 ,digitsTypedTextFiled.frame.size.height+2, 3*keypadHeight/4, keypadHeight)];
        
        width= 30;
        
    }else
    {
        [dialerView setFrame:CGRectMake(60,self.dialerImage.frame.origin.y+self.dialerImage.frame.size.height/2-10,self.view.frame.size.width-120,self.view.frame.size.height/2)];
        
        [digitsTypedTextFiled setFrame:CGRectMake(0, 0, dialerView.frame.size.width, 50)];
        CGFloat keypadHeight = dialerView.frame.size.height-digitsTypedTextFiled.frame.size.height-2;
        [keypadView setFrame:CGRectMake(dialerView.frame.size.width/2 - (3*keypadHeight/4)/2 ,digitsTypedTextFiled.frame.size.height+2, 3*keypadHeight/4, keypadHeight)];
        width = 4*(keypadView.frame.size.width/3)/5;
    }
    CGFloat dialerWidth = keypadView.frame.size.width;
    CGFloat dialerHeight = keypadView.frame.size.height;
    CGFloat buttonWidth = dialerWidth/3;
    CGFloat buttonHeight = dialerHeight/4;
    buttonWidth = buttonHeight;
    int hpadding = 2;
    int vpadding = 2;
    for (int i=1; i<=12; i++)
    {
        SWCircleLineButton *button = [[SWCircleLineButton alloc]init];
        [button setTitle:[NSString stringWithFormat:@"%d",i] forState:UIControlStateNormal];
        switch (i)
        {
            case 1:
                [button setFrame:CGRectMake(0+2*hpadding, 0+vpadding, width, width)];
                break;
            case 2:
                [button setFrame:CGRectMake(buttonWidth+hpadding, 0+vpadding, width, width)];
                break;
            case 3:
                [button setFrame:CGRectMake(2*buttonWidth, 0+vpadding, width, width)];
                break;
            case 4:
                [button setFrame:CGRectMake(0+2*hpadding,buttonHeight+vpadding, width, width)];
                break;
            case 5:
                [button setFrame:CGRectMake(buttonWidth+hpadding, buttonHeight+vpadding, width, width)];
                break;
            case 6:
                [button setFrame:CGRectMake(2*buttonWidth, buttonHeight+vpadding, width, width)];
                break;
            case 7:
                [button setFrame:CGRectMake(0+2*hpadding, 2*buttonHeight+vpadding, width, width)];
                break;
            case 8:
                [button setFrame:CGRectMake(buttonWidth+hpadding, 2*buttonHeight+vpadding, width, width)];
                break;
            case 9:
                [button setFrame:CGRectMake(2*buttonWidth, 2*buttonHeight+vpadding, width, width)];
                break;
            case 10:
                [button setFrame:CGRectMake(0+2*hpadding,3*buttonHeight+vpadding, width, width)];
                [button setTitle:@"*" forState:UIControlStateNormal];
                break;
            case 11:
                [button setFrame:CGRectMake(buttonWidth+hpadding,3*buttonHeight+vpadding, width, width)];
                [button setTitle:@"0" forState:UIControlStateNormal];
                break;
            case 12:
                [button setFrame:CGRectMake(2*buttonWidth,3*buttonHeight+vpadding, width, width)];
                [button setTitle:@"#" forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    
        [button addTarget:self action:@selector(dialerButtonClikedAtIndex:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundColor:[UIColor clearColor]];
         button.tag = i;
        [button.titleLabel setTextColor:[UIColor whiteColor]];
        [button drawCircleButton:[UIColor whiteColor]];
        [keypadView addSubview:button];
        
        dialerView.hidden = YES;
        
        
    }
    
    [self.view bringSubviewToFront:dialerView];
    
    [dialerButton setHidden:NO];
    
    //[dialerImage removeFromSuperview];
    
    dialerImageRect_org = CGRectMake(self.view.frame.size.width/2 - 75, 100, 150, 150);
    
  
    calledNumberRect_org = CGRectMake(10,260,self.view.frame.size.width-20,50);

    

    callingLabelRect_org =  CGRectMake(10, 310, self.view.frame.size.width-20,30);

    

    displyTimerRect_org = CGRectMake(10, 310, self.view.frame.size.width-20,30);

    

}
-(void) dialbackspace
{
    if ([digitsTypedTextFiled.text length]>0)
    {
        digitsTypedTextFiled.text = [digitsTypedTextFiled.text substringToIndex:[digitsTypedTextFiled.text length] - 1];
    }
}
-(void) dialerButtonClikedAtIndex:(UIButton *)button
{
    if ( !digitsTypedTextFiled.text || [digitsTypedTextFiled.text isEqualToString:@"(null)"] )
    {
        digitsTypedTextFiled.text=@"";
    }
    else
        if (button.tag ==11)
        {
          
            [self._connection sendDigits:@"0"];
            [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@0",digitsTypedTextFiled.text]];
        }else
            if (button.tag ==10)
            {
                
                [self._connection sendDigits:@"*"];
                [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@*",digitsTypedTextFiled.text]];
            }else
                if (button.tag ==12)
                {
                    
                    [self._connection sendDigits:@"#"];
                    [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@#",digitsTypedTextFiled.text]];
                }else
                    {
                        [self._connection sendDigits:[NSString stringWithFormat:@"%ld",(long)button.tag]];
                        [digitsTypedTextFiled setText:[NSString stringWithFormat:@"%@%ld",digitsTypedTextFiled.text,(long)button.tag]];
                    }
    
}

- (IBAction)dialerButtonCLicked:(id)sender
{
    NSLog(@"dialerButtonCLicked");
    dialerView.Hidden = !dialerView.isHidden;
    
    if (dialerView.hidden)
    {
    [dialerImagerView setFrame:dialerImageRect_org];
    [callingTextLabel setFrame:calledNumberRect_org];
    [calledPhNumber setFrame:calledNumberRect_org];
    [timerLabel setFrame:displyTimerRect_org];
    }
    else
    {
    [dialerImagerView setFrame:CGRectMake(30, 60, 80, 80)];
    [calledPhNumber setFrame:CGRectMake(120, 65, self.view.frame.size.width - 130, 50)];
    [callingTextLabel setFrame:CGRectMake(120, 100, 200, 40)];
    [timerLabel setFrame:CGRectMake(120, 100, 200, 40)];
    }
    
}

@end
