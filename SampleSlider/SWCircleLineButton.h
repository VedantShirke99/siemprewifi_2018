//
//  SWCircleLineButton.h
//  SiempreWifi
//
//  Created by Jayesh on 28/03/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWCircleLineButton : UIButton
@property (nonatomic, strong) CAShapeLayer *circleLayer;
@property (nonatomic, strong) UIColor *colour;
- (void)drawCircleButton:(UIColor *)colour;
@end
