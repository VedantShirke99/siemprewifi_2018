//
//  AppLinkController.m
//  SampleSlider
//
//  Created by Jayesh on 27/11/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "AppLinkController.h"

@interface AppLinkController ()

@end

@implementation AppLinkController



NSArray *applist,*appIcons,*checkPath,*downloadlinks;
int i,j;


- (void)viewDidLoad
{
    [super viewDidLoad];
    applist = [NSArray arrayWithObjects:/*@"WAZE", @"GOOGLE MAPS",*/@"APPLE MAPS",@"COASTA RICA TRAVEL GUIDE",@"PANAMA TRAVEL GUIDE",@"DOMINICIAN REPUBLIC TRAVEL GUIDE",@"MEXICO TRAVEL GUIDE",@"PUERTO RICO TRAVEL GUIDE", nil];
    
    appIcons= [NSArray arrayWithObjects:/*@"newWaze1.png",@"newGoogleMaps.png",*/@"newAppleMap1.png",@"CRI.png",@"PAN.png",@"DOM.png",@"MEX.png",@"PRI.png",nil];
    
    checkPath=[NSArray arrayWithObjects:/*@"waze://",@"comgooglemaps://",*/@"maps://",@"http:////www.lonelyplanet.com//costa-rica//things-to-do",@"http://www.lonelyplanet.com/panama/things-to-do",@"http://www.lonelyplanet.com/dominican-republic/things-to-do",@"http://www.lonelyplanet.com/mexico/things-to-do",@"http://www.lonelyplanet.com/puerto-rico/things-to-do", nil];
    
    downloadlinks=[NSArray arrayWithObjects:@"id323229106",@"id585027354",@"id592990211", nil];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FromHomePage"])
    {
        UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [leftButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(backBarButtonItem_clicked) forControlEvents:UIControlEventTouchUpInside];
        [leftButton setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
        [self.navigationItem setLeftBarButtonItem:leftBtn];
    }
}


-(void)backBarButtonItem_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [applist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [applist objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"OpenSans" size:14]];
    cell.imageView.frame = CGRectMake(0, 0, 50, 50);
    cell.imageView.image = [UIImage imageNamed:[appIcons objectAtIndex:indexPath.row]];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self installApp:indexPath.row];
}
- (void) installApp:(int)index
{
    i=index;
    
    NSLog(@"%@",applist[index]);
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:checkPath[index]]])
    {
        j=1;
        NSString *msg=[NSString stringWithFormat:NSLocalizedString(@"You will be redirected to Apple Store. Do you want to continue?",nil)];
        
        UIAlertView *installAppAlert = [[UIAlertView alloc]initWithTitle:
                                        NSLocalizedString(@"Alert",nil) message: msg delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"NO",nil) otherButtonTitles:NSLocalizedString(@"YES",nil), nil];
        [installAppAlert show];
    }
    else
    {
        j=2;
        NSString *msg=[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"You will be navigated to",nil),applist[index], NSLocalizedString(@". Are you sure, You want to navigate away from Siempre Wifi?",nil)];
        
        UIAlertView *openAppAlert = [[UIAlertView alloc]initWithTitle:
                                     NSLocalizedString(@"Alert",nil) message: msg delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"NO",nil) otherButtonTitles:NSLocalizedString(@"YES",nil), nil];
        
        [openAppAlert show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0)
    {
        
    }
    if(buttonIndex == 1) {
        
        if(j==1){
            [self redirectToAppStore:i];
        }
        if(j==2){
            [self openApp:i];
        }
    }
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.imageView.frame = CGRectMake(10, 10, 5, 5);
    
}

//this method redirect to applestore
-(void) redirectToAppStore:(int)index
{
    NSString *iTunesLink = iTunesLink = [NSString stringWithFormat:@"http://itunes.apple.com/us/app/""%@",downloadlinks[i]];
    NSLog(@"%@",iTunesLink);
    NSLog(@"URLWithString--->%@",downloadlinks[i]);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

//this method opens the app
-(void)openApp:(int)index
{
    NSLog(@"%@",checkPath[i]);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: checkPath[i]]];
    NSLog(@"URLWithString--->%@",checkPath[i]);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    // This will create a "invisible" footer
    return 0.02f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new];
}



@end
