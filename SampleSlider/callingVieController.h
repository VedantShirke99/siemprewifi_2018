//
//  callingVieController.h
//  SampleSlider
//
//  Created by Jayesh on 12/1/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwilioClient.h"
int timerCount;
@interface callingVieController : UIViewController<TCDeviceDelegate,TCConnectionDelegate>
{
    
    UITextField *calledNumber;
    NSTimer *timerNew;
}
@property (strong,nonatomic) TCConnection* _connection;
@property (retain, nonatomic) IBOutlet UILabel *calledPhoneNumber;
@property (retain,nonatomic) IBOutlet UITextField *calledNumber;
@property (strong, nonatomic)  UILabel *displayTimer;

- (IBAction)callEndBtn:(id)sender;
-(void)timer;
@property (strong, nonatomic) IBOutlet UIButton *endbtn;
@property (strong,nonatomic) IBOutlet UIImageView *dialerImage;
@property (strong, nonatomic) IBOutlet UIButton *dialerButton;
- (IBAction)dialerButtonCLicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *callingLabel;
-(void) dialerButtonClikedAtIndex:(UIButton *)button;
@end
