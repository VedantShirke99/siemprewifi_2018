//
//  PasswordRecoveryViewController.m
//  SampleSlider
//
//  Created by Jayesh on 24/11/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "PasswordRecoveryViewController.h"
#import "AFNetworking.h"
#import "HomeViewController.h"

@interface PasswordRecoveryViewController ()

@end

@implementation PasswordRecoveryViewController

@synthesize mySpinner,EmailIDTxt;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mySpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    mySpinner.center = CGPointMake(160, 300);
    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [leftButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backBarButtonItem_clicked) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
}

-(void)backBarButtonItem_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sendResetInstructionsButtonClicked:(id)sender
{
    
    [mySpinner startAnimating];
    mySpinner.hidesWhenStopped = YES;
    [self.view addSubview:mySpinner];
    
    
    
    
    if ([EmailIDTxt.text isEqualToString:@""]) {
        [mySpinner stopAnimating];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter the Email",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    }else{
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSString *serverAddress =[URL_LINk stringByAppendingString:[NSString stringWithFormat:@"recoverPassword?email_ID=%@",EmailIDTxt.text]];
        [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             self.posts = (NSDictionary *) responseObject;
             NSString *response = self.posts[@"mailSent"][@"success"];
             
             NSLog(@"%@",self.post);
             
             
             if ([response isEqualToString:@"true"])
             {
                 [mySpinner stopAnimating];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message Sent",nil) message:NSLocalizedString(@"Reset Instructions are sent to your email",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                 [alert show];
             }
             else
             {
                 [mySpinner stopAnimating];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Invalid Email ID",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                 [alert show];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [mySpinner stopAnimating];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Error while connecting to the server.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
             [alert show];
         }];
        
    }
    
}

- (IBAction)backgroundGesture:(id)sender {
    [self.view endEditing:YES];
}



- (IBAction)cancel:(id)sender{
    

    [self dismissViewControllerAnimated:true completion:nil];

    
    
}
@end
