//
//  HomeViewController.h
//  SampleSlider
//
//  Created by Jayesh on 21/01/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "ViewController.h"
#define URL_LINk @"https://www.siempre-wifi.com/"
@interface HomeViewController : ViewController <UIAlertViewDelegate>
- (IBAction)homeTextView:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *callButton;

@end
