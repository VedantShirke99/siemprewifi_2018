//
//  ChangePasswordAndLogOutViewController.h
//  SampleSlider
//
//  Created by Jayesh on 12/12/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface ChangePasswordAndLogOutViewController : UIViewController <MBProgressHUDDelegate>
- (IBAction)logoutButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *twiliNumber;
-(void)logout;
@end
