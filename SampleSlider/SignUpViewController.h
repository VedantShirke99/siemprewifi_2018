//
//  SignUpViewController.h
//  SiempreWifi
//
//  Created by Priya on 09/10/18.
//  Copyright © 2018 WhiteSnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *reEnterPasswordTextField;
@property (strong,nonatomic) NSDictionary *posts;
@end
