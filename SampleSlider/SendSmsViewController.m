//
//  SendSmsViewController.m
//  SampleSlider
//
//  Created by Jayesh on 25/11/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//
#import "Reachability.h"
#import "SendSmsViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "HomeViewController.h"
#import "Flurry.h"
#import "GAIDictionaryBuilder.h"
#import "IAPViewController.h"
#import "GAI.h"
#import "ChangePasswordAndLogOutViewController.h"
//LOCAL CONSTANTS
#define SMSALERTVIEW 1
#define CREDITALERTVIEW 2

@interface SendSmsViewController ()<MBProgressHUDDelegate>{
    
    NSString *userName;
    NSString *phoneNumberStr;
    NSString *countryCodeFlag;
    NSString *countryCode;
    MBProgressHUD *HUD;
    NSMutableArray *newCountryCodeStr;
    NSArray *countryList;
     NSString *countryCodeStr;
    NSUInteger countryCodeLength;
    MBProgressHUD *hud;
    BOOL countrySelected;
    UILabel *countrySelectedLabel;
    BOOL activeUser;
    NSString* phoneNumber;
    NSString * phNumber;
}

@end




@implementation SendSmsViewController
@synthesize sendSmsTextField,senderNumber,countryCodeData,countryCodeName,pickerViewContainer,textRemaining,textUsed,contacts,creditRemaingSpinner,creditRemainigImg,textUsedImg,textUsedSpinner,phoneNo,countrtTextField,phoneNoTextField,sendButton,contact;
@synthesize SelectedCountryLabel;
@synthesize showPickerButton;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [textUsedSpinner setFrame:CGRectMake(self.textUsedImg.frame.origin.x, self.textUsedImg.frame.origin.y, self.textUsedImg.frame.size.width, self.textUsedImg.frame.size.height)];
    countrySelected =NO;
    [phoneNoTextField.layer setCornerRadius:2];
    [countrtTextField.layer setCornerRadius:2];
    [sendSmsTextField.layer setCornerRadius:2];
    [sendButton.layer setCornerRadius:2];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    phoneNoTextField.leftView = paddingView;
    phoneNoTextField.leftViewMode = UITextFieldViewModeAlways;
    [phoneNo setTintColor:[UIColor blackColor]];
    [phoneNo becomeFirstResponder];
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    countrtTextField.leftView = paddingView1;
    countrtTextField.leftViewMode = UITextFieldViewModeAlways;
    

    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    sendSmsTextField.leftView = paddingView3;
    sendSmsTextField.leftViewMode = UITextFieldViewModeAlways;
    [sendSmsTextField setTintColor:[UIColor blackColor]];
    [sendSmsTextField becomeFirstResponder];
    
    
   
    
    newCountryCodeStr = [[NSMutableArray alloc]init];

    countryList  = [[NSArray alloc]initWithObjects:NSLocalizedString(@"Other (+)",nil),
                    NSLocalizedString(@"Panama (+507)",nil),
                    NSLocalizedString(@"Costa Rica (+506)",nil),
                    NSLocalizedString(@"United States (+1)",nil),
                    NSLocalizedString(@"United Kingdom (+44)",nil),
                    NSLocalizedString(@"Spain (+34)",nil),
                    NSLocalizedString(@"India (+91)",nil),
                    NSLocalizedString(@"Germany (+49)",nil),
                    NSLocalizedString(@"Mexico (+52)",nil),
                    NSLocalizedString(@"France (+33)",nil),
                    NSLocalizedString(@"Brazil (+55)",nil),
                    NSLocalizedString(@"Spain (+34)",nil),
                    NSLocalizedString(@"Italy (+3)",nil),
                    NSLocalizedString(@"Irel(+353)",nil),
                    NSLocalizedString(@"Portugal (+351)",nil),
                    NSLocalizedString(@"Canada (+1)",nil),
                    NSLocalizedString(@"D.R. (+1809)",nil),
                    NSLocalizedString(@"Chile (+56)",nil),
                    NSLocalizedString(@"Switzerland (+41)",nil),
                    NSLocalizedString(@"Austria (+43)",nil),
                    NSLocalizedString(@"Afghanistan (+93)",nil),
                    NSLocalizedString(@"Albania (+355)",nil),
                    NSLocalizedString(@"Algeria (+213)",nil),
                    NSLocalizedString(@"American Samoa (+1684)",nil),
                    NSLocalizedString(@"Andorra (+376)",nil),
                    NSLocalizedString(@"Angola (+244)",nil),
                    NSLocalizedString(@"Anguilla (+1264)",nil),
                    NSLocalizedString(@"Antarctica (+672)",nil),
                    NSLocalizedString(@"Antigua and Barbuda (+1268)",nil),
                    NSLocalizedString(@"Argentina (+54)",nil),
                    NSLocalizedString(@"Armenia (+374)",nil),
                    NSLocalizedString(@"Ar(+297)",nil),
                    NSLocalizedString(@"Australia (+61)",nil),
                    NSLocalizedString(@"Austria (+43)",nil),
                    NSLocalizedString(@"Azerbaijan (+994)",nil),
                    NSLocalizedString(@"Bahamas (+1242)",nil),
                    NSLocalizedString(@"Bahrain (+973)",nil),
                    NSLocalizedString(@"Banglad(+880)",nil),
                    NSLocalizedString(@"Barbados (+1246)",nil),
                    NSLocalizedString(@"Belarus (+375)",nil),
                    NSLocalizedString(@"Belgium (+32)",nil),
                    NSLocalizedString(@"Belize (+501)",nil),
                    NSLocalizedString(@"Benin (+229)",nil),
                    NSLocalizedString(@"Bermuda (+441)",nil),
                    NSLocalizedString(@"Bhutan (+975)",nil),
                    NSLocalizedString(@"Bolivia (+591)",nil),
                    NSLocalizedString(@"Bosnia and Herzegovina (+387)",nil),
                    NSLocalizedString(@"Botswana (+267)",nil),
                    NSLocalizedString(@"Brazil (+55)",nil),
                    NSLocalizedString(@"British VirIslands (+1284)",nil),
                    NSLocalizedString(@"Brunei (+673)",nil),
                    NSLocalizedString(@"Bulgaria (+359)",nil),
                    NSLocalizedString(@"Burkina Faso (+226)",nil),
                    NSLocalizedString(@"Burma  Myanmar (+95)",nil),
                    NSLocalizedString(@"Burundi (+257)",nil),
                    NSLocalizedString(@"Cambodia (+855)",nil),
                    NSLocalizedString(@"Cameroon (+237)",nil),
                    NSLocalizedString(@"Canada (+1)",nil),
                    NSLocalizedString(@"Cape Verde (+238)",nil),
                    NSLocalizedString(@"Cayman Islands (+1345)",nil),
                    NSLocalizedString(@"Central AfriRepublic (+236)",nil),
                    NSLocalizedString(@"Chad (+235)",nil),
                    NSLocalizedString(@"Chile (+56)",nil),
                    NSLocalizedString(@"China (+86)",nil),
                    NSLocalizedString(@"Christmas Island (+61)",nil),
                    NSLocalizedString(@"Cocos  Keeling Islands (+61)",nil),
                    NSLocalizedString(@"Colombia (+57)",nil),
                    NSLocalizedString(@"Comoros (+269)",nil),
                    NSLocalizedString(@"Cook Islands (+682)",nil),
                    NSLocalizedString(@"Costa Rica (+506)",nil),
                    NSLocalizedString(@"Croatia (+385)",nil),
                    NSLocalizedString(@"Cuba (+53)",nil),
                    NSLocalizedString(@"Cyprus (+357)",nil),
                    NSLocalizedString(@"Czech Republic (+420)",nil),
                    NSLocalizedString(@"Democratic Republic of the Congo (+243)",nil),
                    NSLocalizedString(@"Denmark (+45)",nil),
                    NSLocalizedString(@"Djibouti (+253)",nil),
                    NSLocalizedString(@"Dominica (+1767)",nil),
                    NSLocalizedString(@"Dominican Republic (+1809)",nil),
                    NSLocalizedString(@"Ecuador (+593)",nil),
                    NSLocalizedString(@"Egypt (+20)",nil),
                    NSLocalizedString(@"El Salvador (+503)",nil),
                    NSLocalizedString(@"EquatorGuinea (+240)",nil),
                    NSLocalizedString(@"Eritrea (+291)",nil),
                    NSLocalizedString(@"Estonia (+372)",nil),
                    NSLocalizedString(@"Ethiopia (+251)",nil),
                    NSLocalizedString(@"Falkland Islands (+500)",nil),
                    NSLocalizedString(@"FaIsla(+298)",nil),
                    NSLocalizedString(@"Fiji (+679)",nil),
                    NSLocalizedString(@"Finland (+358)",nil),
                    NSLocalizedString(@"France (+33)",nil),
                    NSLocalizedString(@"French Polynesia (+689)",nil),
                    NSLocalizedString(@"Gabon (+241)",nil),
                    NSLocalizedString(@"Gam(+220)",nil),
                    NSLocalizedString(@"Gaza Strip (+970)",nil),
                    NSLocalizedString(@"Georgia (+995)",nil),
                    NSLocalizedString(@"Germany (+49)",nil),
                    NSLocalizedString(@"Ghana (+233)",nil),
                    NSLocalizedString(@"Gibraltar (+350)",nil),
                    NSLocalizedString(@"Gre(+30)",nil),
                    NSLocalizedString(@"Greenland (+299)",nil),
                    NSLocalizedString(@"Grenada (+1473)",nil),
                    NSLocalizedString(@"Guam (+1671)",nil),
                    NSLocalizedString(@"Guatemala (+502)",nil),
                    NSLocalizedString(@"Guinea (+224)",nil),
                    NSLocalizedString(@"Guinea-Bissau (+245)",nil),
                    NSLocalizedString(@"Guyana (+592)",nil),
                    NSLocalizedString(@"Haiti (+509)",nil),
                    NSLocalizedString(@"Holy See  Vatican City (+39)",nil),
                    NSLocalizedString(@"Honduras (+504)",nil),
                    NSLocalizedString(@"HK(+852)",nil),
                    NSLocalizedString(@"Hungary (+36)",nil),
                    NSLocalizedString(@"Iceland (+354)",nil),
                    NSLocalizedString(@"Indonesia (+62)",nil),
                    NSLocalizedString(@"Iran (+98)",nil),
                    NSLocalizedString(@"Iraq (+964)",nil),
                    NSLocalizedString(@"Ireland (+353)",nil),
                    NSLocalizedString(@"Isle of Man (+44)",nil),
                    NSLocalizedString(@"Israel (+972)",nil),
                    NSLocalizedString(@"Italy (+39)",nil),
                    NSLocalizedString(@"Ivory Coast (+225)",nil),
                    NSLocalizedString(@"Jamaica (+1876)",nil),
                    NSLocalizedString(@"Japan (+81)",nil),
                    NSLocalizedString(@"Jor(+962)",nil),
                    NSLocalizedString(@"Kazakhstan (+7)",nil),
                    NSLocalizedString(@"Kenya (+254)",nil),
                    NSLocalizedString(@"Kiribati (+686)",nil),
                    NSLocalizedString(@"Kosovo (+381)",nil),
                    NSLocalizedString(@"Kuwait (+965)",nil),
                    NSLocalizedString(@"Kyrgyzs(+996)",nil),
                    NSLocalizedString(@"Laos (+856)",nil),
                    NSLocalizedString(@"Latvia (+371)",nil),
                    NSLocalizedString(@"Lebanon (+961)",nil),
                    NSLocalizedString(@"Lesotho (+266)",nil),
                    NSLocalizedString(@"Liberia (+231)",nil),
                    NSLocalizedString(@"Libya (+218)",nil),
                    NSLocalizedString(@"Liechtenstein (+423)",nil),
                    NSLocalizedString(@"Lithuania (+370)",nil),
                    NSLocalizedString(@"Luxembourg (+352)",nil),
                    NSLocalizedString(@"Macau (+853)",nil),
                    NSLocalizedString(@"Macedonia (+389)",nil),
                    NSLocalizedString(@"Madagas(+261)",nil),
                    NSLocalizedString(@"Malawi (+265)",nil),
                    NSLocalizedString(@"Malaysia (+60)",nil),
                    NSLocalizedString(@"Maldives (+960)",nil),
                    NSLocalizedString(@"Mali (+223)",nil),
                    NSLocalizedString(@"Malta (+356)",nil),
                    NSLocalizedString(@"MarshIsla(+692)",nil),
                    NSLocalizedString(@"Mauritania (+222)",nil),
                    NSLocalizedString(@"Mauritius (+230)",nil),
                    NSLocalizedString(@"Mayotte (+262)",nil),
                    NSLocalizedString(@"Mexico (+52)",nil),
                    NSLocalizedString(@"Micronesia (+691)",nil),
                    NSLocalizedString(@"Moldova (+373)",nil),
                    NSLocalizedString(@"Monaco (+377)",nil),
                    NSLocalizedString(@"Mongolia (+976)",nil),
                    NSLocalizedString(@"Montenegro (+382)",nil),
                    NSLocalizedString(@"Montserrat (+1 664)",nil),
                    NSLocalizedString(@"Morocco (+212)",nil),
                    NSLocalizedString(@"Mozambique (+258)",nil),
                    NSLocalizedString(@"Namibia (+264)",nil),
                    NSLocalizedString(@"Nauru (+674)",nil),
                    NSLocalizedString(@"Nepal (+977)",nil),
                    NSLocalizedString(@"Netherlands (+31)",nil),
                    NSLocalizedString(@"Netherlands Antil(+599)",nil),
                    NSLocalizedString(@"New Caledonia (+687)",nil),
                    NSLocalizedString(@"New Zealand (+64)",nil),
                    NSLocalizedString(@"Nicaragua (+505)",nil),
                    NSLocalizedString(@"Niger (+227)",nil),
                    NSLocalizedString(@"Nigeria (+234)",nil),
                    NSLocalizedString(@"Niue (+683)",nil),
                    NSLocalizedString(@"Norfolk Island (+672)",nil),
                    NSLocalizedString(@"North Korea (+850)",nil),
                    NSLocalizedString(@"Northern Mariana Islands (+1670)",nil),
                    NSLocalizedString(@"Norway (+47)",nil),
                    NSLocalizedString(@"Oman (+968)",nil),
                    NSLocalizedString(@"Pakistan (+92)",nil),
                    NSLocalizedString(@"Palau (+680)",nil),
                    NSLocalizedString(@"Panama (+507)",nil),
                    NSLocalizedString(@"Papua New Guinea (+675)",nil),
                    NSLocalizedString(@"Paraguay (+595)",nil),
                    NSLocalizedString(@"Peru (+51)",nil),
                    NSLocalizedString(@"Philippines (+63)",nil),
                    NSLocalizedString(@"Pitcairn Islands (+870)",nil),
                    NSLocalizedString(@"Poland (+48)",nil),
                    NSLocalizedString(@"Portugal (+351)",nil),
                    NSLocalizedString(@"Puerto Rico (+1)",nil),
                    NSLocalizedString(@"Qatar (+974)",nil),
                    NSLocalizedString(@"Republic of the Congo (+242)",nil),
                    NSLocalizedString(@"Romania (+40)",nil),
                    NSLocalizedString(@"Russia (+7)",nil),
                    NSLocalizedString(@"Rwanda (+250)",nil),
                    NSLocalizedString(@"Saint Barthelemy (+590)",nil),
                    NSLocalizedString(@"SaHel(+290)",nil),
                    NSLocalizedString(@"Saint Kitts and Nevis (+1869)",nil),
                    NSLocalizedString(@"Saint Lucia (+1 758)",nil),
                    NSLocalizedString(@"Saint Martin (+1 599)",nil),
                    NSLocalizedString(@"Saint Pierre Miquelon (+508)",nil),
                    NSLocalizedString(@"Saint Vincent and the Grenadines (+1784)",nil),
                    NSLocalizedString(@"Samoa (+685)",nil),
                    NSLocalizedString(@"San Marino (+378)",nil),
                    NSLocalizedString(@"Sao Tome and Princ(+239)",nil),
                    NSLocalizedString(@"Saudi Arabia (+966)",nil),
                    NSLocalizedString(@"Senegal (+221)",nil),
                    NSLocalizedString(@"Serbia (+381)",nil),
                    NSLocalizedString(@"Seychelles (+248)",nil),
                    NSLocalizedString(@"Sierra Leone (+232)",nil),
                    NSLocalizedString(@"Singap(+65)",nil),
                    NSLocalizedString(@"Slovakia (+421)",nil),
                    NSLocalizedString(@"Slovenia (+386)",nil),
                    NSLocalizedString(@"Solomon Islands (+677)",nil),
                    NSLocalizedString(@"Somalia (+252)",nil),
                    NSLocalizedString(@"South Africa (+27)",nil),
                    NSLocalizedString(@"South Korea (+82)",nil),
                    NSLocalizedString(@"Sri Lanka (+94)",nil),
                    NSLocalizedString(@"Sudan (+249)",nil),
                    NSLocalizedString(@"Suriname (+597)",nil),
                    NSLocalizedString(@"Swaziland (+268)",nil),
                    NSLocalizedString(@"Sweden (+46)",nil),
                    NSLocalizedString(@"Switzerland (+41)",nil),
                    NSLocalizedString(@"Syria (+963)",nil),
                    NSLocalizedString(@"Taiwan (+886)",nil),
                    NSLocalizedString(@"Tajikistan (+992)",nil),
                    NSLocalizedString(@"Tanzania (+255)",nil),
                    NSLocalizedString(@"Thailand (+66)",nil),
                    NSLocalizedString(@"Timor-Leste (+670)",nil),
                    NSLocalizedString(@"Togo (+228)",nil),
                    NSLocalizedString(@"Tokelau (+690)",nil),
                    NSLocalizedString(@"Tonga (+676)",nil),
                    NSLocalizedString(@"Trinidad and Tobago (+1 868)",nil),
                    NSLocalizedString(@"Tunisia (+216)",nil),
                    NSLocalizedString(@"Turkey (+90)",nil),
                    NSLocalizedString(@"Turkmenistan (+993)",nil),
                    NSLocalizedString(@"Turks and Caicos Islands (+1 649)",nil),
                    NSLocalizedString(@"Tuvalu (+688)",nil),
                    NSLocalizedString(@"Uganda (+256)",nil),
                    NSLocalizedString(@"Ukraine (+380)",nil),
                    NSLocalizedString(@"United Arab Emirates (+971)",nil),
                    NSLocalizedString(@"United Kingdom (+44)",nil),
                    NSLocalizedString(@"United States (+1)",nil),
                    NSLocalizedString(@"Uruguay (+598)",nil),
                    NSLocalizedString(@"Virgin Islands (+1 340)",nil),
                    NSLocalizedString(@"Uzbekistan (+998)",nil),
                    NSLocalizedString(@"Vanuatu (+678)",nil),
                    NSLocalizedString(@"Venezuela (+58)",nil),
                    NSLocalizedString(@"Vietnam (+84)",nil),
                    NSLocalizedString(@"Wallis Fut(+681)",nil),
                    NSLocalizedString(@"West Bank(+970)",nil),
                    NSLocalizedString(@"Yemen (+967)",nil),
                    NSLocalizedString(@"Zambia (+260)",nil),
                    NSLocalizedString(@"Zimbabwe (+263)",nil),nil];
    
    for (int i =0 ; i<251; i++) {
        
    
        NSString *countryName = countryList[i];
        
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
        NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
        countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
        NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
        NSString *countryStr = [splitStr objectAtIndex:0];
        [newCountryCodeStr addObject:countryStr];
        
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str1 = [defaults objectForKey:@"phNumber"];
    phNumber = [NSString stringWithFormat:@"%@",str1];
    int times = [[phNumber componentsSeparatedByString:@"+"] count]-1;
    
    if(([phNumber length] !=0)&& !([phNumber isEqualToString:@"(null)"])) {
        NSString *str = @"";
        if(!(times == 1)){
            str = @"+";
            phNumber =[str stringByAppendingString:phNumber];
            [defaults removeObjectForKey:@"phNumber"];
        }else{
            phNumber = @"";
        }
        NSLog(@"String Ph Number ---%@",[str stringByAppendingString:phNumber]);
        
       // phoneNoTextField.text = str1;
        
        
        NSString *oldTrimmedPhNumber = [phNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *phNumber = [oldTrimmedPhNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *trimPhNumber =  [[phNumber stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        senderNumber.text =[trimPhNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        
        
        if ([str1 characterAtIndex:0] == 43)
            phoneNoTextField.text = str1;
        else
            phoneNoTextField.text = [NSString stringWithFormat:@"+%@",str1];
        
        
        for (int i =0; i<251; i++)
        {
            
            NSString *countryName = countryList[i];
            
            
            NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
            NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
            countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
            NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
            NSString *countryStr = [splitStr objectAtIndex:1];
            countryCodeLength =[countryStr length];
            NSRange range = [trimPhNumber rangeOfString:countryStr];
            
            if(range.length != 0){
                NSLog(@"Rangee oF str %i",range.length);
                countryCodeName.text = [splitStr objectAtIndex:0];
            }
        
        }
    }
    
    [self LoadCredits];

 
    [self.showPickerButton.layer setCornerRadius:10];
    [countrtTextField setHidden:NO];
    [self.SelectedCountryLabel setHighlighted:YES];
    [self.SelectedCountryLabel setText:@"Select a country"];
    [self.SelectedCountryLabel setTextColor:[UIColor whiteColor]];
    [self.SelectedCountryLabel setTextAlignment:NSTextAlignmentRight];    

    
    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [leftButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backBarButtonItem_clicked) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
}


-(void) LoadCredits
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.title=@"";
    NSLog(@"Enter The Text Screen");
    pickerViewContainer.hidden = YES;
    [self.creditRemaingSpinner startAnimating];
    [self.textUsedSpinner startAnimating];
    
    userName = [defaults objectForKey:@"userName"];
    NSLog(@"username---->%@",userName);
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"countryCode" ofType:@"plist"];
    
    countryCodeData = [[NSArray alloc]initWithContentsOfFile:path];
    
    countryCodeData = [countryCodeData sortedArrayUsingSelector:@selector(compare:)];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    pickerViewContainer.frame = CGRectMake(0,800, 320, 261);
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        
        [self.creditRemaingSpinner stopAnimating];
        [self.textUsedSpinner stopAnimating];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please check your internet connection and try again later.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
        
    }else
    {
        creditRemainigImg.hidden =YES;
        textUsedImg.hidden = YES;
        
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString *url = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"getTextCredits?email_ID=%@",userName]];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Response----%@",responseObject);
         
         if ([[[responseObject objectForKey:@"creditsBalance"]valueForKey:@"active"] isEqualToString:@"True"])
         {
             activeUser = YES;
         }
         else
         {
             activeUser = NO;
         }
         textRemaining.text = [[responseObject objectForKey:@"creditsBalance"]valueForKey:@"credits_balance"];
         
         textUsed.text = [[responseObject objectForKey:@"creditsBalance"]
                          valueForKey:@"text_credits_used"];
         
         if ([textRemaining.text intValue] < 0) {
             textRemaining.text =@"0";
         }
         if ([textUsed.text intValue] < 0) {
             textUsed.text =@"0";
         }
         [self.creditRemaingSpinner stopAnimating];
         [self.textUsedSpinner stopAnimating];
         creditRemainigImg.hidden =NO;
         textUsedImg.hidden = NO;
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.creditRemaingSpinner stopAnimating];
         [self.textUsedSpinner stopAnimating];
         creditRemainigImg.hidden =NO;
         textUsedImg.hidden = NO;
    }];
}
}
-(void)backBarButtonItem_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    

    if (alertView.tag == SMSALERTVIEW)
    {
        switch (buttonIndex)
        {
            case 0: [self sendSMS];
                break;
            case 1: alertView.hidden = YES;
                break;
            default:
                break;
        }

    }
    else
    
        if (alertView.tag == CREDITALERTVIEW)
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    IAPViewController *VC = [[IAPViewController alloc]init];
                    [self.navigationController pushViewController:VC animated:YES];
                }
                    break;
                case 1: alertView.hidden = YES;
                    break;
                case 2: //Active User
                    break;
                default:
                    break;
            }
        }
    else
    if (alertView.tag ==3)
    {
        NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
        __block NSString *userNameFromNSUserDefaults = [defs objectForKey:@"userName"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"appLogout?email_ID=%@",[userNameFromNSUserDefaults stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        
        [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:nil forKey:@"userName"];
             
             UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"MainStoryBoard"];
             [self presentViewController:vc animated:YES completion:nil ];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             NSLog(@"LogOut Saved.....: %@", error);
         }];
    }
        
}

- (IBAction)sendSmsButtonClicked:(id)sender
{
    pickerViewContainer.hidden =YES;
    
    if([textRemaining.text isEqualToString:@"0"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"You have run out of credits. Please purchase more credits.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Buy",nil) otherButtonTitles:NSLocalizedString(@"Later",nil), nil];
        alert.tag = CREDITALERTVIEW;
        [alert show];
        return;
    }
    if([senderNumber.text isEqual:@""])
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter the phone number",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    }else if([countryCodeName.text isEqualToString:@""]){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please select the country",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    }else if(![self checkSubstring]){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter the correct country extension",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    }else if([senderNumber.text isEqualToString:countryCode]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter number after the country code",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    }else
        if ([sendSmsTextField.text  isEqual: @""])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message Alert",nil) message:NSLocalizedString(@"Please enter message body",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [alert show];
            alert.tag = SMSALERTVIEW;
            return;
        }
        else
            if (activeUser)
            {
                if((sendSmsTextField.text.length<=160) && ![textRemaining.text isEqualToString:@"0"])
                {
                    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
                    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
                    
                    if (networkStatus == NotReachable)
                    {
                        NSLog(@"There IS NO internet connection");
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please check your internet connection and try again later.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    else
                        [self sendSMS];
                }
            }
            else
                if (activeUser != YES && [textRemaining.text  isEqual:@""] && [textUsed.text  isEqual:@""])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Credits not Loaded. Please Wait.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                    [alert show];
                    [self LoadCredits];
                }
                else
                {
                    
                    UIAlertView *activeUserAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Account Expired",nil) message:NSLocalizedString(@"Your account has expired. Please contact administrator.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                    activeUserAlert.tag = 3;
                    [activeUserAlert show];
                }
    
}

-(BOOL)checkSubstring{
    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (!countryCode){
        [self hidePickerView:nil];
    }
    if (ver >= 8.0) {
        // Only executes on version 8 or above.
        if ([senderNumber.text containsString:countryCode]) {
            NSLog(@"string contains extension!");
            return YES;
        } else {
            NSLog(@"string does not contain extension");
            return NO;
        }
    } else {
        //Execute on version 7 and below
        if ([senderNumber.text rangeOfString:countryCode].location == NSNotFound) {
            NSLog(@"string does not contain extension");
            return NO;
        } else {
            NSLog(@"string contains country extension!");
            return YES;
        }
    }
    return YES;
}

-(void) sendSMS
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.delegate = self;
    hud.labelText = @"Sending.....";
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString* body = sendSmsTextField.text;
    NSString* sender_no = senderNumber.text;
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *startTime = [DateFormatter stringFromDate:[NSDate date]];
    NSLog(@"username---->%@",userName);
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"sendSms/?email_ID=%@&receiver=%@&body=%@&time=%@",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[sender_no stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"],[body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSLog(@"ServerText---->%@",serverAddress);
    
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         self.posts = (NSDictionary *) responseObject;
         self.post = self.posts[@"sendSms"][@"success"];
         NSString *value = self.posts[@"sendSms"][@"success"];
         NSLog(@"%@",value);
         if ([value isEqualToString:@"true"])
         {
             HUD.hidden = YES;
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message Alert",nil) message:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Message sent to",nil),senderNumber.text] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             
             NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                            [NSString stringWithFormat:@"%@",senderNumber.text],@"TO",
                                            nil];
             [Flurry logEvent:@"Message Sent" withParameters:flurryParams];
             
             id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
             [tracker send:[[GAIDictionaryBuilder createAppView] build]];
             [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithFormat:@"Message Sent to%@",senderNumber.text]     // Event category (required)
                                                                   action:@"None"  // Event action (required)
                                                                    label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                                                                    value:nil] build]];
             
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [alert show];
             senderNumber.text =@"";
             sendSmsTextField.text=@"";
             [self LoadCredits];
         }
         else
         {
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Message could not be delivered",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [alert show];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSLog(@"ERROR:%@",[error localizedDescription]);
         NSLog(@"Error: %@", error);
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Error Connecting to Server",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
         [alert show];
     }];
}
#pragma mark _UIPickerView Datasource & Delegate Methods
#pragma marks -UIPickerView & Delegate Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
    
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [newCountryCodeStr count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return newCountryCodeStr[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

    NSString *selectCountry = [newCountryCodeStr objectAtIndex:row];
    countryCodeName.text = selectCountry;
    [self.SelectedCountryLabel setText:selectCountry];
    countryCodeFlag = @"true";
    
}

- (IBAction)tapGestureKeyboard:(id)sender
{
    pickerViewContainer.hidden = NO;
    [self.view endEditing:NO];
     countrySelected = NO;
}
- (IBAction)tapGestureSendSms:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)InAppClicked:(id)sender
{
    IAPViewController *VC = [[IAPViewController alloc]init];
    [self.navigationController pushViewController:VC animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
      countrySelected = NO;
}
- (IBAction)showPickerBtn:(id)sender
{
    [phoneNo endEditing:YES];
    [sendSmsTextField endEditing:YES];
    
    if (senderNumber.isEditing)
    {
        NSLog(@"Editing On");
    }
    else
    {
        NSString *path = [[NSBundle mainBundle]pathForResource:@"countryCode" ofType:@"plist"];
        countryCodeData = [[NSArray alloc]initWithContentsOfFile:path];
        countryCodeData = [countryCodeData sortedArrayUsingSelector:@selector(compare:)];
        [UIView beginAnimations:nil context:NULL];
        pickerViewContainer.hidden = NO;
        [UIView commitAnimations];
    }
}

-(void)setCountryCode{
    NSArray *splitStr = [[NSArray alloc]init];
    for (int i =0 ; i<251; i++)
    {
        NSString *countryName = countryList[i];
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
        NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
        countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
        splitStr = [countryCodeStr componentsSeparatedByString:@"("];
        NSString *countryStr = [[splitStr objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *temp = [splitStr objectAtIndex:1];
        NSString *str = countrtTextField.text;
        if([str isEqualToString:countryStr])
        {
            countryCode = temp;
        }
    }
}

- (IBAction)hidePickerView:(id)sender
{
    NSArray *splitStr = [[NSArray alloc]init];
    
    for (int i =0 ; i<251; i++)
    {
        NSString *countryName = countryList[i];
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
        NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
        countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
        splitStr = [countryCodeStr componentsSeparatedByString:@"("];
        NSString *countryStr = [splitStr objectAtIndex:0];
        NSString *temp = [splitStr objectAtIndex:1];
        NSString *str = countryCodeName.text;
        if([[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:[countryStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]) //([str isEqualToString:countryStr])
        {
            countryCode = temp;
        }
    }
    if([senderNumber.text length] > 0){
        if(![[senderNumber.text substringToIndex:1] isEqualToString:@"+"]){
            senderNumber.text = [countryCode stringByAppendingString:senderNumber.text];
        } else {
            senderNumber.text = countryCode;
        }
    } else {
        senderNumber.text = countryCode;
    }
    countryCodeLength =[countryCode length];
    [UIView beginAnimations:nil context:NULL];
    [UIView commitAnimations];
    pickerViewContainer.hidden = YES;

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
     countrySelected = NO;
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField ==senderNumber)
    {
        if (!pickerViewContainer.hidden)
        {
            return NO;
        }
        else
        {
            textField.keyboardType = UIKeyboardTypeNumberPad;
            pickerViewContainer.hidden =YES;
            countrySelected = YES;
            return YES;
        }

    }
    else
        if(textField == senderNumber || textField == sendSmsTextField)
        {
        pickerViewContainer.hidden =YES;
        return YES;
        }
        else
            return NO;
}

- (IBAction)contactBtn:(id)sender
{

    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(ver>= 9.0){
        contact = [[CNContactPickerViewController alloc]init];
        [contact setDelegate:self];
        //        [contact setDisplayedPropertyKeys:[NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonPhoneProperty], nil]];
        //        [contact setDisplayedPropertyKeys:[NSArray arrayWithObjects:[NSNumber numberWithInt:KCN]]]
        [self presentViewController:contact animated:YES completion:nil];
    } else {
        contacts = [[ABPeoplePickerNavigationController alloc] init];
        [contacts setPeoplePickerDelegate:self];
        [contacts setDisplayedProperties:[NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]]];
        NSLog(@"Phone Number---->%d",kABPersonPhoneProperty);
        [self presentViewController:contacts animated:YES completion:nil ];
    }}

-(void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty{
    NSLog(@"Selected");
    //    ABMutableMultiValueRef multiEmail = ABRecordCopyValue(person);
    //    NSString *phoneNumber = (__bridge NSString *) ABMultiValueCopyValueAtIndex(multiEmail, identifier);
    phoneNumber = [contactProperty.contact.phoneNumbers[0].value valueForKey:@"digits"];
    NSMutableCharacterSet *characterSet =
    [NSMutableCharacterSet characterSetWithCharactersInString:@"()-"];
    NSArray *arrayOfComponents = [phoneNumber componentsSeparatedByCharactersInSet:characterSet];
    phoneNumberStr = [arrayOfComponents componentsJoinedByString:@""];
    NSString *firstLetter = [phoneNumberStr substringToIndex:1];
    
    
    if([firstLetter isEqualToString:@"+"]){
        
        NSString *phNumber = [[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        senderNumber.text = phNumber;
        NSLog(@"Phone Number----->%@",phoneNumberStr);
        
        for (int i= 0 ; i<251; i++) {
            
            
            NSString *countryName = countryList[i];
            
            
            NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
            NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
            countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
            NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
            NSString *countryStr = [splitStr objectAtIndex:1];
            
            NSArray *temp = [phoneNumberStr componentsSeparatedByString:@" "];
            NSString *countryCodeStr = [temp objectAtIndex:0];
            NSRange range = [countryCodeStr rangeOfString:countryStr];
            
            if(range.length != 0)
            {
                countryCodeName.text = [splitStr objectAtIndex:0];
            }
            
        }
        
        
    }else if([countryCodeFlag isEqualToString:@"true"])
    {
        senderNumber.text = [[[countryCode stringByAppendingString:phoneNumberStr]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"numberField.text----%@",senderNumber.text);
        senderNumber.text = [senderNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    }else{
        senderNumber.text=[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (phNumber)
    {
        if(![([phoneNoTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]])  isEqual: @""])
        {
            [self selectCountryFromNumber:phoneNoTextField.text];
            if (![self.countrtTextField.text isEqualToString:@""])
            {
                [self setCountryCode];
            }
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    ABMutableMultiValueRef multiEmail = ABRecordCopyValue(person, property);
    NSString *phoneNumber = (__bridge NSString *) ABMultiValueCopyValueAtIndex(multiEmail, identifier);
    
    NSMutableCharacterSet *characterSet =
    [NSMutableCharacterSet characterSetWithCharactersInString:@"()-"];
    NSArray *arrayOfComponents = [phoneNumber componentsSeparatedByCharactersInSet:characterSet];
    phoneNumberStr = [arrayOfComponents componentsJoinedByString:@""];
    NSString *firstLetter = [phoneNumberStr substringToIndex:1];
    
    
    if([firstLetter isEqualToString:@"+"])
    {
        
        senderNumber.text = [[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        NSLog(@"Phone Number----->%@",phoneNumberStr);
        
        for (int i= 0 ; i<251; i++) {
            
            NSString *countryName = countryList[i];
            
            
            NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
            NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
            countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
            NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
            NSString *countryStr = [splitStr objectAtIndex:1];
            
            NSArray *temp = [phoneNumberStr componentsSeparatedByString:@" "];
            NSString *countryCodeStr = [temp objectAtIndex:0];
            NSRange range = [countryCodeStr rangeOfString:countryStr];
            
            if(range.length != 0){
                NSLog(@"Rangee oF str %i",range.length);
                countryCodeName.text = [splitStr objectAtIndex:0];
            }

        }

    }else if([countryCodeFlag isEqualToString:@"true"]){
        
        senderNumber.text = [[[countryCode  stringByAppendingString:phoneNumberStr]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                             
    }else{
        senderNumber.text=[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    
    
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
  
    [contacts dismissViewControllerAnimated:true completion:nil];
    
}



- (IBAction)textChanged:(id)sender
{
    
        NSString *newString = [[[[senderNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""]stringByReplacingOccurrencesOfString:@"(" withString:@""]stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"MODIFIED STRING : %@",newString);
        [senderNumber setText:[NSString stringWithFormat:@"%@",newString]];
}

-(void) selectCountryFromNumber:(NSString *) Number
{
    if ((Number.length > 1) && Number.length == 1 && [[Number substringToIndex:1] isEqualToString:@"+"])
        countrtTextField.text = NSLocalizedString(@"Other",nil);
    else
        if ((Number.length > 3) && (Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+91"])
            countrtTextField.text = NSLocalizedString(@"India",nil);
        else
            if ((Number.length > 4) && [[Number substringToIndex:4] isEqualToString:@"+507"])
                countrtTextField.text = NSLocalizedString(@"Panama",nil);
            else
                if ((Number.length > 4) && [[Number substringToIndex:4] isEqualToString:@"+506"]) {
                    countrtTextField.text =  NSLocalizedString(@"Costa Rica",nil);
                }
                else
                    if ((Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+34"]) {
                        countrtTextField.text =  NSLocalizedString(@"Spain",nil);
                    }
                    else
                        if ((Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+49"]) {
                            countrtTextField.text = NSLocalizedString(@"Germany",nil);
                        }
                        else
                            if ((Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+52"]) {
                                countrtTextField.text = NSLocalizedString(@"Mexico",nil);
                            }
                            else
                                if ((Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+33"]) {
                                    countrtTextField.text = NSLocalizedString(@"France (+33)",nil);
                                }
                                else
                                    if ((Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+55"]) {
                                        countrtTextField.text =NSLocalizedString(@"Brazil",nil);
                                    } else
                                        if ((Number.length > 2) && [[Number substringToIndex:2] isEqualToString:@"+3"]) {
                                            countrtTextField.text =NSLocalizedString(@"Italy ",nil);
                                        }
                                        else
                                            if ((Number.length > 4) && [[Number substringToIndex:4] isEqualToString:@"+353"]) {
                                                countrtTextField.text =  NSLocalizedString(@"Irel",nil);
                                            }
                                            else
                                                if ((Number.length > 4) && [[Number substringToIndex:4] isEqualToString:@"+351"]) {
                                                    countrtTextField.text =  NSLocalizedString(@"Portugal",nil);
                                                }
                                                else
                                                    if ((Number.length > 2) && [[Number substringToIndex:2] isEqualToString:@"+1"]) {
                                                        countrtTextField.text =  NSLocalizedString(@"Canada",nil);
                                                    }
                                                    else
                                                        if ((Number.length > 5) && [[Number substringToIndex:5] isEqualToString:@"+1809"]) {
                                                            countrtTextField.text = NSLocalizedString(@"D.R.",nil);
                                                        }
                                                        else
                                                            if ((Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+56"]) {
                                                                countrtTextField.text =  NSLocalizedString(@"Chile",nil);
                                                            }
                                                            else
                                                                if ((Number.length > 3) && [[Number substringToIndex:3] isEqualToString:@"+41"]) {
                                                                    countrtTextField.text =  NSLocalizedString(@"Switzerland",nil);
                                                                }
                                                                else
                                                                    countrtTextField.text = NSLocalizedString(@"Other",nil);
    
    
    
}

@end
