//
//  SWActiveUser.m
//  SiempreWifi
//
//  Created by Jayesh on 26/03/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "SWActiveUser.h"
#import "HomeViewController.h"
#import "AFNetworking.h"
@implementation SWActiveUser
-(BOOL) checkActive
{
    __block BOOL activeStatus;
       
    
    NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"checkActive?email_ID=%@&",[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"RESPONSE :  %@",responseObject);
        NSString *status = [[responseObject objectForKey:@"activeStatus"] objectForKey:@"active"];

        if ([status isEqualToString:@"1"])
        {
            activeStatus = YES;
        }
        else
            activeStatus = NO;
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSLog(@"There is no internet connection");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please check your internet connection and try again later",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
    activeStatus = YES;
    return activeStatus;
}
@end
