//
//  SWCircleLineButton.m
//  SiempreWifi
//
//  Created by Jayesh on 28/03/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import "SWCircleLineButton.h"


@implementation SWCircleLineButton
@synthesize circleLayer;
@synthesize colour;

- (void)drawCircleButton:(UIColor *)color
{
    self.colour = color;
    self.titleLabel.textColor = [UIColor whiteColor];
    
    [self setTitleColor:color forState:UIControlStateNormal];
    
    self.circleLayer = [CAShapeLayer layer];
    
    [self.circleLayer setBounds:CGRectMake(0.0f, 0.0f, [self bounds].size.width,
                                           [self bounds].size.height)];
    
    [self.circleLayer setPosition:CGPointMake(CGRectGetMidX([self bounds]),CGRectGetMidY([self bounds]))];
    
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    
    [self.circleLayer setPath:[path CGPath]];
    
    [self.circleLayer setStrokeColor:[[UIColor lightGrayColor] CGColor]];
    
    [self.circleLayer setLineWidth:1.0f];
    [self.circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    
    [[self layer] addSublayer:self.circleLayer];
    self.titleLabel.textColor = [UIColor blueColor];
}

- (void)setHighlighted:(BOOL)highlighted
{
    if (highlighted)
    {
        self.titleLabel.textColor = [UIColor whiteColor];
        [self.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:18]];
        [self.circleLayer setFillColor:self.colour.CGColor];
    }
    else
    {
        [self.circleLayer setFillColor:[UIColor clearColor].CGColor];
        [self.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:20]];
        self.titleLabel.textColor = [UIColor blackColor];
    }
}


@end
