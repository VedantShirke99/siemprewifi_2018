 //
//  CallViewController.m
//  SampleSlider
//
//  Created by Jayesh on 26/11/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "SWCallingViewController.h"
#import "UIDevice-Hardware.h"
#import "CallViewController.h"
#import "AppDelegate.h"
#import "sys/utsname.h"
#import "MainVCViewController.h"
#import "AFNetworking.h"
#import "TwilioClient.h"
#import "TCConnectionDelegate.h"
#import "Reachability.h"
#import "customViewCell.h"
#import "HomeViewController.h"
#import "Flurry.h"
#import "SWActiveUser.h"
#import "ChangePasswordAndLogOutViewController.h"
#import "IAPViewController.h"
#import "SWHelper.h"
@interface CallViewController ()<TCDeviceDelegate,TCConnectionDelegate>
{
   //TCDevice* _phone;
    TCConnection* _connection;
    BOOL creditsLoaded;
    UILabel *countrySelected;
    BOOL activeUserStatus;
}
@end
//countryCodeData
@implementation CallViewController{
    
    NSString *username;
    NSString *credits;
    NSString *countryCode;
    NSString *phoneNumberStr;
    NSString *countryCodeFlag;
    NSString *newPhoneNumber;
    NSArray *countryList;
    NSString *contryStr;
    NSString *countryCodeStr;
    NSMutableArray *newCountryCodeStr;
    NSUInteger countryCodeLength;
    NSArray *dialBtn;
    NSArray *handler;
    UILabel *minUsed;
    UILabel *creditsBalanceText;
    UIButton *pasteButton;
    CGFloat TappedX;
    SWCallingViewController *callingViewController;
}
@synthesize numberField,pickerViewContainer,countryCodeData,countryCodeTxtField,callingView,callCreditsUsed,creditsBalance,phConnection,phone,contacts,delegate,creditSpinner,minuteUsedSpinner,creditImg,creditUsedImg,detailsOfCallLogs,collectionView,contact;
@synthesize countrySelected;
@synthesize creditsRemainingImageView,creditsUsedImageView,countryCodeTextField,callButton;
@synthesize selectedNumber;
-(void)device:(TCDevice*)device didStopListeningForIncomingConnections:(NSError*)error
{
    NSLog(@"Stopped Listening");
    NSLog(@"Twilio Service Error : Twillio Server Disconnected");
}
-(void)connection:(TCConnection*)connection didFailWithError:(NSError*)error
{

    NSLog(@"Failed Due to Error : %@",[error localizedDescription]);
    
}
//if (self.selectedNumber) numberField.text = self.selectedNumber; else numberField.text = self.selectedNumber;

-(void) dialClear:(id)sender
{
    NSLog(@"Cleared");
}

-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) {
            corner = corner | UIRectCornerTopLeft;
        }
        if (tr) {
            corner = corner | UIRectCornerTopRight;
        }
        if (bl) {
            corner = corner | UIRectCornerBottomLeft;
        }
        if (br) {
            corner = corner | UIRectCornerBottomRight;
        }
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    } else {
        return view;
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([numberField.text length]<=4)
        return YES;
    else
        return NO;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    // Get the specific point that was touched
    CGPoint point = [touch locationInView:self.view];
    NSLog(@"X location: %f", point.x);
    NSLog(@"Y Location: %f",point.y);
    TappedX = point.x;
}
-(void)pasteFromPasteBoard
{
    NSLog(@"Paste Button CLicked");
    pasteButton.hidden = YES;
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    NSLog(@"STRING FROM CLIPBOARD: %@",pboard.string);
    if (pboard.string)
    {
        NSString *newString = [[[[pboard.string stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""]stringByReplacingOccurrencesOfString:@"(" withString:@""]stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"MODIFIED STRING : %@",newString);
        [numberField setText:[NSString stringWithFormat:@"%@%@",numberField.text,newString]];
    }
    
}
//-(void)TapMethod:(UILongPressGestureRecognizer*) tapgesture
//{
//    CGPoint tappedPoint = [tapgesture locationInView:self.view];
//    CGFloat xCoordinate = tappedPoint.x;
//    [pasteButton  setFrame:CGRectMake(xCoordinate, self.numberField.frame.origin.y-5 -30, 50, 30)];
//    NSLog(@"Double Clicked");
//    pasteButton.hidden = NO;
//}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)viewDidLoad
{
    creditsLoaded = NO;
    [super viewDidLoad];
    
    
    [self.collectionView setHidden:YES];
  
    [countrySelected setBackgroundColor:[UIColor clearColor]];
    [countrySelected setTextColor:[UIColor whiteColor]];
    [countrySelected setTextAlignment:NSTextAlignmentRight];
    [countrySelected setText:@"Select a country"];
    
    [self.view addSubview:countrySelected];
    [countrySelected setHighlighted:YES];
    [countryCodeTextField setHidden:NO];
    
    [countrySelected.layer setCornerRadius:10];
    
    [self.downKeyButton.layer setCornerRadius:10];
    [self.numberField.layer setCornerRadius:2];
   
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    numberField.leftView = paddingView;
    numberField.leftViewMode = UITextFieldViewModeAlways;
    [numberField setTintColor:[UIColor blackColor]];
    numberField.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)]; // Hide keyboard, but show blinking cursor
    [numberField becomeFirstResponder];
    
    [self.countryCodeTextField.layer setCornerRadius:2];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    countryCodeTextField.leftView = paddingView2;
    countryCodeTextField.leftViewMode = UITextFieldViewModeAlways;
    
    pasteButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4, self.numberField.frame.origin.y-5 -30, 50, 30)];
    [self.view addSubview:pasteButton];
    [pasteButton.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:15]];
    [pasteButton.layer setCornerRadius:5];
    [pasteButton addTarget:self action:@selector(pasteFromPasteBoard) forControlEvents:UIControlEventTouchUpInside];
    [pasteButton setBackgroundColor:[UIColor darkGrayColor]];
    [pasteButton setTitle:@"paste" forState:UIControlStateNormal];
    pasteButton.hidden =YES;

    minUsed = [[UILabel alloc]initWithFrame:CGRectMake(self.callCreditsUsed.frame.origin.x, self.callCreditsUsed.frame.origin.y, self.callCreditsUsed.frame.size.width, self.callCreditsUsed.frame.size.height)];

    [self.view addSubview:minUsed];
    [minUsed setFont:[UIFont fontWithName:@"OpenSans" size:15]];
    
    creditsBalanceText = [[UILabel alloc]initWithFrame:CGRectMake(self.creditSpinner.frame.origin.x + self.creditSpinner.frame.size.width, self.callCreditsUsed.frame.origin.y, 25, 25)];
    [creditsBalanceText setFont:[UIFont fontWithName:@"OpenSans" size:15]];
    [creditsBalanceText setTextColor:[UIColor redColor]];
    [self.view addSubview:creditsBalanceText];
    
    [[UINavigationBar appearance]setBackgroundColor:[UIColor colorWithRed:108 green:125 blue:137 alpha:1.0]];

    
    
    newCountryCodeStr = [[NSMutableArray alloc]init];


    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.scrollEnabled = NO;
    
    

    dialBtn = [[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"+",@"0",@"#", nil];
    handler = [[NSArray alloc]initWithObjects:@"dialOne",@"dialTwo", nil];
    
    countryList  = [[NSArray alloc]initWithObjects:
                    NSLocalizedString(@"Other (+)",nil),
                    NSLocalizedString(@"Panama (+507)",nil),
                    NSLocalizedString(@"Costa Rica (+506)",nil),
                    NSLocalizedString(@"United States (+1)",nil),
                    NSLocalizedString(@"United Kingdom (+44)",nil),
                    NSLocalizedString(@"Spain (+34)",nil),
                    NSLocalizedString(@"India (+91)",nil),
                    NSLocalizedString(@"Germany (+49)",nil),
                    NSLocalizedString(@"Mexico (+52)",nil),
                    NSLocalizedString(@"France (+33)",nil),
                    NSLocalizedString(@"Brazil (+55)",nil),
                    NSLocalizedString(@"Spain (+34)",nil),
                    NSLocalizedString(@"Italy (+3)",nil),
                    NSLocalizedString(@"Irel(+353)",nil),
                    NSLocalizedString(@"Portugal (+351)",nil),
                    NSLocalizedString(@"Canada (+1)",nil),
                    NSLocalizedString(@"D.R. (+1809)",nil),
                    NSLocalizedString(@"Chile (+56)",nil),
                    NSLocalizedString(@"Switzerland (+41)",nil),
                    NSLocalizedString(@"Austria (+43)",nil),
                    NSLocalizedString(@"Afghanistan (+93)",nil),
                    NSLocalizedString(@"Albania (+355)",nil),
                    NSLocalizedString(@"Algeria (+213)",nil),
                    NSLocalizedString(@"American Samoa (+1684)",nil),
                    NSLocalizedString(@"Andorra (+376)",nil),
                    NSLocalizedString(@"Angola (+244)",nil),
                    NSLocalizedString(@"Anguilla (+1264)",nil),
                    NSLocalizedString(@"Antarctica (+672)",nil),
                    NSLocalizedString(@"Antigua and Barbuda (+1268)",nil),
                    NSLocalizedString(@"Argentina (+54)",nil),
                    NSLocalizedString(@"Armenia (+374)",nil),
                    NSLocalizedString(@"Ar(+297)",nil),
                    NSLocalizedString(@"Australia (+61)",nil),
                    NSLocalizedString(@"Austria (+43)",nil),
                    NSLocalizedString(@"Azerbaijan (+994)",nil),
                    NSLocalizedString(@"Bahamas (+1242)",nil),
                    NSLocalizedString(@"Bahrain (+973)",nil),
                    NSLocalizedString(@"Banglad(+880)",nil),
                    NSLocalizedString(@"Barbados (+1246)",nil),
                    NSLocalizedString(@"Belarus (+375)",nil),
                    NSLocalizedString(@"Belgium (+32)",nil),
                    NSLocalizedString(@"Belize (+501)",nil),
                    NSLocalizedString(@"Benin (+229)",nil),
                    NSLocalizedString(@"Bermuda (+441)",nil),
                    NSLocalizedString(@"Bhutan (+975)",nil),
                    NSLocalizedString(@"Bolivia (+591)",nil),
                    NSLocalizedString(@"Bosnia and Herzegovina (+387)",nil),
                    NSLocalizedString(@"Botswana (+267)",nil),
                    NSLocalizedString(@"Brazil (+55)",nil),
                    NSLocalizedString(@"British VirIslands (+1284)",nil),
                    NSLocalizedString(@"Brunei (+673)",nil),
                    NSLocalizedString(@"Bulgaria (+359)",nil),
                    NSLocalizedString(@"Burkina Faso (+226)",nil),
                    NSLocalizedString(@"Burma  Myanmar (+95)",nil),
                    NSLocalizedString(@"Burundi (+257)",nil),
                    NSLocalizedString(@"Cambodia (+855)",nil),
                    NSLocalizedString(@"Cameroon (+237)",nil),
                    NSLocalizedString(@"Canada (+1)",nil),
                    NSLocalizedString(@"Cape Verde (+238)",nil),
                    NSLocalizedString(@"Cayman Islands (+1345)",nil),
                    NSLocalizedString(@"Central AfriRepublic (+236)",nil),
                    NSLocalizedString(@"Chad (+235)",nil),
                    NSLocalizedString(@"Chile (+56)",nil),
                    NSLocalizedString(@"China (+86)",nil),
                    NSLocalizedString(@"Christmas Island (+61)",nil),
                    NSLocalizedString(@"Cocos  Keeling Islands (+61)",nil),
                    NSLocalizedString(@"Colombia (+57)",nil),
                    NSLocalizedString(@"Comoros (+269)",nil),
                    NSLocalizedString(@"Cook Islands (+682)",nil),
                    NSLocalizedString(@"Costa Rica (+506)",nil),
                    NSLocalizedString(@"Croatia (+385)",nil),
                    NSLocalizedString(@"Cuba (+53)",nil),
                    NSLocalizedString(@"Cyprus (+357)",nil),
                    NSLocalizedString(@"Czech Republic (+420)",nil),
                    NSLocalizedString(@"Democratic Republic of the Congo (+243)",nil),
                    NSLocalizedString(@"Denmark (+45)",nil),
                    NSLocalizedString(@"Djibouti (+253)",nil),
                    NSLocalizedString(@"Dominica (+1767)",nil),
                    NSLocalizedString(@"Dominican Republic (+1809)",nil),
                    NSLocalizedString(@"Ecuador (+593)",nil),
                    NSLocalizedString(@"Egypt (+20)",nil),
                    NSLocalizedString(@"El Salvador (+503)",nil),
                    NSLocalizedString(@"EquatorGuinea (+240)",nil),
                    NSLocalizedString(@"Eritrea (+291)",nil),
                    NSLocalizedString(@"Estonia (+372)",nil),
                    NSLocalizedString(@"Ethiopia (+251)",nil),
                    NSLocalizedString(@"Falkland Islands (+500)",nil),
                    NSLocalizedString(@"FaIsla(+298)",nil),
                    NSLocalizedString(@"Fiji (+679)",nil),
                    NSLocalizedString(@"Finland (+358)",nil),
                    NSLocalizedString(@"France (+33)",nil),
                    NSLocalizedString(@"French Polynesia (+689)",nil),
                    NSLocalizedString(@"Gabon (+241)",nil),
                    NSLocalizedString(@"Gam(+220)",nil),
                    NSLocalizedString(@"Gaza Strip (+970)",nil),
                    NSLocalizedString(@"Georgia (+995)",nil),
                    NSLocalizedString(@"Germany (+49)",nil),
                    NSLocalizedString(@"Ghana (+233)",nil),
                    NSLocalizedString(@"Gibraltar (+350)",nil),
                    NSLocalizedString(@"Gre(+30)",nil),
                    NSLocalizedString(@"Greenland (+299)",nil),
                    NSLocalizedString(@"Grenada (+1473)",nil),
                    NSLocalizedString(@"Guam (+1671)",nil),
                    NSLocalizedString(@"Guatemala (+502)",nil),
                    NSLocalizedString(@"Guinea (+224)",nil),
                    NSLocalizedString(@"Guinea-Bissau (+245)",nil),
                    NSLocalizedString(@"Guyana (+592)",nil),
                    NSLocalizedString(@"Haiti (+509)",nil),
                    NSLocalizedString(@"Holy See  Vatican City (+39)",nil),
                    NSLocalizedString(@"Honduras (+504)",nil),
                    NSLocalizedString(@"HK(+852)",nil),
                    NSLocalizedString(@"Hungary (+36)",nil),
                    NSLocalizedString(@"Iceland (+354)",nil),
                    NSLocalizedString(@"Indonesia (+62)",nil),
                    NSLocalizedString(@"Iran (+98)",nil),
                    NSLocalizedString(@"Iraq (+964)",nil),
                    NSLocalizedString(@"Ireland (+353)",nil),
                    NSLocalizedString(@"Isle of Man (+44)",nil),
                    NSLocalizedString(@"Israel (+972)",nil),
                    NSLocalizedString(@"Italy (+39)",nil),
                    NSLocalizedString(@"Ivory Coast (+225)",nil),
                    NSLocalizedString(@"Jamaica (+1876)",nil),
                    NSLocalizedString(@"Japan (+81)",nil),
                    NSLocalizedString(@"Jor(+962)",nil),
                    NSLocalizedString(@"Kazakhstan (+7)",nil),
                    NSLocalizedString(@"Kenya (+254)",nil),
                    NSLocalizedString(@"Kiribati (+686)",nil),
                    NSLocalizedString(@"Kosovo (+381)",nil),
                    NSLocalizedString(@"Kuwait (+965)",nil),
                    NSLocalizedString(@"Kyrgyzs(+996)",nil),
                    NSLocalizedString(@"Laos (+856)",nil),
                    NSLocalizedString(@"Latvia (+371)",nil),
                    NSLocalizedString(@"Lebanon (+961)",nil),
                    NSLocalizedString(@"Lesotho (+266)",nil),
                    NSLocalizedString(@"Liberia (+231)",nil),
                    NSLocalizedString(@"Libya (+218)",nil),
                    NSLocalizedString(@"Liechtenstein (+423)",nil),
                    NSLocalizedString(@"Lithuania (+370)",nil),
                    NSLocalizedString(@"Luxembourg (+352)",nil),
                    NSLocalizedString(@"Macau (+853)",nil),
                    NSLocalizedString(@"Macedonia (+389)",nil),
                    NSLocalizedString(@"Madagas(+261)",nil),
                    NSLocalizedString(@"Malawi (+265)",nil),
                    NSLocalizedString(@"Malaysia (+60)",nil),
                    NSLocalizedString(@"Maldives (+960)",nil),
                    NSLocalizedString(@"Mali (+223)",nil),
                    NSLocalizedString(@"Malta (+356)",nil),
                    NSLocalizedString(@"MarshIsla(+692)",nil),
                    NSLocalizedString(@"Mauritania (+222)",nil),
                    NSLocalizedString(@"Mauritius (+230)",nil),
                    NSLocalizedString(@"Mayotte (+262)",nil),
                    NSLocalizedString(@"Mexico (+52)",nil),
                    NSLocalizedString(@"Micronesia (+691)",nil),
                    NSLocalizedString(@"Moldova (+373)",nil),
                    NSLocalizedString(@"Monaco (+377)",nil),
                    NSLocalizedString(@"Mongolia (+976)",nil),
                    NSLocalizedString(@"Montenegro (+382)",nil),
                    NSLocalizedString(@"Montserrat (+1 664)",nil),
                    NSLocalizedString(@"Morocco (+212)",nil),
                    NSLocalizedString(@"Mozambique (+258)",nil),
                    NSLocalizedString(@"Namibia (+264)",nil),
                    NSLocalizedString(@"Nauru (+674)",nil),
                    NSLocalizedString(@"Nepal (+977)",nil),
                    NSLocalizedString(@"Netherlands (+31)",nil),
                    NSLocalizedString(@"Netherlands Antil(+599)",nil),
                    NSLocalizedString(@"New Caledonia (+687)",nil),
                    NSLocalizedString(@"New Zealand (+64)",nil),
                    NSLocalizedString(@"Nicaragua (+505)",nil),
                    NSLocalizedString(@"Niger (+227)",nil),
                    NSLocalizedString(@"Nigeria (+234)",nil),
                    NSLocalizedString(@"Niue (+683)",nil),
                    NSLocalizedString(@"Norfolk Island (+672)",nil),
                    NSLocalizedString(@"North Korea (+850)",nil),
                    NSLocalizedString(@"Northern Mariana Islands (+1670)",nil),
                    NSLocalizedString(@"Norway (+47)",nil),
                    NSLocalizedString(@"Oman (+968)",nil),
                    NSLocalizedString(@"Pakistan (+92)",nil),
                    NSLocalizedString(@"Palau (+680)",nil),
                    NSLocalizedString(@"Panama (+507)",nil),
                    NSLocalizedString(@"Papua New Guinea (+675)",nil),
                    NSLocalizedString(@"Paraguay (+595)",nil),
                    NSLocalizedString(@"Peru (+51)",nil),
                    NSLocalizedString(@"Philippines (+63)",nil),
                    NSLocalizedString(@"Pitcairn Islands (+870)",nil),
                    NSLocalizedString(@"Poland (+48)",nil),
                    NSLocalizedString(@"Portugal (+351)",nil),
                    NSLocalizedString(@"Puerto Rico (+1)",nil),
                    NSLocalizedString(@"Qatar (+974)",nil),
                    NSLocalizedString(@"Republic of the Congo (+242)",nil),
                    NSLocalizedString(@"Romania (+40)",nil),
                    NSLocalizedString(@"Russia (+7)",nil),
                    NSLocalizedString(@"Rwanda (+250)",nil),
                    NSLocalizedString(@"Saint Barthelemy (+590)",nil),
                    NSLocalizedString(@"SaHel(+290)",nil),
                    NSLocalizedString(@"Saint Kitts and Nevis (+1869)",nil),
                    NSLocalizedString(@"Saint Lucia (+1 758)",nil),
                    NSLocalizedString(@"Saint Martin (+1 599)",nil),
                    NSLocalizedString(@"Saint Pierre Miquelon (+508)",nil),
                    NSLocalizedString(@"Saint Vincent and the Grenadines (+1784)",nil),
                    NSLocalizedString(@"Samoa (+685)",nil),
                    NSLocalizedString(@"San Marino (+378)",nil),
                    NSLocalizedString(@"Sao Tome and Princ(+239)",nil),
                    NSLocalizedString(@"Saudi Arabia (+966)",nil),
                    NSLocalizedString(@"Senegal (+221)",nil),
                    NSLocalizedString(@"Serbia (+381)",nil),
                    NSLocalizedString(@"Seychelles (+248)",nil),
                    NSLocalizedString(@"Sierra Leone (+232)",nil),
                    NSLocalizedString(@"Singap(+65)",nil),
                    NSLocalizedString(@"Slovakia (+421)",nil),
                    NSLocalizedString(@"Slovenia (+386)",nil),
                    NSLocalizedString(@"Solomon Islands (+677)",nil),
                    NSLocalizedString(@"Somalia (+252)",nil),
                    NSLocalizedString(@"South Africa (+27)",nil),
                    NSLocalizedString(@"South Korea (+82)",nil),
                    NSLocalizedString(@"Sri Lanka (+94)",nil),
                    NSLocalizedString(@"Sudan (+249)",nil),
                    NSLocalizedString(@"Suriname (+597)",nil),
                    NSLocalizedString(@"Swaziland (+268)",nil),
                    NSLocalizedString(@"Sweden (+46)",nil),
                    NSLocalizedString(@"Switzerland (+41)",nil),
                    NSLocalizedString(@"Syria (+963)",nil),
                    NSLocalizedString(@"Taiwan (+886)",nil),
                    NSLocalizedString(@"Tajikistan (+992)",nil),
                    NSLocalizedString(@"Tanzania (+255)",nil),
                    NSLocalizedString(@"Thailand (+66)",nil),
                    NSLocalizedString(@"Timor-Leste (+670)",nil),
                    NSLocalizedString(@"Togo (+228)",nil),
                    NSLocalizedString(@"Tokelau (+690)",nil),
                    NSLocalizedString(@"Tonga (+676)",nil),
                    NSLocalizedString(@"Trinidad and Tobago (+1 868)",nil),
                    NSLocalizedString(@"Tunisia (+216)",nil),
                    NSLocalizedString(@"Turkey (+90)",nil),
                    NSLocalizedString(@"Turkmenistan (+993)",nil),
                    NSLocalizedString(@"Turks and Caicos Islands (+1 649)",nil),
                    NSLocalizedString(@"Tuvalu (+688)",nil),
                    NSLocalizedString(@"Uganda (+256)",nil),
                    NSLocalizedString(@"Ukraine (+380)",nil),
                    NSLocalizedString(@"United Arab Emirates (+971)",nil),
                    NSLocalizedString(@"United Kingdom (+44)",nil),
                    NSLocalizedString(@"United States (+1)",nil),
                    NSLocalizedString(@"Uruguay (+598)",nil),
                    NSLocalizedString(@"Virgin Islands (+1 340)",nil),
                    NSLocalizedString(@"Uzbekistan (+998)",nil),
                    NSLocalizedString(@"Vanuatu (+678)",nil),
                    NSLocalizedString(@"Venezuela (+58)",nil),
                    NSLocalizedString(@"Vietnam (+84)",nil),
                    NSLocalizedString(@"Wallis Fut(+681)",nil),
                    NSLocalizedString(@"West Bank(+970)",nil),
                    NSLocalizedString(@"Yemen (+967)",nil),
                    NSLocalizedString(@"Zambia (+260)",nil),
                    NSLocalizedString(@"Zimbabwe (+263)",nil),nil];


    for (int i =0 ; i<251; i++) {
     
        NSString *countryName = countryList[i];
        
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
        NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
        countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
        NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
        NSString *countryStr = [splitStr objectAtIndex:0];
        [newCountryCodeStr addObject:countryStr];
        
    }
    
    NSString *firstLetter = [[detailsOfCallLogs objectForKey:@"fields"]valueForKey:@"callerid"];
    int times = [[firstLetter componentsSeparatedByString:@"+"] count]-1;
    if([detailsOfCallLogs count]!=0){
        NSString *str = @"";
        if(!(times == 1)){
              str = @"+";
        }
       
        NSString *oldTrimmedPhNumber = [[[detailsOfCallLogs objectForKey:@"fields"]valueForKey:@"callerid"]stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *phNumber = [[str stringByAppendingString:oldTrimmedPhNumber]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *trimPhNumber =  [[phNumber stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        numberField.text =[trimPhNumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        numberField.text = self.selectedNumber;
        for (int i =0; i<251; i++) {
            
            NSString *countryName = countryList[i];
            
            
            NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
            NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
            countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
            NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
            NSString *countryStr = [splitStr objectAtIndex:1];
            countryCodeLength =[countryStr length];
            NSRange range = [trimPhNumber rangeOfString:countryStr];
            
            if(range.length != 0){
                NSLog(@"Rangee oF str %i",range.length);
                countryCodeTxtField.text = [splitStr objectAtIndex:0];
            }
            
        }
        
    }

    creditUsedImg.hidden = YES;
    creditImg.hidden = YES;
    [self.creditSpinner startAnimating];
    [minuteUsedSpinner setFrame:CGRectMake(creditsUsedImageView.frame.origin.x,creditsUsedImageView.frame.origin.y, creditsUsedImageView.frame.size.width, creditsUsedImageView.frame.size.height)];
    [self.minuteUsedSpinner startAnimating];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countryCode" ofType:@"plist"]];
    NSLog(@"dictionary = %@", dictionary);
    NSArray *array = [dictionary objectForKey:@"keyarray1"];
    NSLog(@"array = %@", array);
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    username = [defaults objectForKey:@"userName"];
    
   
    self.title =@"";
   
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];

    pickerViewContainer.hidden = YES;
    NSString *path = [[NSBundle mainBundle]pathForResource:@"countryCode" ofType:@"plist"];
    
    countryCodeData = [[NSArray alloc]initWithContentsOfFile:path];
    
    countryCodeData = [countryCodeData sortedArrayUsingSelector:@selector(compare:)];
   
//    pickerViewContainer.frame = CGRectMake(0, 800, 320, 261);
    pickerViewContainer.frame = CGRectMake(0, 0, 320, 261);
    

    UIButton *leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [leftButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backBarButtonItem_clicked) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
   }


-(void)backBarButtonItem_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) getCredits
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    username = [defaults objectForKey:@"userName"];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please check your internet connection and try again later.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    }else
    {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *url =[URL_LINk stringByAppendingString:[NSString stringWithFormat:@"getCallCredits?email_ID=%@",username]];
        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"CALL LOGS RESPONSE : %@",responseObject);
             
             if ([[[responseObject objectForKey:@"creditsBalance"]valueForKey:@"active"] isEqualToString:@"True"])
             {
                 activeUserStatus = YES;
                 
           
             }
             else
             {
                 activeUserStatus = NO;
             }
             creditsBalance.text = [[responseObject objectForKey:@"creditsBalance"]valueForKey:@"credits_balance"];
//             creditsBalanceText.text = [[responseObject objectForKey:@"creditsBalance"]valueForKey:@"credits_balance"];
             
             minUsed.text = [[responseObject objectForKey:@"creditsBalance"]
                             valueForKey:@"call_credits_used"];
             
             if ([creditsBalance.text intValue] < 0)
             {
                 creditsBalance.text =@"0";
             }
             if ([callCreditsUsed.text intValue] < 0)
             {
                 callCreditsUsed.text =@"0";
             }
             [defaults setValue:creditsBalance.text forKey:@"credits"];
             [self.creditSpinner stopAnimating];
             [self.minuteUsedSpinner stopAnimating];
             creditUsedImg.hidden = NO;
             creditImg.hidden = NO;
             creditsLoaded =YES;
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             creditUsedImg.hidden = NO;
             creditImg.hidden = NO;
             
             [self.creditSpinner stopAnimating];
             [self.minuteUsedSpinner stopAnimating];
             NSLog(@"Error: %@", error);
             
         }];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.selectedNumber)
    {
        if([[selectedNumber substringToIndex:1] isEqualToString:@"+"]){
            numberField.text = self.selectedNumber;
        }
        else{
                numberField.text =[NSString stringWithFormat:@"+%@",self.selectedNumber];
        }
        
        [self selectCountryFromNumber:numberField.text];
    }
    self.selectedNumber = nil;
    
    [self createDialer];
    [self.collectionView setFrame:CGRectMake(0, numberField.frame.origin.y + numberField.frame.size.height,self.view.frame.size.width, callButton.frame.origin.y - numberField.frame.origin.y)];
    
    [self getCredits];
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url =[URL_LINk stringByAppendingString:[NSString stringWithFormat:@"getCallCredits?email_ID=%@",username]];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[[responseObject objectForKey:@"creditsBalance"]valueForKey:@"active"] isEqualToString:@"True"])
         {
             activeUserStatus = YES;
                 }
        else
        {
            activeUserStatus = NO;
        }
         creditsBalance.text = [[responseObject objectForKey:@"creditsBalance"]valueForKey:@"credits_balance"];
         
         minUsed.text = [[responseObject objectForKey:@"creditsBalance"]
                         valueForKey:@"call_credits_used"];
         if ([creditsBalance.text intValue] < 0) {
             creditsBalance.text =@"0";
         }
         if ([callCreditsUsed.text intValue] < 0) {
             callCreditsUsed.text =@"0";
             
         }
         [creditsBalance setTextColor:[UIColor redColor]];
         [defaults setValue:creditsBalance.text forKey:@"credits"];
         

         [self.creditSpinner stopAnimating];
         [self.minuteUsedSpinner stopAnimating];
         creditUsedImg.hidden = NO;
         creditImg.hidden = NO;
        
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         creditUsedImg.hidden = NO;
         creditImg.hidden = NO;
         
         [self.creditSpinner stopAnimating];
         [self.minuteUsedSpinner stopAnimating];
         NSLog(@"Error: %@", error);
         
     }];
    if ([self.countryCodeTxtField.text isEqualToString:@""])
    {
//        self.countryCodeTxtField.text=@"Select a country";
    } else {
        [self setCountryCode];
    }
    NSLog(@"Country Selected : %@",self.countryCodeTxtField.text);
    [countrySelected setText:self.countryCodeTxtField.text];
}

-(void)setCountryCode{
    NSArray *splitStr = [[NSArray alloc]init];
    for (int i =0 ; i<251; i++)
    {
        NSString *countryName = countryList[i];
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
        NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
        countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
        splitStr = [countryCodeStr componentsSeparatedByString:@"("];
        NSString *countryStr = [[splitStr objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *temp = [splitStr objectAtIndex:1];
        NSString *str = countryCodeTxtField.text;
        if([str isEqualToString:countryStr])
        {
            countryCode = temp;
        }
    }
}

- (void)device:(TCDevice *)device didReceiveIncomingConnection:(TCConnection *)connection
{
    
  
    NSLog(@"Incoming connection from: %@", [connection parameters][@"From"]);
  if (device.state == TCDeviceStateBusy) {
        [connection reject];
    } else {
        [connection accept];
        //_connection = connection;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marks -UIPickerView & Delegate Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;

}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [newCountryCodeStr count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return newCountryCodeStr[row];
}


-(void)configureLeftMenuButton:(UIButton *)button
{
    
    CGRect frame = button.frame;
    frame.origin = (CGPoint) {0,0};
    frame.size = (CGSize){30,30};
    button.frame = frame;
    [button setImage:[UIImage imageNamed:@"togglebtnSlider.png"] forState:UIControlStateNormal];
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    countryCodeFlag = @"true";
    NSString *selectCountry = [newCountryCodeStr objectAtIndex:row];
    countryCodeTxtField.text = selectCountry;
    [countrySelected setText:selectCountry];
}



- (IBAction)tapGestureKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)countryCodePicker:(id)sender
{
    [self.view bringSubviewToFront:pickerViewContainer];
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"countryCode" ofType:@"plist"];
    
    countryCodeData = [[NSArray alloc]initWithContentsOfFile:path];
    
    [UIView beginAnimations:nil context:NULL];
    pickerViewContainer.hidden = NO;
    [UIView commitAnimations];

}

- (IBAction)InAppClicked:(id)sender
{
    IAPViewController *VC = [[IAPViewController alloc]init];
    [self.navigationController pushViewController:VC animated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        switch (buttonIndex)
        {
        case 0:
            {
                IAPViewController *VC = [[IAPViewController alloc]init];
                [self.navigationController pushViewController:VC animated:YES];
            }
            break;
        case 1: alertView.hidden = YES;
            break;
        default:
            break;
        }
    }
    else if (alertView.tag == 2)
    {
        if (!activeUserStatus)
        {
        
        NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
        __block NSString *userName = [defs objectForKey:@"userName"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"appLogout?email_ID=%@",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:nil forKey:@"userName"];
             
             UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"MainStoryBoard"];
             [self presentViewController:vc animated:YES completion:nil ];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             NSLog(@"LogOut Saved.....: %@", error);
         }];
        
        }
    }
    else if(alertView.tag == 3 || alertView.tag == 4){
        [[[SWHelper alloc] init] openAppSettingsForPermission];
    }
}
-(IBAction)dialButtonPressed:(id)sender
{
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted)
        {
            if ([creditsBalance.text  isEqual: @"0"])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"You have run out of credits. Please purchase more credits.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Buy",nil) otherButtonTitles:NSLocalizedString(@"Later",nil), nil];
                alertView.tag =1;
                [alertView show];
                return;
            }
            if(!creditsLoaded)
            {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please wait while credits are loading",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil,nil];
                [alertView show];
                alertView.tag = 2;
                return;
            }
            else
            {
                NSLog(@"Call Button CLicked");
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:numberField.text forKey:@"calledPhoneNumber"];
//                [defaults setObject:@"false" forKey:@"missedCalledStatus"];
                if([creditsBalance.text isEqualToString:@"0"])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Not enough credits",nil) message:NSLocalizedString(@"Buy additional credits to make calls and send text messages via PayPal",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                    [alert show];
                    
                }else if([numberField.text isEqualToString:@""]){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Phone number alert",nil) message:NSLocalizedString(@"Phone number is not valid",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                    [alert show];
                }else if ([countryCodeTxtField.text isEqualToString:@""]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please select the country",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                    [alert show];
                    
                }else if(![self checkSubstring]){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter the correct country extension.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                    [alert show];
                }else if([numberField.text isEqualToString: countryCode]){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please enter number after the country code.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                    [alert show];
                } else
                    if ([callCreditsUsed.text  isEqual:@""] && [creditsBalance.text  isEqual:@""])
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Credits not Loaded. Please Wait.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                        [alert show];
                        [self getCredits];
                    }
                    else//CHECK ACTIVE
                        if (activeUserStatus)
                        {
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            NSString *userName = [defaults objectForKey:@"userName"];
                            NSLog(@"userName--->%@",userName);
                            
                            AppDelegate *appdelegate = [UIApplication sharedApplication].delegate;
                            NSDictionary *params = @{@"phoneNumber": self.numberField.text,@"email_ID":userName,@"callLimit":creditsBalance.text};
                            
                            //MAKE CALL
                            _connection = [appdelegate.phone connect:params delegate:self];
                            
                            
                            
                            callingViewController = [[SWCallingViewController alloc]init];
                            callingViewController._connection = _connection;
                            [self presentViewController:callingViewController animated:YES completion:nil ];
                            
                            phoneNumber = numberField.text;
                            
                        }
                        else
                        {
                            UIAlertView *activeUserAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Account Expired",nil) message:NSLocalizedString(@"Your account has expired. Please contact administrator.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil)  otherButtonTitles:nil, nil];
                            
                            activeUserAlert.tag = 2;
                            [activeUserAlert show];
                            
                        }
                
            }
            
        }
        else
        {
            UIAlertView *alert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Microphone access required",nil) message:NSLocalizedString(@"In order to use this app to make phone calls you will need to grant access to the phone's microphone.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [alert show];
            alert.tag = 3;
        }
    }];
}

-(BOOL)checkSubstring{
    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (!countryCode){
        [self hidePickerView:nil];
    }
    
    if (ver >= 8.0) {
        // Only executes on version 8 or above.
        if ([numberField.text containsString:countryCode]) {
            NSLog(@"string contains extension!");
            return YES;
        } else {
            NSLog(@"string does not contain extension");
            return NO;
        }
    } else {
        //Execute on version 7 and below
        if ([numberField.text rangeOfString:countryCode].location == NSNotFound) {
            NSLog(@"string does not contain extension");
            return NO;
        } else {
            NSLog(@"string contains country extension!");
            return YES;
        }
    }
    return YES;
}
-(void) viewDidDisappear:(BOOL)animated
{
    pickerViewContainer.hidden = YES;
}

-(IBAction)hangupButtonPressed:(id)sender
{
    AppDelegate *appdelegate = [UIApplication sharedApplication].delegate;
    [appdelegate.phConnection disconnect];
}

-(void)dialOne {
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    pickerViewContainer.hidden = YES;
    numberField.text = [NSString stringWithFormat:@"%@1",numberField.text];
   
}

- (void)dialTwo {
    pickerViewContainer.hidden = YES;
    numberField.text = [NSString stringWithFormat:@"%@2",numberField.text];
}

- (void)dialThree {
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@3",numberField.text];
    
}

- (void)dialFour {
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@4",numberField.text];
}

- (void)dialFive{
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@5",numberField.text];
}

- (void)dialSix{
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@6",numberField.text];
}

- (void)dialSeven{
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@7",numberField.text];
}

- (void)dialEight{
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@8",numberField.text];
}

- (void)dialNine{
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@9",numberField.text];
}

- (void)dialClear{
    pickerViewContainer.hidden = YES;
      numberField.text = [NSString stringWithFormat:@"%@+",numberField.text];
}
- (void)dialPlus{
    pickerViewContainer.hidden = YES;
    numberField.text = [NSString stringWithFormat:@"%@+",numberField.text];
}

- (void)dialZero{
    pickerViewContainer.hidden = YES;
     numberField.text = [NSString stringWithFormat:@"%@0",numberField.text];
}

- (void)dialbackspace
{
    
    phoneNumber = numberField.text;
    
    if ([phoneNumber length]>0)
            {
        
                phoneNumber = [phoneNumber substringToIndex:[phoneNumber length] - 1];
                numberField.text = phoneNumber;
                
            } 

}

- (IBAction)hidePickerView:(id)sender
{
    NSLog(@"Clicked....");
    NSArray *splitStr = [[NSArray alloc]init];
    for (int i =0 ; i<251; i++)
    {
        NSString *countryName = countryList[i];
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
        NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
        countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
        splitStr = [countryCodeStr componentsSeparatedByString:@"("];
        NSString *countryStr = [splitStr objectAtIndex:0];
        NSString *temp = [splitStr objectAtIndex:1];
        NSString *str = countryCodeTxtField.text;
        if([str isEqualToString:countryStr])
        {
           countryCode = temp;
        }
    }
    if([numberField.text length] > 0){
        if(![[numberField.text substringToIndex:1] isEqualToString:@"+"]){
            numberField.text = [countryCode stringByAppendingString:numberField.text];
        } else {
            numberField.text = countryCode;
        }
    } else {
        numberField.text = countryCode;
    }
    countryCodeLength = [countryCode length];
    pickerViewContainer.hidden = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView commitAnimations];
}





-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField == countryCodeTxtField)
    {
        pickerViewContainer.hidden = NO;
    }
    [textField resignFirstResponder];
    
    return YES;
}




- (IBAction)phoneContact:(id)sender
{
    
    CNContactStore * contactStore = [CNContactStore new];
    [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if(granted){
            float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(ver>= 9.0){
                contact = [[CNContactPickerViewController alloc]init];
                [contact setDelegate:self];
                //        [contact setDisplayedPropertyKeys:[NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonPhoneProperty], nil]];
                //        [contact setDisplayedPropertyKeys:[NSArray arrayWithObjects:[NSNumber numberWithInt:KCN]]]
                [self presentViewController:contact animated:YES completion:nil];
            } else {
                contacts = [[ABPeoplePickerNavigationController alloc] init];
                [contacts setPeoplePickerDelegate:self];
                [contacts setDisplayedProperties:[NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]]];
                NSLog(@"Phone Number---->%d",kABPersonPhoneProperty);
                [self presentViewController:contacts animated:YES completion:nil ];
            }
        }
        else{
            UIAlertView *contactAlert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Contacts access required",nil) message:NSLocalizedString(@"In order to use this app to acess Contacts you will need to grant access.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [contactAlert show];
            contactAlert.tag = 4;
        }
    }];
}



- (IBAction)additionSign:(id)sender {
    pickerViewContainer.hidden = YES;
    numberField.text = [NSString stringWithFormat:@"%@+",numberField.text];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

-(void)connectionDidConnect:(TCConnection *)connection
{
    NSLog(@"Connected.....");
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"dailerView"
     object:self];
    [callingViewController callConnected];
}

-(void)connectionDidDisconnect:(TCConnection *)connection
{
    NSLog(@"Disconnect....");
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)connectionDidStartConnecting:(TCConnection *)connection{
    NSLog(@"Connecting........");
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - UITableView Delegate Methods

// Customize the number of sections in the table view.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty{
    NSLog(@"Selected");
    //    ABMutableMultiValueRef multiEmail = ABRecordCopyValue(person);
    //    NSString *phoneNumber = (__bridge NSString *) ABMultiValueCopyValueAtIndex(multiEmail, identifier);
    phoneNumber = [contactProperty.contact.phoneNumbers[0].value valueForKey:@"digits"];
    NSMutableCharacterSet *characterSet =
    [NSMutableCharacterSet characterSetWithCharactersInString:@"()-"];
    NSArray *arrayOfComponents = [phoneNumber componentsSeparatedByCharactersInSet:characterSet];
    phoneNumberStr = [arrayOfComponents componentsJoinedByString:@""];
    NSString *firstLetter = [phoneNumberStr substringToIndex:1];
    
    
    if([firstLetter isEqualToString:@"+"]){
        
        NSString *phNumber = [[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        numberField.text = phNumber;
        NSLog(@"Phone Number----->%@",phoneNumberStr);
        
        for (int i= 0 ; i<251; i++) {
            
            
            NSString *countryName = countryList[i];
            
            
            NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
            NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
            countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
            NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
            NSString *countryStr = [splitStr objectAtIndex:1];
            
            NSArray *temp = [phoneNumberStr componentsSeparatedByString:@" "];
            NSString *countryCodeStr = [temp objectAtIndex:0];
            NSRange range = [countryCodeStr rangeOfString:countryStr];
            
            if(range.length != 0)
            {
                countryCodeTxtField.text = [splitStr objectAtIndex:0];
            }
            
        }
        
        
    }else if([countryCodeFlag isEqualToString:@"true"])
    {
        numberField.text = [[[countryCode stringByAppendingString:phoneNumberStr]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"numberField.text----%@",numberField.text);
        numberField.text = [numberField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    }else{
        numberField.text=[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
}


-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
   
    ABMutableMultiValueRef multiEmail = ABRecordCopyValue(person, property);
    NSString *phoneNumber = (__bridge NSString *) ABMultiValueCopyValueAtIndex(multiEmail, identifier);
    
    NSMutableCharacterSet *characterSet =
    [NSMutableCharacterSet characterSetWithCharactersInString:@"()-"];
    NSArray *arrayOfComponents = [phoneNumber componentsSeparatedByCharactersInSet:characterSet];
    phoneNumberStr = [arrayOfComponents componentsJoinedByString:@""];
     NSString *firstLetter = [phoneNumberStr substringToIndex:1];
    
    
    if([firstLetter isEqualToString:@"+"]){
        
        NSString *phNumber = [[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        numberField.text = phNumber;
        NSLog(@"Phone Number----->%@",phoneNumberStr);
        
        for (int i= 0 ; i<251; i++) {
         
            
            NSString *countryName = countryList[i];
            
            
            NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@")-"];
            NSArray *arrayOfComponents = [countryName componentsSeparatedByCharactersInSet:characterSet];
            countryCodeStr = [arrayOfComponents componentsJoinedByString:@""];
            NSArray *splitStr = [countryCodeStr componentsSeparatedByString:@"("];
            NSString *countryStr = [splitStr objectAtIndex:1];
            
            NSArray *temp = [phoneNumberStr componentsSeparatedByString:@" "];
            NSString *countryCodeStr = [temp objectAtIndex:0];
            NSRange range = [countryCodeStr rangeOfString:countryStr];
            
            if(range.length != 0)
            {
                countryCodeTxtField.text = [splitStr objectAtIndex:0];
            }
            
        }
        
        
    }else if([countryCodeFlag isEqualToString:@"true"])
    {
        numberField.text = [[[countryCode stringByAppendingString:phoneNumberStr]stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"numberField.text----%@",numberField.text);
        numberField.text = [numberField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    }else{
        numberField.text=[[phoneNumberStr stringByReplacingOccurrencesOfString:@" " withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    
    
}


-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [contacts dismissViewControllerAnimated:true completion:nil];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [dialBtn count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier =@"Cell";
    customViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
   
    if(indexPath.row!=11){
        [cell.collectionViewBtn setTitle:[dialBtn objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    }else{
        //[cell.collectionViewBtn setBackgroundImage:buttonImage forState:UIControlStateNormal];
        cell.imageView.image =  [UIImage imageNamed:@"arrow.png"];
    }
    [dialBtn objectAtIndex:indexPath.row];
    if(indexPath.row == 0){
            [cell.collectionViewBtn addTarget:self action:@selector(dialOne) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 1){
        [cell.collectionViewBtn addTarget:self action:@selector(dialTwo) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 2){
        [cell.collectionViewBtn addTarget:self action:@selector(dialThree) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 3){
        [cell.collectionViewBtn addTarget:self action:@selector(dialFour) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 4){
        [cell.collectionViewBtn addTarget:self action:@selector(dialFive) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 5){
        [cell.collectionViewBtn addTarget:self action:@selector(dialSix) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 6){
        [cell.collectionViewBtn addTarget:self action:@selector(dialSeven) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 7){
        [cell.collectionViewBtn addTarget:self action:@selector(dialEight) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 8){
        [cell.collectionViewBtn addTarget:self action:@selector(dialNine) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 9){
        [cell.collectionViewBtn addTarget:self action:@selector(dialClear) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 10){
        [cell.collectionViewBtn addTarget:self action:@selector(dialZero) forControlEvents:UIControlEventTouchUpInside];
    }else if(indexPath.row == 11){
        [cell.collectionViewBtn addTarget:self action:@selector(dialbackspace) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}





- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   NSInteger width = collectionView.frame.size.width;
    if(width == 375)
    {
        return CGSizeMake(115, 70);
    }else
    if(width == 414)
    {
        return CGSizeMake(130, 80);
    }else
    {
        return CGSizeMake(100, 58);
    }
   
    
}

-(void)createDialer
{
    UIView *dialerView = [[UIView alloc]initWithFrame:CGRectMake(-1, self.numberField.frame.origin.y+numberField.frame.size.height+10, self.view.frame.size.width+2, self.callButton.frame.origin.y-numberField.frame.origin.y-numberField.frame.size.height-8)];
    
    [self.view addSubview:dialerView];
    [self.callButton setFrame:CGRectMake(0, self.view.frame.size.height - 30, self.view.frame.size.width,30)];
    CGFloat dialerWidth = dialerView.frame.size.width;
    CGFloat dialerHeight = dialerView.frame.size.height;
    CGFloat buttonWidth = dialerWidth/3;
    CGFloat buttonHeight = dialerHeight/4;
    NSMutableArray *buttonArray = [NSMutableArray array];
    for (int i=1; i<=12; i++)
    {
        UIButton *button = [[UIButton alloc]init];
        [button setTitle:[NSString stringWithFormat:@"%d",i] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"buttonbk.png"] forState:UIControlStateHighlighted];

        
            if((self.view.frame.size.width == 320) && (self.view.frame.size.height == 480))
            {
                // do something
                [button.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:24.0f]];
            }
            else
                [button.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:32.0f]];
        
        

        [button.layer setBorderWidth:1];
        switch (i)
        {
            
            case 1: [button setFrame:CGRectMake(0, 1, buttonWidth,buttonHeight)];
                    [button addTarget:self action:@selector(dialOne) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 2: [button setFrame:CGRectMake(buttonWidth-1, 1, buttonWidth+3,buttonHeight)];
                    [button addTarget:self action:@selector(dialTwo) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 3: [button setFrame:CGRectMake(2*buttonWidth+1, 1, buttonWidth+1,buttonHeight)];
                    [button addTarget:self action:@selector(dialThree) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 4: [button setFrame:CGRectMake(0, buttonHeight, buttonWidth, buttonHeight)];
                    [button addTarget:self action:@selector(dialFour) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 5: [button setFrame:CGRectMake(buttonWidth-1, buttonHeight, buttonWidth+3, buttonHeight)];
                    [button addTarget:self action:@selector(dialFive) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 6: [button setFrame:CGRectMake(2*buttonWidth+1, buttonHeight, buttonWidth+1, buttonHeight)];
                    [button addTarget:self action:@selector(dialSix) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 7: [button setFrame:CGRectMake(0, 2*buttonHeight-1, buttonWidth,buttonHeight)];
                    [button addTarget:self action:@selector(dialSeven) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 8: [button setFrame:CGRectMake(buttonWidth-1, 2*buttonHeight-1, buttonWidth+3,buttonHeight)];
                    [button addTarget:self action:@selector(dialEight) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 9: [button setFrame:CGRectMake(2*buttonWidth+1, 2*buttonHeight-1, buttonWidth+1,buttonHeight)];
                    [button addTarget:self action:@selector(dialNine) forControlEvents:UIControlEventTouchUpInside];
                    break;
            case 10: [button setFrame:CGRectMake(0,3*buttonHeight-2, buttonWidth,buttonHeight)];
                     [button setTitle:@"+" forState:UIControlStateNormal];
                     [button addTarget:self action:@selector(dialPlus) forControlEvents:UIControlEventTouchUpInside];
                      break;
            case 11: [button setFrame:CGRectMake(buttonWidth-1, 3*buttonHeight-2, buttonWidth+3,buttonHeight)];
                     [button setTitle:@"0" forState:UIControlStateNormal];
                     [button addTarget:self action:@selector(dialZero) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 12: [button setFrame:CGRectMake(2*buttonWidth+1, 3*buttonHeight-2, buttonWidth+1,buttonHeight)];
                     [button setTitle:@"" forState:UIControlStateNormal];
                     [button setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
                     [button addTarget:self action:@selector(dialbackspace) forControlEvents:UIControlEventTouchUpInside];
                break;
            default:
                break;
        }
        UIColor *borderColor = [UIColor colorWithRed:76.0/256.0f green:95.0/256.0f blue:113.0/256.0f alpha:100.0f];
        [button.layer setBorderColor:borderColor.CGColor];
        [buttonArray addObject:button];
        [dialerView addSubview:button];
    }
    
}

-(void) selectCountryFromNumber:(NSString *) Number
{
    
     if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+91"])
            countryCodeTxtField.text = NSLocalizedString(@"India",nil);
        else
        if (Number.length > 4 && [[Number substringToIndex:4] isEqualToString:@"+507"])
            countryCodeTxtField.text = NSLocalizedString(@"Panama",nil);
        else
            if (Number.length > 4 && [[Number substringToIndex:4] isEqualToString:@"+506"]) {
                countryCodeTxtField.text =  NSLocalizedString(@"Costa Rica",nil);
            }
            else
                if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+34"]) {
                    countryCodeTxtField.text =  NSLocalizedString(@"Spain",nil);
                }
                else
                    if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+49"]) {
                       countryCodeTxtField.text = NSLocalizedString(@"Germany",nil);
                    }
                    else
                        if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+52"]) {
                            countryCodeTxtField.text = NSLocalizedString(@"Mexico",nil);
                        }
                        else
                            if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+33"]) {
                                countryCodeTxtField.text = NSLocalizedString(@"France (+33)",nil);
                            }
                            else
                                if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+55"]) {
                                    countryCodeTxtField.text =NSLocalizedString(@"Brazil",nil);
                                } else
                                    if (Number.length > 2 && [[Number substringToIndex:2] isEqualToString:@"+3"]) {
                                        countryCodeTxtField.text =NSLocalizedString(@"Italy ",nil);
                                    }
                                    else
                                        if (Number.length > 4 && [[Number substringToIndex:4] isEqualToString:@"+353"]) {
                                            countryCodeTxtField.text =  NSLocalizedString(@"Irel",nil);
                                        }
                                        else
                                            if (Number.length > 4 && [[Number substringToIndex:4] isEqualToString:@"+351"]) {
                                                countryCodeTxtField.text =  NSLocalizedString(@"Portugal",nil);
                                            }
                                            else
                                                if (Number.length > 2 && [[Number substringToIndex:2] isEqualToString:@"+1"]) {
                                                    countryCodeTxtField.text =  NSLocalizedString(@"Canada",nil);
                                                }
                                                else
                                                    if (Number.length > 5 && [[Number substringToIndex:5] isEqualToString:@"+1809"]) {
                                                        countryCodeTxtField.text = NSLocalizedString(@"D.R.",nil);
                                                    }
                                                    else
                                                        if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+56"]) {
                                                            countryCodeTxtField.text =  NSLocalizedString(@"Chile",nil);
                                                        }
                                                        else
                                                            if (Number.length > 3 && [[Number substringToIndex:3] isEqualToString:@"+41"]) {
                                                                countryCodeTxtField.text =  NSLocalizedString(@"Switzerland",nil);
                                                            }
                                                            else
                                                                countryCodeTxtField.text = NSLocalizedString(@"Other",nil);
                                
    
    
}
@end
