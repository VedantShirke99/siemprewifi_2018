//
//  Constants.h
//  SampleSlider
//
//  Created by Jayesh on 28/02/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FLURRYTOKEN @"PG33BT5QGTZ5FRXRZDGC"
#define GATRACKINGID  @"UA-59989421-1" //UA-59989421-1
#define GAALLOWTRACKING  @"allowTracking"
#define TOKENINTERVAL 3600

@interface Constants : NSObject

@end
