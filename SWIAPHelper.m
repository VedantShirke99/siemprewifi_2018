//
//  SWIAPHelper.m
//  SiempreWifi
//
//  Created by Jayesh on 03/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

/*
 Generate Shared Secret
 A shared secret is a unique code that you should use when you make the call to our servers for your In-App Purchase receipts. Without a shared secret, you will not be able to test auto-renewable In-App Purchase subscriptions in the sandbox mode. Also note that you will not be able to make them available on the App Store.
 Note: Regardless of what app they are associated with, all of your auto-renewable subscriptions will use this same shared secret.
 d253a8b9230b430fb0db8cc9f7f36fe6
*/
#import "SWIAPHelper.h"

@implementation SWIAPHelper
static SWIAPHelper *helper;
//static dispatch_once_t once;
+(instancetype) sharedInstance
{
    if (!helper)
    {
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      PLATINUM_PACK_ID,
                                      GOLD_PACK_ID,
                                      SILVER_PACK_ID,
                                      nil];
        helper = [[SWIAPHelper alloc]initWithProductIdentifiers:productIdentifiers];
    }
    else
        return helper;
    
    return helper;
}

@end
