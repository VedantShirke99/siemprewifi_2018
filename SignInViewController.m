//
//  SignInViewController.m
//  SampleSlider
//
//  Created by Jayesh on 22/11/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "SignInViewController.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import <AddressBook/AddressBook.h>
#import "MBProgressHUD.h"
#import "HomeViewController.h"
#import <unistd.h>
#import "Constants.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"

#define SCREENSHOT_MODE 0



#ifndef kCFCoreFoundationVersionNumber_iOS_8_0
#define kCFCoreFoundationVersionNumber_iOS_7_0 847.20
#endif



@interface SignInViewController ()<MBProgressHUDDelegate,UITextFieldDelegate>{
   
    Reachability* reachability;
    MBProgressHUD *HUD;
    long long expectedLength;
    long long currentLength;
}

@end

@implementation SignInViewController

@synthesize myspinner,userNameTextfield,passwordTextfield;
@synthesize signinbtn;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [userNameTextfield setBorderStyle:UITextBorderStyleNone];
    userNameTextfield.backgroundColor = [UIColor whiteColor];
    userNameTextfield.returnKeyType = UIReturnKeyNext;

    [passwordTextfield setBorderStyle:UITextBorderStyleNone];
    passwordTextfield.backgroundColor = [UIColor whiteColor];
    passwordTextfield.returnKeyType = UIReturnKeyGo;
    //[UIDevice currentDevice].proximityMonitoringEnabled = YES;
    self.userNameTextfield.delegate = self;
    self.passwordTextfield.delegate = self;
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)signInClicked:(id)sender
{
    [self signIn];
}
-(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}
-(void) signIn
{
    if ([userNameTextfield.text  isEqual:@""] && [passwordTextfield.text  isEqual: @""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Alert", nil) message:NSLocalizedString(@"Please enter username and password",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil)  otherButtonTitles:nil, nil];
            [alert show];
    }
    else
    if([userNameTextfield.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Alert" message: NSLocalizedString(@"Please Enter valid Email ID", nil)  delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
            [alert show];
    }
    else
    if([passwordTextfield.text isEqualToString:@""])
    {
                
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Alert",nil) message:NSLocalizedString(@"Please Enter the Password",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                [alert show];
    }
    else
    if (![self validateEmail:userNameTextfield.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Alert",nil) message:NSLocalizedString(@"Please Enter valid Email ID",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        hud.delegate = self;
        hud.labelText = NSLocalizedString(@"Authenticating",nil);
        
        NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"appLogin?email_ID=%@&password=%@",userNameTextfield.text,passwordTextfield.text]];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSLog(@"Server Address--->%@",serverAddress);
        if([userNameTextfield.text isEqualToString:@" "])
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"Please Enter the Email Id or Password",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            
            [userNameTextfield becomeFirstResponder];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 self.posts = (NSDictionary *) responseObject;
                 
                 NSString *value = self.posts[@"loginCheck"][@"success"];
                 
                 
                 if ([value isEqualToString:@"true"])
                 {
                     NSString *userName = [userNameTextfield text];
                     NSString *password = [passwordTextfield text];
                     NSInteger flag = true;
                     userName = [userName lowercaseString];
                     
                     [defaults setObject:userName forKey:@"userName"];
                     [defaults setObject:password forKey:@"password"];
                     [defaults setInteger:flag forKey:@"flag"];
                     [defaults synchronize];
                     AppDelegate *delegate = [UIApplication sharedApplication].delegate;
                     [delegate registrationForTwilio];
                     [self dismissViewControllerAnimated:YES completion:nil];
                     [self.myspinner startAnimating];
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     
                    
                     NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                                   nil];
                     [Flurry logEvent:@"User LoggedIn" withParameters:flurryParams];
                     
                     id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                     [tracker send:[[GAIDictionaryBuilder createAppView] build]];
                     [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Message Received "     // Event category (required)
                            action:@"None"  // Event action (required)
                            label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                            value:nil] build]];
                     
                 }
                 else if ([value isEqualToString:@"inactive"])
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"This user is inactive. Please contact administrator",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     [alert show];
                     
                 }
                 else if ([value isEqualToString:@"logged"])
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:NSLocalizedString(@"This User Already Logged In! Please contact administratior.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     [alert show];
                   
                 }
                 else
                 {
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Failed",nil) message:NSLocalizedString(@"The username or password is incorrect. Try again.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     [alert show];
                     
                 }
                 
                 NSString *deviceToken = [defaults objectForKey:@"deviceTokenStr"];
                 UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                 localNotification.alertBody = @"Your alert message";
                 
                 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                 NSString  *serverAddress = [URL_LINk stringByAppendingString:[NSString stringWithFormat:@"updateApnsRegId?email_ID=%@&regId=%@&device_type=iphone",[userNameTextfield.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[deviceToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                 [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
                  {
                      NSLog(@"Saved");
                      
                  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      NSLog(@"Not Saved.....: %@", error);
                      
                  }];
                 
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 NSLog(@"Error: %@", error);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",nil) message:@"Error while connecting to the server." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 [alert show];
             }];
        }
    }

}
- (void)myTask
{
    // Do something usefull in here instead of sleeping ...
    sleep(1);
}
- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"forgotPasswordSegue" sender:self];
}

//Make keyboard move down using gesture
- (IBAction)backgroundTap:(id)sender
{
    [self.view endEditing:YES];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.userNameTextfield)
    {
        [textField resignFirstResponder];
        [passwordTextfield becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self signIn];
        
    }
         return NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [HUD show:NO];
}


-(void)spinner
{
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.dimBackground = YES;
    HUD.delegate = self;
    [HUD show:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
     NSLog(@"TxtEditing");
    if(textField == self.userNameTextfield){
       
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 25), self.view.frame.size.width,self.view.frame.size.height);
        [UIView commitAnimations];
    }else if(textField == self.passwordTextfield){
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 50), self.view.frame.size.width,self.view.frame.size.height);
        [UIView commitAnimations];
    }

}

- (void)textFieldDidEndEditing:(UITextField *)textfield{
    if(textfield == self.userNameTextfield){
         NSLog(@"TxtEditing");
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 25), self.view.frame.size.width,self.view.frame.size.height);
        [UIView commitAnimations];
    }else if(textfield == self.passwordTextfield){
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 50), self.view.frame.size.width,self.view.frame.size.height);
        [UIView commitAnimations];
    }


   }

@end
