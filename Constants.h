//
//  Constants.h
//  SiempreWifi
//
//  Created by Jayesh on 15/05/15.
//  Copyright (c) 2015 WhiteSnow. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SILVER_PACK_ID @"com.livepulsewebsolutios.SiempreWifi.Silver_Pack"
#define GOLD_PACK_ID @"com.livepulsewebsolutios.SiempreWifi.GoldPack"
#define PLATINUM_PACK_ID @"com.livepulsewebsolutios.SiempreWifi.Platinum_Pack"


#define SILVER_PACK @"Silver Pack"
#define GOLD_PACK @"Gold Pack"
#define PLATINUM_PACK @"Platinum Pack"