//
//  callingVieController.m
//  SampleSlider
//
//  Created by Jayesh on 12/1/14.
//  Copyright (c) 2014 WhiteSnow. All rights reserved.
//

#import "callingVieController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "CallViewController.h"
#import "TableViewController.h"
#import "HomeViewController.h"
#import "Flurry.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
@interface callingVieController ()<TCDeviceDelegate,TCConnectionDelegate>


@end

@implementation callingVieController{
    bool start;
    NSTimeInterval time;
    NSString *startTime;
    NSString *userName;
    NSString *token;
    NSString *displayTimerStatus;
}
@synthesize calledPhoneNumber,calledNumber,displayTimer,endbtn;

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = NO;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.endbtn setTitle:NSLocalizedString(@"END",nil) forState:UIControlStateNormal];
    // Do any additional setup after loading the view.jjjsw37e1`
    UIDevice *device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = YES;
   [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dailerView) name:@"dailerView" object:nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    calledNumber.text = [defaults objectForKey:@"calledPhoneNumber"];
    self.displayTimer.text = @"";
    start = false;
    [endbtn setTitle:NSLocalizedString(@"END", nil)  forState:UIControlStateNormal];
    if (start == false)
    {
        start = true;
    }
    else
    {
    start = false;
    }
  }
-(void) updateDisplyTimer
{
    for (int i = 0; i<5; i++)
    {
        sleep(1);
        displayTimer.text = [NSString stringWithFormat:@"%@.",displayTimer.text];
    }
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
-(void)device:(TCDevice*)device didStopListeningForIncomingConnections:(NSError*)error
{
    NSLog(@"Stopped Listening");
    NSLog(@"Twilio Service Error : Twillio Server Disconnected");
}
-(void)connection:(TCConnection*)connection didFailWithError:(NSError*)error
{
    NSLog(@"Failed Due to Error: %@",error);

}


-(void)update
{
    int minutes;
    
    if (start == false)
    {
        return;
    }
    else
    {
       
        displayTimer.hidden = YES;
        if([displayTimerStatus isEqualToString:@"YES"])
        {
            displayTimer.hidden = NO;
            NSTimeInterval currebtTime = [NSDate timeIntervalSinceReferenceDate];
            NSTimeInterval elappedTime = currebtTime -time;
            
             minutes= (int)(elappedTime / 60.0);
            int seconds = (int)(elappedTime - (minutes*60));
            self.displayTimer.text = [NSString stringWithFormat:@"%02u:%02u",minutes,seconds];
        }
        
        
        [self performSelector:@selector(update) withObject:self afterDelay:0.1];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *credits = [defaults objectForKey:@"credits"];
        
        if([credits integerValue] == minutes){
            AppDelegate *delegate =[UIApplication sharedApplication].delegate;
            [delegate.phone disconnectAll];
            [self dismissViewControllerAnimated:true completion:nil];
            
        }
    }
    
}
- (void)didReceiveMemoryWarning
{
    calledPhoneNumber.text = calledNumber.text;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)timer
{
    [self performSelectorOnMainThread:@selector(timerOnMainThread) withObject:nil waitUntilDone:NO];
}
-(void)timerOnMainThread
{
    NSLog(@"Timmer Clicked");
    self.displayTimer.hidden = YES;
    
}
- (IBAction)callEndBtn:(id)sender
{
    NSLog(@"End Button CLicked");
    start = false;
    [self dismissViewControllerAnimated:true completion:nil];
    AppDelegate *delegate =[UIApplication sharedApplication].delegate;
    [delegate.phone disconnectAll];
    [self creditCalculation];

}

-(void) creditCalculation
{
    NSLog(@"Call End Button --->%@",startTime);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    userName = [defaults objectForKey:@"userName"];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    startTime = [DateFormatter stringFromDate:[NSDate date]];
    [defaults setObject:self.displayTimer.text forKey:@"displayTimer"];
    [defaults setObject:calledNumber.text forKey:@"calledNumber"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString  *serverAddress =[URL_LINk stringByAppendingString:[NSString stringWithFormat:@"update_call_log?email_ID=%@&time=%@&duration=%@&caller_ID=%@&type=dialed",[userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[startTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.displayTimer.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[calledNumber.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSDictionary *flurryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]],@"USER NAME",
                                  [NSString stringWithFormat:@"Dialed"],@"CALL TYPE",
                                  [NSString stringWithFormat:@"%@",calledNumber.text],@"CALL RECEIPEINT",
                                  nil];
    
    [Flurry logEvent:@"Call" withParameters:flurryParams];
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[NSString stringWithFormat:@"CALL DAILED TO : %@",calledNumber.text]     // Event category (required)
                                                          action:@"CALL TAKEN"  // Event action (required)
                                                           label:[NSString stringWithFormat:@"User Logged In : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]         // Event label
                                                           value:nil] build]];
    
    NSLog(@"startTime------>%@",serverAddress);
    [manager GET:serverAddress parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Saved");
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Not Saved.....: %@", error);
         
     }];
    
    NSUserDefaults *durationTime = [NSUserDefaults standardUserDefaults];
    
    [durationTime setObject:self.displayTimer.text forKey:@"durationTime"];
}



-(void)connectionDidDisconnect:(TCConnection *)connection
{
    NSLog(@"Disconnect....");
}

-(void)connectionDidStartConnecting:(TCConnection *)connection
{
    NSLog(@"Connecting........");
}

-(void)dailerView
{
    NSLog(@"Call Connected....");
    displayTimer.text =@"";
    [self performSelectorOnMainThread:@selector(update) withObject:self waitUntilDone:NO];
    displayTimerStatus =@"YES";
    time = [NSDate timeIntervalSinceReferenceDate];
}

@end
